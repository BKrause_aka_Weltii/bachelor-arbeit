import os
import re
from pprint import pprint


def run():
	labelRegEx = r"\\label\{(?P<name>([\w\s\d:]*))\}"
	labels = dict()
	files = os.listdir("./")

	for file in files:
		if re.search(r"[\w]+.tex", file):
			f = open(file, 'r')
			lines = f.readlines()
			found = []
			for line in lines:
				m = re.search(labelRegEx, line)
				if m:
					found.append(m.group('name'))
			if len(found) > 0:
				labels[file] = found
	pprint(labels)


if __name__ == '__main__':
	run()
