import re
import os
import subprocess
import sys

page_regex = r"name=\"(?P<names>[\d\s\w\-\_]*)\""
draw_io_cmd = "/var/lib/flatpak/app/com.jgraph.drawio.desktop/x86_64/stable/active/files/draw.io/drawio" # "/snap/bin/drawio" 
file_extension = ".jpg"

def get_page_names(file_path):
    f = open(file_path, "r")
    lines = f.readlines()[0].split(">")
    names = []
    for line in lines:
        m = re.search(page_regex, line)
        if m:
            names.append(m.group("names"))
    return names


def get_file_name(file_path):
    splitSlash = file_path.split("/")
    splitDot = splitSlash[len(splitSlash) - 1].split(".")
    return splitDot[0]


def run(file_path: str, export_to_path: str):
    names = get_page_names(file_path)
    file_name = get_file_name(file_path).strip()

    for index, name in enumerate(names):
        print(f"\n####### Export {name} #######\n")
        # Comment out which version you don't need!
        new_file_name = f"{file_name}-{name}{file_extension}"
        # new_file_name = f"{file_name}-{name}.{file_extension}"
        cmd = [
            draw_io_cmd,
            "-x",
            "-f png",
            f"-p {index}",
            f"-o {new_file_name}",
            file_path
        ]

        subprocess.run(cmd, stdout=subprocess.PIPE)
        subprocess.run(["mv", f"./ {new_file_name}", f"{export_to_path}{new_file_name}"])


if __name__ == "__main__":
    if (sys.argv[1] == "--help"):
        print("Help for drawio export diagram script")
        print("export-diagram.py <path to .drawio file> <export to folder>")
    else:
        file_path = sys.argv[1]
        if (not file_path):
            raise Exception("path to .drawio file is missing!")
        export_path = sys.argv[2]
        if (not export_path):
            raise Exception("export to folder path is missing!")
        run(file_path, export_path)

