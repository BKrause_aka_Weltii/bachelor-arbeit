function string:isempty()
    return self == nil or self == ''
end

function copyTable(table)
    local mewTable = {}
    for key, value in pairs(table) do
        mewTable[key] = value
    end
    return mewTable
end

function string:split(seperator, max, regex)
    local final = {}

    if seperator == '' then
        seperator = ','
    end

    if max and max < 1 then
        max = nil
    end


    if self:len() > 0 then
        local plain = not regex
        max = max or -1

        local field, start = 1, 1
        local first, last = self:find(seperator, start, plain)
        while first and max ~= 0 do
            final[field] = self:sub(start, first - 1)
            field = field + 1
            start = last + 1
            first, last = self:find(seperator, start, plain)
            max = max - 1
        end
        final[field] = self:sub(start)
    end

    return final
end

function read(path)
    local seperator = ";"
    local csv = {}
    local file = assert(io.open(path, "r"))
    for line in file:lines() do
        fields = line:split(seperator)
        for i = 1, #fields do
            local field = fields[i]
            if field == '' then
                field = " "
            end
            fields[i] = tonumber(field) or field
        end
        table.insert(csv, fields)
    end
    file:close()
    return csv
end

local csv = read("../assets/feature_poll.csv")
local baseTable = {
    ["Das brauche ich!"] = 0,
    ["Könnte nützlich sein"] = 0,
    ["Wahrscheinlich brauche ich es nicht"] = 0,
    ["Werde ich definitiv nicht nutzen!"] = 0,
    ["Ist mir egal"] = 0,
}
local weigthTable = {
    ["Das brauche ich!"] = 1,
    ["Könnte nützlich sein"] = 2,
    ["Wahrscheinlich brauche ich es nicht"] = 3,
    ["Werde ich definitiv nicht nutzen!"] = 4,
    ["Ist mir egal"] = 5,
}
local table = {
    ["19"] = "Ein einfacher Chat",
    ["20"] = "Private Gruppenchats",
    ["21"] = "Öffentliche Gruppenchats",
    ["22"] = "News Forum",
    ["23"] = "Ein Foodshare-Programm",
    ["24"] = "Organisationsmöglichkeiten für einen Wohnheimflohmarkt",
    ["25"] = "Meldungen an den Hausmeister",
    ["26"] = "Grillplatz-Buchungen",
    ["27"] = "Veranstaltungsinformationen (bspw. Parties)",
    ["28"] = "Kontaktdaten (bspw. des Hausmeister)",
}
local data = {
    ["Ein einfacher Chat"] = copyTable(baseTable),
    ["Private Gruppenchats"] = copyTable(baseTable),
    ["Öffentliche Gruppenchats"] = copyTable(baseTable),
    ["News Forum"] = copyTable(baseTable),
    ["Ein Foodshare-Programm"] = copyTable(baseTable),
    ["Organisationsmöglichkeiten für einen Wohnheimflohmarkt"] = copyTable(baseTable),
    ["Meldungen an den Hausmeister"] = copyTable(baseTable),
    ["Grillplatz-Buchungen"] = copyTable(baseTable),
    ["Veranstaltungsinformationen (bspw. Parties)"] = copyTable(baseTable),
    ["Kontaktdaten (bspw. des Hausmeister)"] = copyTable(baseTable),
}

for i = 3, 112, 1 do -- start in line 2, run to line 112, steps 1
    for key, value in ipairs(csv[i]) do
        if (key > 18 and key < 29) then -- only show pairs, where the value is not empty
            if (not value:isempty()) then
                local col = table[tostring(key)]
                local chopValue = value:gsub('"', '')
                local count = data[col][chopValue]
                if (count ~= nil) then
                    data[col][chopValue] = count + 1
                end
            end
        end
    end
end

local linesep = " \\\\ \\hline\n"
local output = "Funktionen &"

for key, value in pairs(baseTable) do
    output = output .. " " .. key .. " &"
end
output = output:sub(1, -2) .. "Durchschnitt " .. linesep

for key, value in pairs(data) do
    output = output .. key .. " & "
    local avarage = 0
    local totalCount = 0
    for k, v in pairs(value) do
        totalCount = totalCount + tonumber(v)
        avarage = avarage + (weigthTable[k] * v)
        output = output .. v .. " & "
    end
    output = output:sub(1, -2) .. " " .. avarage / totalCount .. linesep
end

-- TODO add ordering
print(output)