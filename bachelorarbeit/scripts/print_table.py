# Baut die Tabelle für die Nutzwertanalyse

# Community, Hilfestellungen in Online Foren | CHOF
# Communitybeiträge in Form von Komponenten | CINFOK
# Dokumentation des Framework | DDF
# Compiler Hilfen. | CH
# Persönliche Präferenz | PP
# Zusatz | Z

# | Schlecht   | Mittel | Gut   | Sehr gut |
# | ---------- | ------ | ----- | -------- |
# | 1 - 3      | 4 - 5  | 6 - 8 | 9 - 10   |

wichtung=dict(
    chof=0.2,
    cinfok=0.1,
    ddf=0.2,
    ch=0.1,
    pp=0.3,
    z=0.1
)


data = dict(
    Angular=dict(
        chof=10, cinfok=10, ddf=8, ch=4, pp=6, z=5,  # Zusatz - Jenachdem wie weit ich mit Babel etc. gehe wird der Compileprozess besser
    ),
    Elm=dict(
        chof=8, cinfok=4, ddf=6, ch=10, pp=8, z=8,  # Zusatz, klare Sprache, neu, hat nur einen Einsatzzweck und gibt einen guten Weg vor, wie man Code schreibt. Tolle Syntax
    ),
    Flutter=dict(
        chof=0, cinfok=0, ddf=0, ch=0, pp=0, z=0,
    ),
    React=dict(
        chof=10, cinfok=10, ddf=10, ch=0, pp=6, z=5,  # Zusatz - Jenachdem wie weit ich mit Babel etc. gehe wird der Compileprozess besser
    ),
    Vue=dict(
        chof=10, cinfok=10, ddf=10, ch=8, pp=10, z=5,  # Zusatz - Jenachdem wie weit ich mit Babel etc. gehe wird der Compileprozess besser
    ),
)

def dict_values_as_string(data: dict):
    ret: str = ""
    sum = 0
    for key in data:
        s = round(data[key], 2)
        sum += s
        ret += f"{s} & "
    ret += f"{round(sum, 2)}"
    return ret


def dict_values_with_wichtung_as_string(data: dict, wichtung: dict):
    ret: str = ""
    sum = 0
    for key in data:
        s = round(data[key] * wichtung[key], 2)
        sum += s
        ret += f"{s} & "
    ret += f"{round(sum, 2)}"
    return ret


def print_mit_wichtung(data: dict):
    for framework in data:        
        print(f"{framework} & {dict_values_with_wichtung_as_string(data[framework], wichtung)} \\tabB")


def print_ohne_wichtung(data: dict):
    for framework in data:
        print(f"{framework} & {dict_values_as_string(data[framework])} \\tabB")


if __name__ == "__main__":
    print("Mit Wichtung")
    print_mit_wichtung(data)
    print("Ohne Wichtung")
    print_ohne_wichtung(data)
