import uvicorn
from email_proxy.fast_api import app

app = app

if __name__ == "__main__":
    uvicorn.run("email_proxy.fast_api:app", host="0.0.0.0", port=8000, reload=True)
