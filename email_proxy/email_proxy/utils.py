from email.message import EmailMessage


def is_missing(key, in_arr, missing_arr):
    missing_arr.append(key) if not in_arr.get(key, False) else None


class MissingValues(BaseException):
    pass


def add_header_field_to_email_message(key: str, value, email_message: EmailMessage):
    # if the key already in the email_message, then replace the header
    if key in email_message:
        email_message.replace_header(key, value)
    else:
        email_message[key] = value


def get_time_string_format():
    return "%a, %d %b %Y %H:%M:%S %z"
