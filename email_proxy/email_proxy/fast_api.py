import json
from typing import Optional, List

from fastapi import FastAPI
from pydantic import BaseModel
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse, Response

from email_proxy.default_providers import default_providers
from email_proxy.imap_socket import ImapSocket
from email_proxy.mail import Mail
from email_proxy.provider import Provider
from email_proxy.provider_cache import ProviderCache, ProviderWrongData
from email_proxy.user import User

provider_cache = ProviderCache()


class BaseUserData(BaseModel):
    user: User
    provider_id: Optional[str] = None
    provider: Optional[Provider] = None


class SendMail(BaseUserData):
    email: Mail


class GetMails(BaseUserData):
    search_string: str
    mailboxes: List[str]
    move_to_mailbox: Optional[str]


def init() -> FastAPI:
    for provider in default_providers:
        try:
            provider_cache.add_provider(provider)
        except ProviderWrongData as e:
            print(f"Error: Provider {provider.id} raises with: {e}")

    fast_api = FastAPI()

    origins = [
        "*"
    ]

    fast_api.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @fast_api.get("/")
    def root():
        return RedirectResponse("/docs#")

    @fast_api.get("/ping")
    def ping():
        """
        Check if the email_proxy is available
        :return: string
        """
        return "pong"

    @fast_api.get("/ding")
    def ding():
        """
        Check if the email_proxy is available
        :return: string
        """
        return "dong dongelong"

    @fast_api.get("/providers")
    def get_providers():
        providers = []
        prov_tmp = provider_cache.get_all_providers()
        for key in prov_tmp.keys():
            providers.append(prov_tmp[key].to_json())
        return Response(
            content=json.dumps(providers, indent=2), media_type="application/json",
        )

    @fast_api.post("/providers")
    def post_provider(provider: Provider):
        if provider_cache.has_provider(provider.id):
            if provider_cache.get_provider(provider.id).__eq__(provider):
                return Response(
                    content=json.dumps(
                        dict(
                            message=f"Your provider is successfully added to the system",
                            known_provider=json.dumps(
                                provider_cache.get_provider(provider.id).to_json(), indent=2
                            ),
                        )
                    ),
                    media_type="application/json",
                    status_code=200,
                )
            else:
                return Response(
                    content=json.dumps(
                        dict(
                            message=f"I already have a proxy with the id {provider.id} in my system and the data are different. Please check your data or change the wanted id.",
                            known_provider=json.dumps(
                                provider_cache.get_provider(provider.id).to_json(), indent=2
                            ),
                        )
                    ),
                    media_type="application/json",
                    status_code=403,
                )
        try:
            provider_cache.add_provider(provider)
            return Response(
                content=json.dumps(
                    dict(
                        message=f"Your provider is successfully added to the system",
                        known_provider=json.dumps(
                            provider_cache.get_provider(provider.id).to_json(), indent=2
                        ),
                    )
                ),
                media_type="application/json",
                status_code=200,
            )
        except ProviderWrongData as e:
            return Response(
                content=json.dumps(
                    dict(
                        message="Something goes wrong with the data of your Provider. Please check the urls, ports, "
                        "and encryption's and try it again!",
                        error=(
                            e.args[0][0],
                            dict(
                                imap=e.args[0][1]["imap"][0],
                                smtp=e.args[0][1]["smtp"][0],
                            ),
                        ),
                    )
                ),
                media_type="application/json",
                status_code=400,
            )

    @fast_api.get("/providers/{provider_id}")
    def get_provider(provider_id: str):
        provider = provider_cache.get_provider(provider_id)
        if provider is None:
            return Response(
                content=json.dumps(
                    dict(
                        message=f"Provider with the id {provider_id} cannot be found!",
                    )
                ),
                media_type="application/json",
                status_code=404,
            )

        return Response(
            content=json.dumps(provider.to_json(), indent=2),
            media_type="application/json",
            status_code=200,
        )

    def process_provider(provider_id: str, provider: Provider):
        if provider_id is None and provider is None:
            return Response(
                content=json.dumps(
                    dict(
                        message="No provider data can be found. Please add a Provider over the providers endpoint or add a Provider object to the request!"
                    )
                ),
                media_type="application/json",
                status_code=400,
            )
        if provider_id and provider is None:
            provider = provider_cache.get_provider(provider_id)
            if provider is None:
                return Response(
                    content=json.dumps(
                        dict(
                            message=f"Provider with the id {provider_id} cannot be found! Please check the id, or add a Provider to the request!"
                        )
                    ),
                    media_type="application/json",
                    status_code=400,
                )
        elif provider is not None:
            if not provider.check_provider()[0]:
                return Response(
                    content=json.dumps(
                        dict(
                            message=f"Your provider is not valid. Please check the urls, ports, and encryptions of the provider and try it again!"
                        ),
                        indent=2,
                    ),
                    media_type="application/json",
                    status_code=400,
                )

        return provider

    @fast_api.post("/provider/check_user")
    def check_user(data: BaseUserData):
        """
        Check the user data on its correctness.
        """
        user: User = data.user
        provider_id: Optional[str] = data.provider_id
        provider: Optional[Provider] = process_provider(provider_id, data.provider)
        if type(provider) is not Provider:
            return provider

        if not user.email or not user.password or not user.login_name:
            return Response(
                content=json.dumps(dict(message="E-Mail, password or login name are empty!",)),
                media_type="application/json",
                status_code=400,
            )

        valid, status = provider.check_user(user)
        if valid:
            return Response(
                content=json.dumps(dict(message="The user data are valid",)),
                media_type="application/json",
                status_code=200,
            )
        else:
            return Response(
                content=json.dumps(
                    dict(
                        message="The user data seems wrong! Please check the login data and the chosen provider and try it again!",
                        status=dict(imap=status["imap"][0], smtp=status["smtp"][0],),
                    )
                ),
                media_type="application/json",
                status_code=400,
            )

    @fast_api.post("/provider/user/send_mail")
    def send_mail(data: SendMail):
        """
        Send a e-mail to somebody
        """
        user: User = data.user
        mail: Mail = data.email
        provider_id: Optional[str] = data.provider_id
        provider: Optional[Provider] = process_provider(provider_id, data.provider)
        if type(provider) is not Provider:
            return provider

        smtp = provider.get_new_smtp_socket()
        smtp.open()
        status, err = smtp.login_user(user)
        if status is not True:
            return Response(
                content=json.dumps(
                    dict(
                        message="We cannot login you. Please check your user data and try it again!",
                    )
                ),
                media_type="application/json",
                status_code=403,
            )
        status, err = smtp.send_mail(mail)
        smtp.close()
        if status is not True:
            return Response(
                content=json.dumps(
                    dict(
                        message="Something goes wrong.",
                        err=str(err),
                        err_type=type(err).__name__,
                    )
                ),
                media_type="application/json",
                status_code=400,
            )
        return Response(
            content=json.dumps(dict(message="E-Mail was send successfully!",)),
            media_type="application/json",
            status_code=200,
        )

    @fast_api.post("/provider/user/get_mails")
    def get_mails(data: GetMails):
        """
        Get all mails from the specified mailboxes which matches with the search string.
        This process needs some time!
        """
        provider_id: Optional[str] = data.provider_id
        provider: Optional[Provider] = process_provider(provider_id, data.provider)
        if type(provider) is not Provider:
            return provider
        user: User = data.user
        mailboxes = data.mailboxes
        move_to_mailbox = data.move_to_mailbox
        imap: ImapSocket = provider.get_new_imap_socket().open()[1]
        status, err = imap.login_user(user)
        if status is not True:
            return Response(
                content=json.dumps(
                    dict(
                        message="We cannot login you. Please check your user data and try it again!",
                    )
                ),
                media_type="application/json",
                status_code=403,
            )
        if len(mailboxes) == 0:
            mailboxes = imap.get_mailboxes()
            if mailboxes is None:
                return Response(
                    content=json.dumps(dict(mails=[])),
                    media_type="application/json",
                    status_code=200,
                )
        search_string = data.search_string.strip()
        if len(search_string) == 0:
            search_string = "ALL"
        try:
            mails = imap.get_all_mails(search_string, mailboxes, move_to_mailbox)
            imap.logout_user()
        except Exception as e:
            return Response(
                content=json.dumps(dict(message=str(e))),
                media_type="application/json",
                status_code=404,
            )
        if not mails:
            return Response(
                content=json.dumps(dict(message=str(mails))),
                media_type="application/json",
                status_code=404,
            )
        tmp_mail = []
        for mail in mails:
            tmp_mail.append(mail.to_json())
        return Response(
            content=json.dumps(dict(mails=tmp_mail)),
            media_type="application/json",
            status_code=200,
        )

    @fast_api.post("/provider/user/get_mailboxes")
    async def get_mailboxes(data: BaseUserData):
        """
        Returns all mailboxes
        """
        provider_id: Optional[str] = data.provider_id
        provider: Optional[Provider] = process_provider(provider_id, data.provider)
        if type(provider) is not Provider:
            return provider
        user: User = data.user
        imap: ImapSocket = provider.get_new_imap_socket().open()[1]
        status, err = imap.login_user(user)
        if status is not True:
            return Response(
                content=json.dumps(
                    dict(
                        message="We cannot login you. Please check your user data and try it again!",
                    )
                ),
                media_type="application/json",
                status_code=403,
            )
        mailboxes = imap.get_mailboxes()
        return Response(
            content=json.dumps(dict(mailboxes=mailboxes)),
            media_type="application/json",
            status_code=200,
        )

    return fast_api


app = init()
