import json

from pydantic import BaseModel

from email_proxy.json_rebuildable import JsonRebuildable
from email_proxy.utils import is_missing, MissingValues


class User(BaseModel, JsonRebuildable):
    login_name: str
    email: str
    password: str

    def __eq__(self, other):
        return (
                self.login_name == other.login_name
                and self.email == other.email
                and self.password == other.password
        )

    @staticmethod
    def from_json(json_obj: str):
        if type(json_obj) == str:
            json_obj = json.loads(json_obj)

        missing = []
        is_missing("login_name", json_obj, missing)
        is_missing("email", json_obj, missing)
        is_missing("password", json_obj, missing)

        if len(missing) > 0:
            raise MissingValues(f"The JSON miss some fields!", missing)

        return User(
            login_name=json_obj.get("login_name"),
            email=json_obj.get("email"),
            password=json_obj.get("password"),
        )

    def to_json(self):
        return dict(login_name=self.login_name, email=self.email, password=self.password, )
