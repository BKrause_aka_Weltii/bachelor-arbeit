from smtplib import (
    SMTP,
    SMTPHeloError,
    SMTPAuthenticationError,
    SMTPNotSupportedError,
    SMTPException,
    SMTPRecipientsRefused,
)
from typing import Any

from pydantic import BaseModel

from email_proxy.email_socket import EmailSocket
from email_proxy.mail import Mail, WrongDateFormat
from email_proxy.user import User


default_timeout = 4


class NobodyIsLoggedInError(BaseException):
    pass


class SocketIsClosedError(BaseException):
    pass


class MissingData(BaseException):
    pass


class NoValidEncryption(BaseException):
    pass


class SmtpSocket(EmailSocket, BaseModel):
    url: str
    port: int
    encryption: str
    user: User = None
    socket: Any = None

    def open(self, timeout=default_timeout):
        if self.socket is None:
            try:
                self.socket = SMTP(self.url, self.port, timeout=timeout)
                if self.encryption == "STARTTLS":
                    self.socket.starttls()
                    return True, self
                elif self.encryption == "SSL_TLS":
                    # FIXME Use SMTP_SSL instead of SMTP!
                    return False, Exception("SSL_TLS is not supported")
                else:
                    return (
                        False,
                        NoValidEncryption(
                            f"{self.encryption} is not a valid encryption!"
                        ),
                    )
            except Exception as e:
                return False, e
        return True, self

    def close(self):
        if self.socket:
            self.socket.close()
            self.socket = None
            return True, None
        else:
            return False, SocketIsClosedError()

    def is_valid(self, timeout=4):
        status, socket = self.open()
        return status, socket

    def login_user(self, user: User):
        if self.socket:
            try:
                self.socket.login(user.login_name, user.password)
                self.user = user
                return True, self
            except SMTPHeloError as e:
                return False, e
            except SMTPAuthenticationError as e:
                return False, e
            except SMTPNotSupportedError as e:
                return False, e
            except SMTPException as e:
                return False, e
        return False, SocketIsClosedError()

    def logout_user(self):
        self.close()

    def send_mail(self, mail: Mail):
        if not self.user:
            return False, NobodyIsLoggedInError()
        if not self.socket:
            return False, SocketIsClosedError()

        from_ = mail.header.get("from", None)
        to = mail.header.get("to", None)
        try:
            message_string = mail.to_raw_mail().as_string()
        except WrongDateFormat as e:
            return False, e
        if not from_:
            return False, Exception("From is missing!")
        if not to:
            return False, Exception("To is missing!")
        if not message_string:
            return False, Exception("The Mail cannot convert to the right format.")
        try:
            self.socket.sendmail(from_, to, message_string)
        except Exception as e:
            return False, e
        return True, self
