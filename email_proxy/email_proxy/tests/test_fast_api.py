import json
from datetime import datetime
from smtplib import SMTP, SMTPAuthenticationError

from fastapi.testclient import TestClient

from email_proxy.fast_api import app, provider_cache
from email_proxy.tests.test_imap_socket import mock_imap4_ssl
from email_proxy.tests.test_smtp_socket import mock_smtp

client = TestClient(app)
provider_cache.clear()


def get_date():
    form = datetime.now().strftime("%a, %d %b %Y %H:%M:%S +0100")
    return form


def generate_provider_dict(id: str = "example"):
    return {
        "id": id,
        "name": "Example",
        "base_url": "example.net",
        "imap_url": "imap.example.net",
        "imap_port": 993,
        "imap_encryption": "SSL_TLS",
        "smtp_url": "smtp.example.net",
        "smtp_port": 587,
        "smtp_encryption": "STARTTLS",
    }


def generate_user_dict():
    return dict(
        login_name="herbert@example.net",
        email="herbert@example.net",
        password="herbert_is-the_best",
    )


def generate_mail_dict():
    mail = dict(
        header=dict(
            date=get_date(),
            subject="",
            to=["stickman.herbert@example.org", "stickman.nobody@example.org"],
            some_extra_field="Which contains no relevant information",
            content_type='text/plain; charset="utf-8"',
        ),
        body="Hey, I'm a body :)",
    )
    mail["header"]["from"] = "stickman@example.org"
    return mail


def test_ping():
    response = client.get("/ping")
    assert response.content.decode("utf-8") == '"pong"'


def test_ding():
    response = client.get("/ding")
    assert response.content.decode("utf-8") == '"dong dongelong"'


#########################################################################################'
# get_providers
#########################################################################################'


def test_get_providers(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    response = client.get("/providers")
    assert response.status_code == 200
    assert response.json() == []


def test_get_providers_multiple(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    response = client.get("/providers")
    assert response.status_code == 200
    assert response.json() == []

    client.post(
        "/providers", json=generate_provider_dict("example_2"),
    )

    response = client.get("/providers")
    assert response.status_code == 200
    assert response.json() == [generate_provider_dict("example_2")]


#########################################################################################'
# post_providers
#########################################################################################'


def test_post_providers_success(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    response = client.post("/providers", json=generate_provider_dict(),)
    assert response.status_code == 200


def test_post_providers_multiple_times_success(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    response = client.post("/providers", json=generate_provider_dict("provicer_1"),)
    assert response.status_code == 200
    response = client.post("/providers", json=generate_provider_dict("provicer_2"),)
    assert response.status_code == 200
    response = client.post("/providers", json=generate_provider_dict("provicer_3"),)
    assert response.status_code == 200
    response = client.post("/providers", json=generate_provider_dict("provicer_4"),)
    assert response.status_code == 200

    response = client.get("/providers")
    assert response.status_code == 200
    assert len(json.loads(response.text)) == 6


def test_post_providers_failed_already_known(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    provider = generate_provider_dict()
    provider["smtp-url"] = "other.domain.net"
    response = client.post("/providers", json=provider)
    assert response.status_code == 200


def test_post_providers_failed_invalid_urls(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)

    def ret(self, host="", port=0, local_hostname=None, timeout=0, source_address=None):
        raise Exception("timed out")

    monkeypatch.setattr(SMTP, "__init__", ret)
    provider = generate_provider_dict()
    provider["id"] = "other"
    response = client.post("/providers", json=provider)
    js_obj = response.json()
    assert response.status_code == 400
    assert (
        js_obj["message"]
        == "Something goes wrong with the data of your Provider. Please check the urls, ports, and encryption's and try it again!"
    )
    assert js_obj["error"] == [False, dict(imap=True, smtp=False)]


#########################################################################################'
# get_provider
#########################################################################################'


def test_get_provider_success(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)

    provider = generate_provider_dict()
    client.post("/providers", json=provider)
    response = client.get(f"providers/{provider['id']}")
    assert response.status_code == 200
    assert response.json() == provider


def test_get_provider_failed_not_found(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    provider = generate_provider_dict()
    client.post("/providers", json=provider)
    response = client.get(f"providers/something_different")
    assert response.status_code == 404
    assert (
        response.json()["message"]
        == f"Provider with the id something_different cannot be found!"
    )


#########################################################################################'
# check_user
#########################################################################################'


def test_check_user_success_no_cached_provider(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    provider = generate_provider_dict("example_5")
    payload = dict(user=generate_user_dict(), provider=provider)
    response = client.post("/provider/check_user", json=payload)
    assert response.status_code == 200


def test_check_user_failed_no_cached_provider_provider_is_invalid(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)

    def ret(self, host="", port=0, local_hostname=None, timeout=0, source_address=None):
        raise Exception("timed out")

    monkeypatch.setattr(SMTP, "__init__", ret)
    provider = generate_provider_dict("example_5")
    payload = dict(user=generate_user_dict(), provider=provider)
    response = client.post("/provider/check_user", json=payload)
    res_json = response.json()
    assert response.status_code == 400
    assert (
        res_json["message"]
        == "Your provider is not valid. Please check the urls, ports, and encryptions of the provider and try it again!"
    )


def test_check_user_failed_no_cached_provider_user_is_invalid(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)

    def smtp_login_mock(self, user, password, *, initial_response_ok=True):
        raise SMTPAuthenticationError(401, "Your username and password are incorrect")

    monkeypatch.setattr(SMTP, "login", smtp_login_mock)
    provider = generate_provider_dict("example_5")
    payload = dict(user=generate_user_dict(), provider=provider)
    response = client.post("/provider/check_user", json=payload)
    res_json = response.json()
    assert response.status_code == 400
    assert (
        res_json["message"]
        == "The user data seems wrong! Please check the login data and the chosen provider and try it again!"
    )
    assert res_json["status"] == dict(imap=True, smtp=False)


def test_check_user_success_cached_provider(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    provider_id = "example_6"
    client.post(
        "/providers", json=generate_provider_dict(provider_id),
    )
    payload = dict(user=generate_user_dict(), provider_id=provider_id)
    response = client.post("/provider/check_user", json=payload)
    assert response.status_code == 200


def test_check_user_failed_cached_provider_not_found(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    provider_id = "the_id_can_not_be_found!"
    payload = dict(user=generate_user_dict(), provider_id=provider_id)
    response = client.post("/provider/check_user", json=payload)
    res_json = response.json()
    assert response.status_code == 400
    assert (
        res_json["message"]
        == f"Provider with the id {provider_id} cannot be found! Please check the id, or add a Provider to the request!"
    )


def test_check_user_failed_no_provider(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    payload = dict(user=generate_user_dict(),)
    response = client.post("/provider/check_user", json=payload)
    res_json = response.json()
    assert response.status_code == 400
    assert (
        res_json["message"]
        == "No provider data can be found. Please add a Provider over the providers endpoint or add a Provider object to the request!"
    )


#########################################################################################'
# send_mail
#########################################################################################'


def test_send_mail_success(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    provider = generate_provider_dict("example_7")
    payload = dict(
        user=generate_user_dict(), provider=provider, email=generate_mail_dict()
    )
    response = client.post("/provider/user/send_mail", json=payload)
    assert response.status_code == 200
    assert response.json()["message"] == "E-Mail was send successfully!"


def test_send_mail_failed_login_failed(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)

    def smtp_login_mock(self, user, password, *, initial_response_ok=True):
        raise SMTPAuthenticationError(401, "Your username and password are incorrect")

    monkeypatch.setattr(SMTP, "login", smtp_login_mock)
    provider = generate_provider_dict("example_7")
    payload = dict(
        user=generate_user_dict(), provider=provider, email=generate_mail_dict()
    )
    response = client.post("/provider/user/send_mail", json=payload)
    assert response.status_code == 403
    assert (
        response.json()["message"]
        == "We cannot login you. Please check your user data and try it again!"
    )


def test_send_mail_failed_wrong_date(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    provider = generate_provider_dict("example_7")
    mail = generate_mail_dict()
    mail["header"]["date"] = datetime.now().strftime("%a, %d %b %Y %H:%M")
    payload = dict(user=generate_user_dict(), provider=provider, email=mail)
    response = client.post("/provider/user/send_mail", json=payload)
    resp_json = response.json()
    assert response.status_code == 400
    assert resp_json["message"] == "Something goes wrong."
    assert resp_json["err_type"] == "WrongDateFormat"
