import json
from imaplib import IMAP4_SSL, IMAP4
from smtplib import SMTP, SMTPAuthenticationError

import pytest

from email_proxy.provider import Provider, MissingValues
from email_proxy.imap_socket import ImapSocket
from email_proxy.smtp_socket import SmtpSocket
from email_proxy.tests.test_imap_socket import mock_imap4_ssl
from email_proxy.tests.test_smtp_socket import mock_smtp


def generate_user():
    from email_proxy.user import User

    return User(
        login_name="herbert@example.net",
        email="herbert@example.net",
        password="herbert_is-the_best",
    )


def generate_provider():
    return Provider(
        id="example",
        name="Example",
        base_url="example.net",
        imap_url="imap.example.net",
        imap_port=547,
        imap_encryption="SSL_TLS",
        smtp_url="smtp.example.net",
        smtp_port=983,
        smtp_encryption="STARTTLS",
    )


def test_to_json():
    assert_string = json.dumps(
        dict(
            id="example",
            name="Example",
            base_url="example.net",
            imap_url="imap.example.net",
            imap_port=547,
            imap_encryption="SSL_TLS",
            smtp_url="smtp.example.net",
            smtp_port=983,
            smtp_encryption="STARTTLS",
        ),
        indent=2,
    )

    provider = json.dumps(generate_provider().to_json(), indent=2)

    assert provider == assert_string


def test_from_json_valid():
    string = json.dumps(
        dict(
            id="example",
            name="Example",
            base_url="example.net",
            imap_url="imap.example.net",
            imap_port=547,
            imap_encryption="SSL_TLS",
            smtp_url="smtp.example.net",
            smtp_port=983,
            smtp_encryption="STARTTLS",
        ),
        indent=2,
    )
    provider = Provider.from_json(string)
    assert provider is not None
    assert json.dumps(provider.to_json(), indent=2) == string
    object_ = dict(
        id="example",
        name="Example",
        base_url="example.net",
        imap_url="imap.example.net",
        imap_port=547,
        imap_encryption="SSL_TLS",
        smtp_url="smtp.example.net",
        smtp_port=983,
        smtp_encryption="STARTTLS",
    )
    provider = Provider.from_json(object_)
    assert provider is not None
    assert json.dumps(provider.to_json(), indent=2) == string


def test_from_json_failed():
    string = '{"id": "example"}'
    with pytest.raises(BaseException) as e:
        assert Provider.from_json(string) is None
    assert e.typename == "MissingValues"
    assert e.value.args[0] == "The JSON miss some fields!"
    assert e.value.args[1] == [
        "name",
        "base_url",
        "imap_url",
        "imap_port",
        "imap_encryption",
        "smtp_url",
        "smtp_port",
        "smtp_encryption",
    ]


def test_check_provider_success(monkeypatch):
    mock_imap4_ssl(monkeypatch)
    mock_smtp(monkeypatch)
    provider = generate_provider()
    assert provider.check_provider()[0] is True


def test_check_provider_failed(monkeypatch):
    def imap_is_valid_mock(self):
        return False, Exception("Connection refused")

    def smtp_is_valid_mock(self):
        return False, Exception("Connection refused")

    monkeypatch.setattr(ImapSocket, "is_valid", imap_is_valid_mock)
    monkeypatch.setattr(SmtpSocket, "is_valid", smtp_is_valid_mock)
    mock_imap4_ssl(monkeypatch)
    mock_smtp(monkeypatch)
    provider = generate_provider()
    status, data = provider.check_provider()
    assert not status
    assert data.get("imap")[0] is False
    assert data.get("imap")[1].args[0] == "Connection refused"
    assert data.get("smtp")[0] is False
    assert data.get("smtp")[1].args[0] == "Connection refused"


def test_check_user_success(monkeypatch):
    mock_imap4_ssl(monkeypatch)
    mock_smtp(monkeypatch)
    provider = generate_provider()
    user = generate_user()
    valid, status = provider.check_user(user)
    assert valid is True
    assert status.get("imap")[0] is True
    assert type(status.get("imap")[1]) == ImapSocket
    assert status.get("smtp")[0] is True
    assert type(status.get("smtp")[1]) == SmtpSocket


def test_check_user_one_failed(monkeypatch):
    # FIXME did i have the case, that a user has different imap and smtp credentials?
    mock_imap4_ssl(monkeypatch)
    mock_smtp(monkeypatch)

    def smtp_login_mock(self, user, password, *, initial_response_ok=True):
        raise SMTPAuthenticationError(401, "Your username and password are incorrect")

    monkeypatch.setattr(SMTP, "login", smtp_login_mock)

    provider = generate_provider()
    user = generate_user()
    valid, status = provider.check_user(user)
    assert valid is False
    assert status.get("imap")[0] is True
    assert type(status.get("imap")[1]) == ImapSocket
    assert status.get("smtp")[0] is False
    assert type(status.get("smtp")[1]) == SMTPAuthenticationError
    assert status.get("smtp")[1].args[0] == 401
    assert status.get("smtp")[1].args[1] == "Your username and password are incorrect"


def test_check_user_failed(monkeypatch):
    # FIXME did i have the case, that a user has different imap and smtp credentials?
    mock_imap4_ssl(monkeypatch)
    mock_smtp(monkeypatch)

    def smtp_login_mock(self, user, password, *, initial_response_ok=True):
        raise SMTPAuthenticationError(401, "Your username and password are incorrect")

    def imap_login_mock(self, user, password):
        raise IMAP4_SSL.error("Your username and password are incorrect")

    monkeypatch.setattr(SMTP, "login", smtp_login_mock)
    monkeypatch.setattr(IMAP4_SSL, "login", imap_login_mock)

    provider = generate_provider()
    user = generate_user()
    valid, status = provider.check_user(user)
    assert valid is False
    assert status.get("imap")[0] is False
    assert type(status.get("imap")[1]) == IMAP4.error
    assert status.get("imap")[1].args[0] == "Your username and password are incorrect"
    assert status.get("smtp")[0] is False
    assert type(status.get("smtp")[1]) == SMTPAuthenticationError
    assert status.get("smtp")[1].args[0] == 401
    assert status.get("smtp")[1].args[1] == "Your username and password are incorrect"


def test_get_new_smtp_socket():
    provider = generate_provider()
    smtp = provider.get_new_smtp_socket()
    assert smtp.url == provider.smtp_url
    assert smtp.port == provider.smtp_port
    assert smtp.encryption == provider.smtp_encryption


def test_get_new_smtp_socket():
    provider = generate_provider()
    imap = provider.get_new_imap_socket()
    assert imap.url == provider.imap_url
    assert imap.port == provider.imap_port
    assert imap.encryption == provider.imap_encryption
