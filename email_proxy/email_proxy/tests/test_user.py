import json

from email_proxy.user import User


def test_equals_success():
    user1 = User(
        login_name="herbert@example.net",
        email="herbert@example.net",
        password="herbert_is-the_best!",
    )
    assert user1.__eq__(user1) is True


def test_equals_failed():
    user1 = User(
        login_name="herbert@example.net",
        email="herbert@example.net",
        password="herbert_is-the_best!",
    )

    user2 = User(
        login_name="sophie@example.net",
        email="sophie@example.net",
        password="sophie_is-better_than-herbert!",
    )

    assert user1.__eq__(user2) is False
    assert user1.__eq__(user1) is True


def test_from_json_success():
    json_obj = json.dumps(
        dict(
            login_name="sophie@example.net",
            email="sophie@example.net",
            password="sophie_is-better_than-herbert!",
        )
    )

    assert_user = User(
        login_name="sophie@example.net",
        email="sophie@example.net",
        password="sophie_is-better_than-herbert!",
    )

    assert assert_user.__eq__(User.from_json(json_obj)) is True


def test_from_json_failed():
    json_obj = json.dumps(
        dict(
            login_name="herbert@example.net",
            email="herbert@example.net",
            password="herbert_is-the_best!",
        )
    )

    assert_user = User(
        login_name="sophie@example.net",
        email="sophie@example.net",
        password="sophie_is-better_than-herbert!",
    )

    assert assert_user.__eq__(User.from_json(json_obj)) is False


def test_to_json():
    assert_user = User(
        login_name="sophie@example.net",
        email="sophie@example.net",
        password="sophie_is-better_than-herbert!",
    )

    assert_string = json.dumps(
        dict(
            login_name="sophie@example.net",
            email="sophie@example.net",
            password="sophie_is-better_than-herbert!",
        ),
        indent=2,
    )

    assert json.dumps(assert_user.to_json(), indent=2) == assert_string
