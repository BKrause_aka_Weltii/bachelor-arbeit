from imaplib import IMAP4_SSL, IMAP4

import pytest

from email_proxy.provider import Provider
from email_proxy.provider_cache import (
    ProviderCache,
    ProviderWrongData,
    AlreadyKnownError,
)
from email_proxy.tests.test_imap_socket import mock_imap4_ssl
from email_proxy.tests.test_smtp_socket import mock_smtp


def generate_provider(id_overwride: str = "example"):
    return Provider(
        id=id_overwride,
        name="Example",
        base_url="example.net",
        imap_url="imap.example.net",
        imap_port=547,
        imap_encryption="SSL_TLS",
        smtp_url="smtp.example.net",
        smtp_port=983,
        smtp_encryption="STARTTLS",
    )


def test_get_provider():
    cache = ProviderCache(providers=dict(example=generate_provider()))
    assert cache.get_provider("example") is not None
    assert cache.get_provider("example_2") is None


def test_has_provider():
    cache = ProviderCache(providers=dict(example=generate_provider()))
    assert cache.has_provider("example")
    assert not cache.has_provider("example_2")


def test_add_provider_success(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    cache = ProviderCache()
    provider = generate_provider()
    cache.add_provider(provider)
    assert len(cache.get_all_providers().keys()) == 1
    assert cache.get_provider("example") is not None


def test_add_provider_failed_already_exists(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)
    cache = ProviderCache()
    provider = generate_provider()
    cache.add_provider(provider)
    assert len(cache.get_all_providers().keys()) == 1
    assert cache.get_provider("example") is not None

    with pytest.raises(AlreadyKnownError) as e:
        cache.add_provider(provider)
    assert e.type == AlreadyKnownError


def test_add_provider_failed_wrong_data(monkeypatch):
    mock_smtp(monkeypatch)
    mock_imap4_ssl(monkeypatch)

    def init_mock(self, host="", port=0, keyfile=None, certfile=None, ssl_context=None):
        raise IMAP4.error("Someting is wrong")

    monkeypatch.setattr(IMAP4_SSL, "__init__", init_mock)

    cache = ProviderCache()
    provider = generate_provider()

    with pytest.raises(ProviderWrongData) as e:
        cache.add_provider(provider)
    assert e.type == ProviderWrongData
    assert e.value.args[0][1].get("imap")[0] is False
    assert e.value.args[0][1].get("imap")[1].args[0] == "Someting is wrong"
    assert e.value.args[0][1].get("smtp")[0] is True
