from smtplib import SMTP, SMTPAuthenticationError

from email_proxy.mail import Mail
from email_proxy.smtp_socket import SmtpSocket

from email_proxy.user import User


def generate_smtp_socket(
    url: str = "smtp.example.net", port: int = 587, encryption: str = "STARTTLS"
):
    return SmtpSocket(url=url, port=port, encryption=encryption)


def generate_mail():
    mail = Mail(
        header=dict(to=["guenther@example.org", "john@example.org"]), body="Hey :)"
    )
    mail.header["from"] = "herbert@example.org"
    return mail


def mock_smtp(monkeypatch):
    def init_mock(
        self, host="", port=0, local_hostname=None, timeout=2, source_address=None
    ):
        pass

    def close_mock(self):
        pass

    def login_mock(self, user, password, *, initial_response_ok=True):
        return True

    def send_message_mock(
        self, msg, from_addr=None, to_addrs=None, mail_options=(), rcpt_options=()
    ):
        pass

    def startls_mock(self):
        pass

    monkeypatch.setattr(SMTP, "__init__", init_mock)
    monkeypatch.setattr(SMTP, "close", close_mock)
    monkeypatch.setattr(SMTP, "login", login_mock)
    monkeypatch.setattr(SMTP, "sendmail", send_message_mock)
    monkeypatch.setattr(SMTP, "starttls", startls_mock)


def generate_user():
    return User(
        login_name="herbert@example.net",
        email="herbert@example.net",
        password="herbert_is-the_best",
    )


def test_open_success(monkeypatch):
    mock_smtp(monkeypatch)
    smtp_socket = generate_smtp_socket()
    status, socket = smtp_socket.open()
    assert status is True
    assert socket is not None


def test_open_failed_wrong_url(monkeypatch):
    def init_mock(
        elf, host="", port=0, local_hostname=None, timeout=2, source_address=None
    ):
        raise Exception(-2, "Name or service not known")

    monkeypatch.setattr(SMTP, "__init__", init_mock)
    smtp_socket = generate_smtp_socket()
    status, exception = smtp_socket.open()
    assert status is False
    assert exception.args[0] == -2
    assert exception.args[1] == "Name or service not known"


def test_open_failed_wrong_port(monkeypatch):
    def ret(self, host="", port=0, local_hostname=None, timeout=0, source_address=None):
        raise Exception("timed out")

    monkeypatch.setattr(SMTP, "__init__", ret)
    smtp_socket = generate_smtp_socket()
    status, exception = smtp_socket.open(2)
    assert status is False
    assert exception.args[0] == "timed out"


def test_open_failed_wrong_encryption(monkeypatch):
    mock_smtp(monkeypatch)
    encryption = "this_is_not_a_encryption_method^^"
    encryption_2 = "SSL_TLS"
    smtp_socket = generate_smtp_socket(encryption=encryption)
    status, exception = smtp_socket.open()
    assert status is False
    assert exception.args[0] == f"{encryption} is not a valid encryption!"

    smtp_socket = generate_smtp_socket(encryption=encryption_2)
    status, exception = smtp_socket.open()
    assert status is False
    assert exception.args[0] == "SSL_TLS is not supported"


def test_is_valid():
    # can be skipped, same functionality as open
    pass


def test_login_user(monkeypatch):
    mock_smtp(monkeypatch)
    user = generate_user()
    socket = generate_smtp_socket()
    socket.open()
    status, sock = socket.login_user(user)
    assert status is True
    assert sock is not None
    assert socket.user is user


def test_login_user_is_invalid(monkeypatch):
    mock_smtp(monkeypatch)

    def login(self, user, password, *, initial_response_ok=True):
        raise SMTPAuthenticationError(401, "Your username and password are not correct")

    monkeypatch.setattr(SMTP, "login", login)
    smtp = generate_smtp_socket()
    smtp.open()
    status, exception = smtp.login_user(generate_user())
    assert status is False
    assert exception.args[0] == 401
    assert exception.args[1] == "Your username and password are not correct"


def test_send_valid_mail(monkeypatch):
    mock_smtp(monkeypatch)
    smtp = generate_smtp_socket()
    smtp.open()
    smtp.login_user(generate_user())
    status = smtp.send_mail(generate_mail())[0]
    assert status is True


def test_send_no_user(monkeypatch):
    # TODO write more specific test
    mock_smtp(monkeypatch)
    smtp = generate_smtp_socket()
    smtp.open()
    smtp.login_user(generate_user())
    status = smtp.send_mail(generate_mail())[0]
    assert status is True


def test_logout_user(monkeypatch):
    mock_smtp(monkeypatch)
    smtp = generate_smtp_socket()
    assert smtp.socket is None
    smtp.close()
    assert smtp.socket is None
    smtp.open()
    assert smtp.socket is not None
    smtp.close()
    assert smtp.socket is None


def test_close(monkeypatch):
    mock_smtp(monkeypatch)
    socket = generate_smtp_socket()
    assert socket.socket is None
    socket.close()
    assert socket.socket is None
    socket.open()
    assert socket.socket is not None
    socket.close()
    assert socket.socket is None
