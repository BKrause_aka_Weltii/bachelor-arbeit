import imaplib
import json
from imaplib import IMAP4_SSL
from typing import Any


from email_proxy.imap_socket import ImapSocket
from email_proxy.mail import Mail
from email_proxy.user import User


def generate_user():
    return User(
        login_name="herbert@example.net",
        email="herbert@example.net",
        password="herbert_is-the_best",
    )


def generate_imap_socket():
    return ImapSocket(url="imap.example.net", port=587, encryption="SSL_TLS",)


def generate_mail():
    mail = Mail(
        header=dict(
            subject="A nice mail",
            to=["sophie@example.net"],
            references="<1234.56789.10@example.net>",
            date="Sun, 20 Sep 2020 12:08:42 +0200",
        ),
        body="Hey Sophie, this is a message",
    )
    mail.header["return_path"] = "<john@example.net>"
    mail.header["message_id"] = "<hasHasdl.AHujklc2123@example.net>"
    mail.header["mime_version"] = "1.0"
    mail.header["content_type"] = 'text/plain; charset="utf-8"; format="flowed"'
    mail.header["content_language"] = "de-DE"
    mail.header["from"] = "john@example.net"
    return mail


def generate_byte_mail():
    return (
        b"1 (RFC822 {9593}",
        b"Return-Path: <john@example.net>\r\n"
        b"Subject: A nice mail\r\n"
        b"To: sophie@example.net\r\n"
        b"References: <1234.56789.10@example.net>\r\n"
        b"From: john@example.net\r\n"
        b"Message-ID: <hasHasdl.AHujklc2123@example.net>\r\n"
        b"Date: Sun, 20 Sep 2020 12:08:42 +0200\r\n"
        b"MIME-Version: 1.0\r\n"
        b"Content-Type: text/plain; charset=utf-8; format=flowed\r\n"
        b"Content-Language: de-DE\r\n"
        b"\r\n"
        b"Hey Sophie, this is a message",
    )


def mock_imap4_ssl(monkeypatch):
    def fetch_mock(self, message_set, message_parts):
        return "status", [generate_byte_mail(), b")"]

    def list_mock(self):
        return (
            "OK",
            [
                bytes('(\\NoInferiors) "/" Archive', "utf-8"),
                bytes('(\\Drafts \\NoInferiors) "/" Drafts', "utf-8"),
                bytes('(\\Sent \\NoInferiors) "/" Sent', "utf-8"),
                bytes('(\\NoInferiors) "/" INBOX', "utf-8"),
                bytes('(\\Trash \\NoInferiors) "/" Trash', "utf-8"),
                bytes('(\\Junk \\NoInferiors) "/" Spam', "utf-8"),
                bytes('(\\NoInferiors) "/" Unknown', "utf-8"),
            ],
        )

    def init_mock(self, host="", port=0, keyfile=None, certfile=None, ssl_context=None):
        pass

    def login_mock(self, user, password):
        pass

    def logout_mock(self):
        pass

    def starttls_mock():
        pass

    def select_mock(self, mailbox="INBOX", readonly=False):
        pass

    def search_mock(self, string: Any, pos: int = ..., endpos: int = ...):
        return [generate_byte_mail(), generate_byte_mail()]

    def close_mock(self):
        pass

    monkeypatch.setattr(IMAP4_SSL, "fetch", fetch_mock)
    monkeypatch.setattr(IMAP4_SSL, "list", list_mock)
    monkeypatch.setattr(IMAP4_SSL, "__init__", init_mock)
    monkeypatch.setattr(IMAP4_SSL, "login", login_mock)
    monkeypatch.setattr(IMAP4_SSL, "logout", logout_mock)
    monkeypatch.setattr(IMAP4_SSL, "starttls", starttls_mock)
    monkeypatch.setattr(IMAP4_SSL, "select", select_mock)
    monkeypatch.setattr(IMAP4_SSL, "search", search_mock)
    monkeypatch.setattr(IMAP4_SSL, "close", close_mock)


def test_open_success(monkeypatch):
    mock_imap4_ssl(monkeypatch)

    smtp = generate_imap_socket()
    status, socket = smtp.open()

    assert status is True
    assert type(socket.socket) == IMAP4_SSL


def test_open_failed(monkeypatch):
    def init_mock(self, host="", port=0, keyfile=None, certfile=None, ssl_context=None):
        raise imaplib.IMAP4.error("Someting is wrong")

    monkeypatch.setattr(IMAP4_SSL, "__init__", init_mock)

    smtp = generate_imap_socket()
    status, socket = smtp.open()

    assert status is False
    assert socket.args[0] == "Someting is wrong"


def test_open_failed_timeout(monkeypatch):
    def init_mock(self, host="", port=0, keyfile=None, certfile=None, ssl_context=None):
        raise Exception("timed out")

    monkeypatch.setattr(IMAP4_SSL, "__init__", init_mock)

    smtp = generate_imap_socket()
    status, socket = smtp.open()

    assert status is False
    assert socket.args[0] == "timed out"


def test_close(monkeypatch):
    mock_imap4_ssl(monkeypatch)

    smtp = generate_imap_socket()
    status, socket = smtp.open()

    assert status is True
    assert smtp.socket is not None

    smtp.close()

    assert smtp.socket is None


def test_is_valid_success(monkeypatch):
    mock_imap4_ssl(monkeypatch)
    smtp = generate_imap_socket()
    assert smtp.is_valid()[0] is True


def test_is_valid_failed(monkeypatch):
    def init_mock(self, host="", port=0, keyfile=None, certfile=None, ssl_context=None):
        raise imaplib.IMAP4.error("Someting is wrong")

    monkeypatch.setattr(IMAP4_SSL, "__init__", init_mock)
    smtp = generate_imap_socket()
    assert smtp.is_valid()[0] is False


def test_login_user_success(monkeypatch):
    mock_imap4_ssl(monkeypatch)
    smtp = generate_imap_socket()
    user = generate_user()
    smtp.open()
    status = smtp.login_user(user)[0]

    assert status is True
    assert smtp.user.__eq__(user)


def test_login_user_failed(monkeypatch):
    def login_mock(self, user, password):
        raise IMAP4_SSL.error("Your login data is wrong!")

    mock_imap4_ssl(monkeypatch)
    monkeypatch.setattr(IMAP4_SSL, "login", login_mock)
    smtp = generate_imap_socket()
    user = generate_user()
    smtp.open()
    status, error = smtp.login_user(user)
    assert status is False
    assert error.args[0] == "Your login data is wrong!"


def test_logout_user(monkeypatch):
    mock_imap4_ssl(monkeypatch)
    smtp = generate_imap_socket()
    user = generate_user()
    smtp.open()
    status = smtp.login_user(user)[0]

    assert status is True
    assert smtp.user.__eq__(user)

    smtp.logout_user()
    assert smtp.user is None


def test_mailboxes(monkeypatch):
    mock_imap4_ssl(monkeypatch)
    expected_value = ["Archive", "Drafts", "Sent", "INBOX", "Trash", "Spam", "Unknown"]

    socket = generate_imap_socket()
    socket.open()
    socket.login_user(generate_user())

    mailboxes = socket.get_mailboxes()
    assert mailboxes == expected_value


def test_fetch_mail(monkeypatch):
    mock_imap4_ssl(monkeypatch)
    socket = generate_imap_socket()
    socket.open()
    socket.login_user(generate_user())
    socket.socket.select("INBOX")
    mail = socket.fetch_mail("1")
    mail_header = json.dumps(mail.header, sort_keys=True)
    assert_mail = generate_mail()
    assert_mail_header = json.dumps(assert_mail.header, sort_keys=True)

    assert mail_header == assert_mail_header
    assert mail.body == assert_mail.body


def test_get_all_mails(monkeypatch):
    mock_imap4_ssl(monkeypatch)
    socket = generate_imap_socket()
    socket.open()
    socket.login_user(generate_user())

    mails = socket.get_all_mails("", ["DeltaChat"])
    assert_mails = [generate_mail(), generate_mail(), generate_mail()]

    assert mails == assert_mails
