import pytest
from datetime import datetime
from email.message import EmailMessage

from email_proxy.mail import Mail, WrongDateFormat


def get_date():
    form = datetime.now().strftime("%a, %d %b %Y %H:%M:%S +0100")
    return form


def gen_raw_email():
    mail = EmailMessage()
    mail.set_content("Hey, I'm a body :)")
    mail["subject"] = ""
    mail["from"] = "stickman@example.org"
    mail["to"] = ["stickman.herbert@example.org", "stickman.nobody@example.org"]
    mail["some-extra-field"] = "Which contains no relevant information"
    mail["date"] = get_date()
    mail.replace_header("content-type", 'text/plain; charset="utf-8" flow="straight"')
    return mail


def gen_mail():
    mail = Mail(
        header=dict(
            date=get_date(),
            subject="",
            to=["stickman.herbert@example.org", "stickman.nobody@example.org"],
            some_extra_field="Which contains no relevant information",
            content_type='text/plain; charset="utf-8"',
        ),
        body="Hey, I'm a body :)",
    )
    mail.header["from"] = "stickman@example.org"
    return mail


def test_from_raw_mail():
    mail_1: Mail = gen_mail()
    mail_2: Mail = Mail.from_raw_mail(gen_raw_email())
    for key in mail_1.header.keys():
        if not key == "date":  # FIXME dates are hard to parse
            assert mail_1.header[key] == mail_2.header[key]


def test_to_raw_mail():
    mail: Mail = gen_mail()
    raw_mail = mail.to_raw_mail()
    checked_raw_mail = gen_raw_email()
    for key in raw_mail.keys():
        assert raw_mail.get_param(key) == checked_raw_mail.get_param(key)
    assert raw_mail.get_content() == checked_raw_mail.get_content()


def test_to_raw_mail_wrong_date_format():
    mail: Mail = gen_mail()
    mail.header["date"] = datetime.now().strftime("%a, %d %b %Y %H:%M")

    with pytest.raises(WrongDateFormat) as e:
        raw_mail = mail.to_raw_mail()
    assert e.type == WrongDateFormat


def test_to_raw_mail_date_from_unix_time_stamp_will_fail():
    mail: Mail = gen_mail()
    mail.header["date"] = datetime.now().timestamp()
    with pytest.raises(WrongDateFormat) as e:
        raw_mail = mail.to_raw_mail()
    assert e.type == WrongDateFormat


def test_additional_header_fields():
    mail: Mail = gen_mail()
    mail.header["additional_header_fields"] = dict(
        list_field=["someData1", "someData2"]
    )
    expected_raw_mail = mail.to_raw_mail()
    expected_raw_mail["list-field"] = "someData1, someData2"
    for key in mail.to_raw_mail().keys():
        assert expected_raw_mail.get_param(key) == expected_raw_mail.get_param(key)
    assert expected_raw_mail.get_content() == expected_raw_mail.get_content()
