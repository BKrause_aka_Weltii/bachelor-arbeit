from abc import ABC, abstractmethod


class JsonRebuildable(ABC):
    @staticmethod
    @abstractmethod
    def from_json(json: str):
        pass

    @abstractmethod
    def to_json(self):
        pass
