import imaplib
import ssl
from email import message_from_bytes, policy
from email.message import EmailMessage
from imaplib import IMAP4_SSL
import socket
from typing import Any

from pydantic import BaseModel

from email_proxy.email_socket import EmailSocket
from email_proxy.mail import Mail
from email_proxy.user import User


default_timeout = 4


class SocketIsNotOpened(BaseException):
    pass


class ImapSocket(EmailSocket, BaseModel):
    url: str
    port: int
    encryption: str
    user: User = None
    socket: Any = None

    def open(self, timeout=default_timeout) -> (bool, IMAP4_SSL):
        if not self.socket:
            try:
                socket.setdefaulttimeout(timeout)
                if self.encryption == "SSL_TLS":
                    context = ssl.create_default_context()
                    self.socket = IMAP4_SSL(
                        host=self.url, port=self.port, ssl_context=context
                    )
                    return True, self
                else:
                    self.socket = imaplib.IMAP4_SSL(host=self.url, port=self.port,)
                    self.socket.starttls()
                    return True, self
            except imaplib.IMAP4.error as e:
                return False, e
            except imaplib.IMAP4.abort as e:
                return False, e
            except Exception as e:
                return False, e

    def close(self):
        if self.socket:
            self.socket.close()
            self.socket = None

    def is_valid(self) -> (bool, Any):
        return self.open()

    def login_user(self, user: User) -> (bool, IMAP4_SSL):
        try:
            if self.socket:
                self.socket.login(user.login_name, user.password)
                self.user = user
                return True, self
        except IMAP4_SSL.error as e:
            return False, e
        return False, SocketIsNotOpened

    def logout_user(self):
        if self.user:
            self.user = None
            self.socket.logout()

    def get_mailboxes(self):
        if self.user and self.socket:
            byte_mailboxes = self.socket.list()
            mailboxes = []

            for bm in byte_mailboxes[1]:
                mailboxes.append(str(bm, "utf-8").split('"/"')[1].strip())

            return mailboxes
        return None

    def fetch_mail(self, id: str):
        if not self.user:
            raise Exception(
                "No user is logged in. Please login a user before calling this method"
            )
        if not self.socket:
            raise Exception(
                "No socket is open. Please open a socket beofre calling this mehtod"
            )

        status, mail = self.socket.fetch(id, "(RFC822)")
        m = Mail.from_raw_mail(
            message_from_bytes(mail[0][1], EmailMessage, policy=policy.SMTP)
        )
        return m

    def get_all_mails(self, search_string: str, mailboxes: [str], move_to_mailbox: str = "Spam"):
        """
        Get all mails from the specified mailboxes which matches with the search string.
        This process needs some time!
        :param search_string:
        :param mailboxes:
        :return:
        """
        if not self.user:
            raise Exception(
                "No user is logged in. Please login a user before calling this method"
            )
        if not self.socket:
            raise Exception(
                "No socket is open. Please open a socket before calling this method"
            )

        search_string = "ALL" if search_string.strip() == "" else search_string
        mails = []

        for mailbox in mailboxes:
            self.socket.select(mailbox)
            status, byte_messages_ids = self.socket.search(None, search_string)
            if not byte_messages_ids[0] == b"":
                byte_messages_ids = byte_messages_ids[0].decode("utf-8").split()
                for id in byte_messages_ids:
                    mail = self.fetch_mail(id)
                    mails.append(mail) if mail is not None else None
                    if move_to_mailbox:
                        try:
                            copy_dat = self.copy_move_and_delete(id, move_to_mailbox)
                            print(copy_dat)
                        except Exception as e:
                            print(f"email {id} cannot be moved to {move_to_mailbox}", e)

        return mails

    def copy_move_and_delete(self, mail_byte_id: str, move_to_mailbox: str):
        # FIXME write a test
        data = dict()
        copy = self.socket.copy(mail_byte_id, move_to_mailbox)
        data["copy"] = copy
        if copy[0] == 'OK':
            mov, del_data = self.socket.store(mail_byte_id, '+FLAGS', '\\Deleted')
            self.socket.expunge()
            data["mov"] = mov
            data["del_data"] = del_data
        elif copy[0] == "NO":
            self.create_new_mailbox(move_to_mailbox)
            return self.copy_move_and_delete(mail_byte_id, move_to_mailbox)
        return data

    def create_new_mailbox(self, mailbox_name: str):
        # FIXME write a test
        data = self.socket.create(mailbox_name)
        return data
