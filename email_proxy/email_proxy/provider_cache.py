from typing import List

from pydantic import BaseModel

from email_proxy.provider import Provider


class AlreadyKnownError(BaseException):
    pass


class ProviderWrongData(BaseException):
    pass


class ProviderCache(BaseModel):
    providers: dict = dict()

    def get_all_providers(self) -> dict:
        return self.providers

    def get_provider(self, provider_id: str) -> Provider:
        return self.providers.get(provider_id, None)

    def has_provider(self, provider_id: str) -> bool:
        return True if self.get_provider(provider_id) is not None else False

    def add_provider(self, provider: Provider):
        if self.has_provider(provider.id):
            raise AlreadyKnownError
        valid = provider.check_provider()
        if not valid[0]:
            raise ProviderWrongData(valid)
        self.providers[provider.id] = provider

    def clear(self):
        self.providers = dict()