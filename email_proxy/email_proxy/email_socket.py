from abc import ABC, abstractmethod


class EmailSocket(ABC):
    @abstractmethod
    def open(self):
        pass

    @abstractmethod
    def close(self):
        pass

    @abstractmethod
    def is_valid(self) -> (bool, str):
        return False, ""

    @abstractmethod
    def login_user(self, user):
        pass

    @abstractmethod
    def logout_user(self):
        pass
