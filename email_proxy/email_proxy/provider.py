import json

from pydantic import BaseModel

from email_proxy.imap_socket import ImapSocket
from email_proxy.json_rebuildable import JsonRebuildable
from email_proxy.smtp_socket import SmtpSocket
from email_proxy.user import User
from email_proxy.utils import is_missing, MissingValues


class Provider(BaseModel, JsonRebuildable):
    id: str
    name: str
    base_url: str
    imap_url: str
    imap_port: int
    imap_encryption: str
    smtp_url: str
    smtp_port: int
    smtp_encryption: str

    def get_new_smtp_socket(self) -> SmtpSocket:
        return SmtpSocket(
            url=self.smtp_url, port=self.smtp_port, encryption=self.smtp_encryption
        )

    def get_new_imap_socket(self) -> ImapSocket:
        return ImapSocket(
            url=self.imap_url, port=self.imap_port, encryption=self.imap_encryption
        )

    def check_provider(self) -> (bool, dict):
        imap = self.get_new_imap_socket().is_valid()
        smtp = self.get_new_smtp_socket().is_valid()
        return imap[0] and smtp[0], dict(imap=imap, smtp=smtp)

    def check_user(self, user: User):
        smtp = self.get_new_smtp_socket()
        smtp.open()
        smtp_status, smtp_ret = smtp.login_user(user)
        smtp.close()
        imap = self.get_new_imap_socket()
        imap.open()
        imap_status, imap_ret = imap.login_user(user)
        imap.logout_user()

        return (
            imap_status and smtp_status,
            dict(imap=(imap_status, imap_ret), smtp=(smtp_status, smtp_ret),),
        )

    def __eq__(self, other):
        return (
            self.id == other.id and
            self.name == other.name and
            self.base_url == other.base_url and
            self.imap_url == other.imap_url and
            self.imap_port == other.imap_port and
            self.imap_encryption == other.imap_encryption and
            self.smtp_url == other.smtp_url and
            self.smtp_port == other.smtp_port and
            self.smtp_encryption == other.smtp_encryption
        )

    @staticmethod
    def from_json(json_obj: dict):
        if type(json_obj) == str:
            json_obj = json.loads(json_obj)
        missing = []

        is_missing("id", json_obj, missing)
        is_missing("name", json_obj, missing)
        is_missing("base_url", json_obj, missing)
        is_missing("imap_url", json_obj, missing)
        is_missing("imap_port", json_obj, missing)
        is_missing("imap_encryption", json_obj, missing)
        is_missing("smtp_url", json_obj, missing)
        is_missing("smtp_port", json_obj, missing)
        is_missing("smtp_encryption", json_obj, missing)

        if len(missing) > 0:
            raise MissingValues(f"The JSON miss some fields!", missing)

        return Provider(
            id=json_obj.get("id"),
            name=json_obj.get("name"),
            base_url=json_obj.get("base_url"),
            imap_url=json_obj.get("imap_url"),
            imap_port=json_obj.get("imap_port"),
            imap_encryption=json_obj.get("imap_encryption"),
            smtp_url=json_obj.get("smtp_url"),
            smtp_port=json_obj.get("smtp_port"),
            smtp_encryption=json_obj.get("smtp_encryption"),
        )

    def to_json(self):
        return dict(
            id=self.id,
            name=self.name,
            base_url=self.base_url,
            imap_url=self.imap_url,
            imap_port=self.imap_port,
            imap_encryption=self.imap_encryption,
            smtp_url=self.smtp_url,
            smtp_port=self.smtp_port,
            smtp_encryption=self.smtp_encryption,
        )
