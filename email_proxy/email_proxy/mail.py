from datetime import datetime
from typing import Any

from pydantic import BaseModel
from abc import ABC, abstractmethod
from email.message import EmailMessage

from email_proxy.json_rebuildable import JsonRebuildable
from email_proxy.utils import add_header_field_to_email_message, get_time_string_format


class WrongDateFormat(Exception):
    pass


class MailRebuildable(ABC):
    @staticmethod
    @abstractmethod
    def from_raw_mail(raw_mail: EmailMessage):
        pass

    @abstractmethod
    def to_raw_mail(self) -> EmailMessage:
        pass


class Mail(MailRebuildable, BaseModel, JsonRebuildable):
    header: dict
    body: str

    @staticmethod
    def from_raw_mail(raw_mail: EmailMessage):
        if raw_mail.is_multipart():
            return

        header = dict()
        for key in raw_mail.keys():
            value: str = raw_mail[key]
            key = key.replace("-", "_").lower()
            if key == "to":  # to must be parsed into an array
                arr = []
                for val in value.split(","):
                    arr.append(val.replace(" ", "").strip())
                header[key] = arr
            else:
                header[key] = str(value).strip()

        return Mail(header=header, body=raw_mail.get_content())

    @staticmethod
    def add_key_value_recursively(val: dict, email_message: EmailMessage):
        for key in val:
            old_key = key
            key = key.replace("_", "-")
            value = val[old_key]
            if type(value) == dict:
                add_header_field_to_email_message(value)
            elif type(value) == list:
                new_value = ""
                for new_val in value:
                    new_value += f"{new_val}, "
                new_value = new_value[:-2]
                add_header_field_to_email_message(
                    key,
                    new_value,
                    email_message
                )
            else:
                add_header_field_to_email_message(
                    key,
                    value,
                    email_message
                )

    def to_raw_mail(self) -> EmailMessage:
        email_message: EmailMessage = EmailMessage()
        email_message.set_content(self.body)
        for k in self.header.keys():
            value = self.header.get(k)
            key = k
            key = key.replace("_", "-")

            if k == "date":
                try:
                    if type(value) == int or type(value) == float:
                        tmp = datetime.fromtimestamp(value / 1000)
                        new = tmp.strftime("%a, %d %b %Y %H:%M:%S %z")
                        value = new
                    datetime.strptime(value, "%a, %d %b %Y %H:%M:%S %z")
                except Exception as e:
                    raise WrongDateFormat(
                        f"The date has the wrong format. The right format is like: Fri, 06 Nov 2020 13:02:20 +0100, npt ${value}."
                    )

            if key == 'additional-header-fields':
                Mail.add_key_value_recursively(value, email_message)
            else:
                add_header_field_to_email_message(key, value, email_message)

        return email_message

    @staticmethod
    def from_json(json: str):
        pass

    def to_json(self):
        return dict(
            header=self.header,
            body=self.body
        )