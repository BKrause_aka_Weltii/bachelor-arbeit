export interface IFullContact {
	position?: string | null;
	name: string;
	phoneNumber?: string;
	email?: string | null;
	office?: string | null;
	open?: {
		asString?: string | null;
		from?: string | null;
		to?: string | null;
	} | null;
}

export const developer: IFullContact = {
	position: 'Entwickler der App',
	name: 'Benjamin Krause',
	email: 'benjamin.krause@stud.th-luebeck.de',
};

export const wohnheim: IFullContact[] = [
	{
		name: 'Hausmeister Studentendorf Lübeck',
		phoneNumber: '0451 40032958',
		office: 'Studentendorf Lübeck | Anschützstraße 11 | Rückseite von Haus 11',
		open: {
			asString:
				'Montag, Dienstag und Donnerstag von 07:30 bis 09:00 Uhr, oder nach Absprache.',
		},
	},
];
export const thluebeck: IFullContact[] = [
	{
		position: 'Hausmeister',
		name: 'Stefan David',
		phoneNumber: '+49 151 52814387',
		email: 'stefan.david@th-luebeck.de',
		office: '2-K.03',
		open: {
			asString:
				'Montag bis Donnerstag: 8:00 - 8:30 Uhr und 13:30 - 14:00 Uhr | Freitag: 8:00 - 8:30 Uhr',
		},
	},
	{
		position: 'E&I Sekretariat',
		name: 'Christiane Schmidt',
		phoneNumber: '+49 451 300 5023',
		email: 'christiane.schmidt@th-luebeck.de',
		office: '2-0.06',
	},
	{
		position: 'E&I Sekretariat',
		name: 'Ines von Hütschler',
		phoneNumber: '+49 451 300 5250',
		email: 'ines.von.huetschler@th-luebeck.de',
		office: '2-0.06',
	},
	{
		position: 'AN Sekretariat',
		name: 'Tanja Daaud',
		phoneNumber: '+49 451 300 5254',
		email: 'tanja.daaud@th-luebeck.de',
		office: '13-0.21',
	},
	{
		position: 'AN Sekretariat',
		name: 'Solveig Dorsch',
		phoneNumber: '+49 451 300 5017',
		email: 'solveig.dorsch@th-luebeck.de',
		office: '13-0.21',
	},
	{
		position: 'Bauwesen Sekretariat',
		name: 'Heike Elß',
		phoneNumber: '+49 451 300 5159',
		email: 'heike.elss@th-luebeck.de',
		office: '14-0.16',
	},
	{
		position: 'MW Sekretariat',
		name: 'Kirsten Aurin',
		phoneNumber: '+49 451 300 5233',
		email: 'kirsten.aurin@th-luebeck.de',
		office: '2-0.03',
	},
];
export const uniluebeck: IFullContact[] = [
	{
		position: 'Geschäftsstelle der MINT-Sektionen',
		name: 'Susanne Markmann',
		phoneNumber: '+49 451 3101-3002',
		email: 'susanne.markmann(at)uni-luebeck(dot)de',
		office: 'Geb. 64, Zimmer 101',
		open: {
			asString: 'Dienstags 11 bis 14 Uhr oder nach Vereinbarung',
		},
	},
	{
		position: 'Studiengangskoordination Medizin',
		name: 'Dr. Karen Sievers',
		phoneNumber: '+49 451 3101 7005 ',
		email: 'karen.sievers@uni-luebeck.de',
	},
	{
		position: 'IT/Kurseinteilung Medizin',
		name: 'Dr. Gabriele Katalinic',
		phoneNumber: '+49 451 3101 7006 ',
		email: 'gabriele.katalinic@uni-luebeck.de',
	},
];
export const musikluebeck: IFullContact[] = [
	{
		position: 'Musikhochschule Studiensekretariat',
		name: '',
		phoneNumber: '+49 451 1505 210',
		email: 'studiensekretariat@mh-luebeck.de',
	},
];
export const other: IFullContact[] = [developer];
