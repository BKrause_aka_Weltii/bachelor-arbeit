import { State } from '.';
import { Provider } from '../../email/Provider';
import { IUser } from '../../email/dexie_interfaces/IUser';
import { IProvider } from '../../email/dexie_interfaces/IProvider';
import { AChat } from '../../email/AChat';
import { ChatType } from '../../email/dexie_interfaces/IChat';
import { AdChat } from '../../email/AdChat';

export const getters = {
	getChat: (state: State) => (chatId: string) => {
		return state.chats.find((chat: any) => chat.id === chatId);
	},
	getCurrentProviderBaseUrl: (state: State) => () => {
		const provider = state.providers.find(
			(provider: Provider) => provider.id == state.loginUser.chosenProviderId,
		);
		return provider.smtpUrl;
	},
	getUser: (state: State) => (email: string) => {
		return state.users.find((user) => user.email === email);
	},
	getChatWithExactUserMatch: (state: State) => (users: IUser[]) => {
		const emails = users.map((user) => user.email);
		return state.chats.filter((chat) => {
			if (emails.length != chat.participants.length) {
				return false;
			}
			const chatMails = chat.participants.map(
				(participant) => participant.email,
			);
			let hasEmail = true;
			chatMails.forEach((email) => {
				if (!emails.includes(email)) {
					hasEmail = false;
				}
			});
			return hasEmail;
		});
	},
	getProvider: (state: State) => (providerId: string) => {
		return state.providers.find(
			(provider: IProvider) => provider.id === providerId,
		);
	},
	getAd: (state: State) => (adId: string) => {
		return (
			state.chats
				.map((chat: AChat) => {
					if (chat.type == ChatType.AD) {
						return (chat as AdChat).ads.find((ad) => ad.id === adId) || null;
					}
					return null;
				})
				.filter((ad) => !!ad)[0] || null
		);
	},
};
