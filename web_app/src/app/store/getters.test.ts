import { emptyState } from '../../Config';
import { TestUtils } from '../../email/tests/TestUtils';
import { getters } from './getters';
import { IUser } from '../../email/dexie_interfaces/IUser';
import { Chat } from '../../email/Chat';
import { User } from '../../email/User';

function generateChat(users: User[]) {
	const randomNumber = TestUtils.generateNumber();
	return new Chat(
		Chat.genId(),
		`random-chat-id${randomNumber}`,
		`random-chat-name${randomNumber}`,
		[],
		users,
		[users[0]],
	);
}

function generateChats() {
	const users = [
		TestUtils.generateRandomUser(),
		TestUtils.generateRandomUser(),
		TestUtils.generateRandomUser(),
		TestUtils.generateRandomUser(),
		TestUtils.generateRandomUser(),
	];
	return [
		generateChat([users[0], users[1], users[2]]),
		generateChat([users[0], users[2], users[3]]),
		generateChat([users[0], users[2], users[1], users[4]]),
	];
}

describe('Test getters - getChatWithExeactUserMatch', () => {
	it('find exact match', () => {
		const state = emptyState;
		state.chats = generateChats();
		const search: IUser[] = state.chats[1].participants;
		const foundChat: Chat[] = getters.getChatWithExactUserMatch(state)(search);
		expect(foundChat.length).toBe(1);
		expect(foundChat[0].equals(state.chats[1])).toBeTrue();
	});

	it('find two matches', () => {
		const state = emptyState;
		const chats = generateChats();
		chats.push(generateChat(chats[2].participants));
		state.chats = chats;
		const search: IUser[] = state.chats[2].participants;
		const foundChat: Chat[] = getters.getChatWithExactUserMatch(state)(search);
		expect(foundChat.length).toBe(2);
	});

	it('find no match', () => {
		const state = emptyState;
		const chats = generateChats();
		chats.push(generateChat(chats[2].participants));
		state.chats = chats;
		const search: IUser[] = [
			chats[0].participants[0],
			TestUtils.generateRandomUser(),
		];
		const foundChat: Chat[] = getters.getChatWithExactUserMatch(state)(search);
		expect(foundChat.length).toBe(0);
	});

	it('find no match', () => {
		const state = emptyState;
		const chats = generateChats();
		chats.push(generateChat(chats[2].participants));
		state.chats = chats;
		const search: IUser[] = [
			chats[0].participants[0],
			TestUtils.generateRandomUser(),
		];
		const foundChat: Chat[] = getters.getChatWithExactUserMatch(state)(search);
		expect(foundChat.length).toBe(0);
	});

	it('find no match with a user from all chats', () => {
		const state = emptyState;
		const chats = generateChats();
		chats.push(generateChat(chats[2].participants));
		state.chats = chats;
		const search: IUser[] = [chats[0].participants[0]];
		const foundChat: Chat[] = getters.getChatWithExactUserMatch(state)(search);
		expect(foundChat.length).toBe(0);
	});
});
