import Vue from 'vue';
import VueX from 'vuex';
import { WebWorkerResponseListener } from '../../WebWorkerEvents/WebWorkerResponseListener';
import { ActionWebWorkerEvent } from '../../WebWorkerEvents/ActionWebWorkerEvent';
import { MutationTypes } from './MutationTypes';
import { ActionTypes, WebWorkerActionTypes } from './ActionTypes';
import { EMail } from '../../email';
import { Chat } from '../../email/Chat';
import { AMessage } from '../../email/dexie_interfaces/AMessage';
import { emptyState } from '../../Config';
import { IUseMailbox } from '../../email/dexie_interfaces/IUseMailbox';
import { UseMailbox } from '../../email/UseMailbox';
import { IProvider } from '../../email/dexie_interfaces/IProvider';
import { Utils } from '../../email/utils';
import { IUser } from '../../email/dexie_interfaces/IUser';
import { User } from '../../email/User';
import { getters } from './getters';
import { CustomPromise } from '../routes/CustomPromise';
import { ChatType } from '../../email/dexie_interfaces/IChat';
import { ChatLoader } from '../../email/ChatLoader';
import { AChat } from '../../email/AChat';
import { ChatContentTypes } from '../../email/ChatContentTypes';
import { Notification } from '../Notification';

Vue.use(VueX);

export interface State {
	proxyStatus: {
		available: boolean;
		statusMessage: string;
	};
	loginUser: {
		email: string;
		password: string;
		loginName: string;
		savePassword: boolean;
		chosenProviderId: string;
	};
	loginFailed: {
		reason: string;
	};
	providers: any;
	chats: AChat[];
	filter: string;
	mailboxes: IUseMailbox[];
	providerEdit: {
		provider: IProvider | null;
		isValid: boolean;
		reason: string;
	};
	users: IUser[];
	currentHomeTab?: string;
	notifications: Notification[];
}

const responseListener = new WebWorkerResponseListener();

const store = new VueX.Store({
	state: emptyState,
	mutations: {
		[MutationTypes.LOGIN_USER](state, payload) {
			state.loginUser = {
				email: payload.email,
				password: payload.password,
				loginName: payload.loginName,
				savePassword: payload.savePassword || false,
				chosenProviderId: payload.chosenProviderId,
			};
		},
		[MutationTypes.LOGIN_FAILED](state, payload) {
			state.loginFailed = {
				reason: payload,
			};
		},
		[MutationTypes.CHANGE_PROXY_STATUS](state, payload) {
			state.proxyStatus.available = payload.available;
			state.proxyStatus.statusMessage = payload.statusMessage;
		},
		[MutationTypes.INIT_STORE](state, payload) {
			this.replaceState(Object.assign(state, payload));
		},
		[MutationTypes.UPDATE_CHATS](state, payload: AChat[]) {
			const notInPayload: AChat[] = state.chats.filter((chat: AChat) => {
				return payload.filter((c: AChat) => chat.id === c.id).length === 0;
			});
			state.chats = notInPayload.concat(payload) as any;
		},
		[MutationTypes.CHANGE_FILTER](state, payload) {
			state.filter = payload;
		},
		[MutationTypes.CHANGE_PROVIDER_EDIT_FIELD_VALUE](
			state,
			payload: { field: string; value: any },
		) {
			//@ts-ignore
			state.providerEdit.provider[payload.field] = payload.value;
		},
		[MutationTypes.CLEAR_PROVIDER_EDIT](state) {
			//@ts-ignore
			state.providerEdit = Utils.emptyState;
		},
		[MutationTypes.ERROR_PROVIDER_EDIT](state, payload: any) {
			//@ts-ignore
			state.providerEdit.isValid = false;
			state.providerEdit.reason = payload;
		},
		[MutationTypes.CHANGE_USER_NICKNAME](
			state,
			payload: {
				email: string;
				nickname: string;
			},
		) {
			state.users = state.users.map((user) => {
				if (user.email === payload.email) {
					user.nickname = payload.nickname;
				}
				return user;
			});
		},
		[MutationTypes.CHANGE_CURRENT_HOME_TAB](state, tab: string) {
			state.currentHomeTab = tab;
		},
		[MutationTypes.ADD_NOTIFICATION](state: State, notification: Notification) {
			state.notifications.push(notification);
		},
		[MutationTypes.DELETE_NOTIFICATION](
			state: State,
			notification: Notification | number,
		) {
			const id =
				typeof notification == 'number' ? notification : notification.id;
			console.log(notification, typeof notification);
			if (!id) {
				console.error('Cannot delete a undefined notification!');
			} else {
				state.notifications = state.notifications?.filter((notific) => {
					return notific.id != id;
				});
			}
		},
	},
	actions: {
		[ActionTypes.LOGIN_USER](context, payload) {
			responseListener
				.sendEvent(
					new ActionWebWorkerEvent(
						ActionTypes.LOGIN_USER,
						payload,
						responseListener,
					),
				)
				.then((event) => {
					if (event.data.status == '200') {
						context.commit(MutationTypes.LOGIN_FAILED, null);
						context.commit(MutationTypes.LOGIN_USER, payload.user);
					} else {
						context.commit(MutationTypes.LOGIN_FAILED, event.data.data);
					}
				})
				.catch((event) => {
					const msg = 'Nobody responeded to the event! 😭';
					console.error(msg, event);
					context.commit(MutationTypes.LOGIN_FAILED, msg);
				});
		},
		[ActionTypes.LOGOUT_USER](context, clearDB) {
			responseListener
				.sendEvent(
					new ActionWebWorkerEvent(
						ActionTypes.LOGOUT_USER,
						{
							clearDB: clearDB || false,
						},
						responseListener,
					),
				)
				.then((success) => {
					if (success) {
						console.log('User successfully logged out');
						context.commit(MutationTypes.LOGIN_USER, {
							email: '',
							password: '',
							loginName: '',
							savePassword: false,
							chosenProviderId: '',
						});
					} else {
						throw Error('You are maybe not logged out correctly!');
					}
				})
				.catch((err) => {
					console.log('User logout failed: ', err);
					// FIXME Add something to visualize, that the user is not logged out!
					context.commit(MutationTypes.LOGIN_USER, {
						email: 'Something goes wrong.',
						password: 'Please reload the page!',
						loginName: '',
						savePassword: false,
						chosenProviderId: '',
					});
				});
		},
		[ActionTypes.CHECK_PROXY_STATUS](context) {
			responseListener
				.sendEvent(
					new ActionWebWorkerEvent(
						ActionTypes.CHECK_PROXY_STATUS,
						{},
						responseListener,
					),
				)
				.then((event) => {
					context.commit(MutationTypes.CHANGE_PROXY_STATUS, {
						available: event.data.status == 200,
						statusMessage: event.data.statusText,
					});
				})
				.catch((event) => {
					console.log('receive error: ', event);
					context.commit(MutationTypes.CHANGE_PROXY_STATUS, {
						available: false,
						statusMessage: event.data,
					});
				});
		},
		[ActionTypes.INIT_STORE_FROM_WEB_WORKER](context, payload) {
			payload.chats = payload.chats.map((chat: any) =>
				ChatLoader.fromJson(chat),
			);
			payload.mailboxes = payload.mailboxes.map((mailbox: any) =>
				UseMailbox.fromJson(mailbox),
			);
			this.commit(MutationTypes.INIT_STORE, payload);
		},
		[ActionTypes.SEND_EMAIL](
			context,
			payload: {
				email: EMail;
				callbackPromise?: CustomPromise;
			},
		) {
			responseListener
				.sendEvent(
					new ActionWebWorkerEvent(
						ActionTypes.SEND_EMAIL,
						payload.email.toJson(),
						responseListener,
						false,
					),
				)
				.then(() => {
					payload.callbackPromise?.thenCallback();
				})
				.catch(() => {
					payload.callbackPromise?.catchCallBack();
				});
		},
		[ActionTypes.CREATE_CHAT](
			context,
			payload: {
				name: string;
				inviteMembers: string[];
				isPublic: boolean;
				onlyAdminsCanSend: boolean;
				type: ChatType;
				callbackPromise?: CustomPromise;
			},
		) {
			responseListener
				.sendEvent(
					new ActionWebWorkerEvent(
						ActionTypes.CREATE_CHAT,
						{
							name: payload.name,
							inviteMembers: payload.inviteMembers,
							isPublic: payload.isPublic,
							onlyAdminsCanSend: payload.onlyAdminsCanSend,
							type: payload.type,
						},
						responseListener,
						false,
					),
				)
				.then((result) => {
					if (result.data.status == 200) {
						const chat = ChatLoader.fromJson(result.data.data.newChat) as AChat;
						console.log('Chat was created successfully!', chat);
						context.commit(MutationTypes.UPDATE_CHATS, [chat]);
						payload.callbackPromise?.thenCallback(chat.id);
					} else {
						throw result.data.data;
					}
				})
				.catch((err) => {
					console.error(err);
					payload.callbackPromise?.catchCallBack(err);
				});
		},
		[ActionTypes.SEND_MESSAGE](
			context,
			payload: {
				message: AMessage;
				callbackPromise?: CustomPromise;
			},
		) {
			console.log('Action send message', payload.message);
			responseListener
				.sendEvent(
					new ActionWebWorkerEvent(
						ActionTypes.SEND_MESSAGE,
						payload.message,
						responseListener,
						false,
					),
				)
				.then((result: any) => {
					console.log('Send_Message result', result);
					payload.callbackPromise?.thenCallback(result);
				})
				.catch((err: any) => {
					payload.callbackPromise?.catchCallBack(err);
				});
		},
		[ActionTypes.CHANGE_USE_MAILBOX](context, payload: IUseMailbox) {
			responseListener
				.sendEvent(
					new ActionWebWorkerEvent(
						ActionTypes.CHANGE_USE_MAILBOX,
						payload,
						responseListener,
						false,
					),
				)
				.then((result) => {
					console.log('something comes back :)', result);
				})
				.catch((err) => {
					console.error(err);
				});
		},
		[ActionTypes.CREATE_PROVIDER](
			context,
			payload: {
				provider: IProvider;
				share: boolean;
				callbackPromise?: CustomPromise;
			},
		) {
			responseListener
				.sendEvent(
					new ActionWebWorkerEvent(
						ActionTypes.CREATE_PROVIDER,
						{
							provider: payload.provider,
							share: payload.share,
						},
						responseListener,
						false,
					),
				)
				.then((result) => {
					console.log('something comes back :)', result);
					if (result.data.status == 200) {
						payload.callbackPromise?.thenCallback();
					} else {
						payload.callbackPromise?.catchCallBack(result.data);
					}
				})
				.catch((err) => {
					console.error(err);
					payload.callbackPromise?.catchCallBack();
				});
		},
		[ActionTypes.CHANGE_USER_NICKNAME](
			context,
			payload: {
				email: string;
				nickname: string;
			},
		) {
			let cachedUser: User;
			User.load(payload.email)
				.then((user: User) => {
					cachedUser = user;
					user.nickname = payload.nickname;
					return user.save();
				})
				.then(() => {
					context.commit(MutationTypes.CHANGE_USER_NICKNAME, {
						email: cachedUser?.email,
						nickname: cachedUser?.nickname,
					});
				});
		},
		[ActionTypes.CREATE_NOTIFICATION](context, notification: Notification) {
			context.commit(MutationTypes.ADD_NOTIFICATION, notification);
			if (notification.showTime > 0) {
				setTimeout(() => {
					context.dispatch(ActionTypes.REMOVE_NOTIFICATION, notification);
				}, notification.showTime * 1000);
			}
		},
		[ActionTypes.REMOVE_NOTIFICATION](
			context,
			notification: Notification | number,
		) {
			context.commit(MutationTypes.DELETE_NOTIFICATION, notification);
		},
		[WebWorkerActionTypes.UPDATE_CHATS](context, payload) {
			payload = payload.map((chat: any) => ChatLoader.fromJson(chat));
			context.commit(MutationTypes.UPDATE_CHATS, payload);
			context.dispatch(
				ActionTypes.CREATE_NOTIFICATION,
				new Notification(
					'Chat Update',
					`Deine Chats haben sich geupdated`,
					1,
					'info',
				),
			);
		},
	},
	getters: getters,
});

export default store;
