# Actions

Two action types are possible:

1. ActionTypes will be send from the frontend (main thread)
2. WebWorkerActionTypes will be send only from a web or service worker.
