import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import ChatView from '../views/Chat.vue';
import NewChat from '../views/NewChat.vue';
import JoinChat from '../views/JoinChat.vue';
import ChatDetals from '../views/ChatDetails.vue';
import Mailboxes from '../views/Mailboxes.vue';
import CustomProvider from '../views/CustomProvider.vue';
import SendNewsMessage from '../views/SendNewsMessage.vue';
import Contacts from '../views/Contacts.vue';
import CaretakerRequest from '../views/CaretakerRequest.vue';
import NewAd from '../views/NewAd.vue';
import About from '../views/About.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
	{
		path: '/',
		name: 'Home',
		component: Home,
	},
	{
		path: '/login',
		name: 'Login',
		component: Login,
	},
	{
		path: '/new-chat',
		name: 'NewChat',
		component: NewChat,
	},
	{
		path: '/join_chat',
		name: 'JoinChat',
		component: JoinChat,
	},
	{
		path: '/chat/:chatId',
		component: ChatView,
		props: true,
	},
	{
		path: '/chat-details/:chatId',
		component: ChatDetals,
		props: true,
	},
	{
		path: '/mailboxes',
		component: Mailboxes,
	},
	{
		path: '/custom-provider',
		component: CustomProvider,
		name: 'CustomProvider',
	},
	{
		path: '/chat/:chatId/send-news-message/',
		component: SendNewsMessage,
		props: true,
	},
	{
		path: '/contacts',
		component: Contacts,
		name: 'Contacts',
	},
	{
		path: '/caretaker-request',
		component: CaretakerRequest,
	},
	{
		path: '/chat/:chatId/new-ad/',
		component: NewAd,
		props: true,
	},
	{
		path: '/chat/:chatId/new-ad/:adId',
		component: NewAd,
		props: true,
	},
	{
		path: '/about',
		component: About,
		name: 'About',
	},
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

export default router;
