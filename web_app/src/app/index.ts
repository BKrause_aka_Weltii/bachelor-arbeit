import './style.scss';
import Vue from 'vue';
import App from './App.vue';
import router from './routes';
import store from './store';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
	faCheck,
	faUser,
	faEnvelope,
	faKey,
	faSave,
	faTimes,
	faPaperPlane,
	faArrowLeft,
	faPlus,
	faAngleDown,
	faGlobe,
	faHouseUser,
	faHome,
	faBoxes,
	faInfo,
	faCoins,
	faExchangeAlt,
	faSearch,
	faGift,
	faRecycle,
	faExclamationTriangle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
	faCheck,
	faUser,
	faEnvelope,
	faKey,
	faSave,
	faTimes,
	faPaperPlane,
	faArrowLeft,
	faPlus,
	faAngleDown,
	faGlobe,
	faHouseUser,
	faHome,
	faBoxes,
	faInfo,
	faCoins,
	faExchangeAlt,
	faSearch,
	faGift,
	faRecycle,
	faExclamationTriangle,
); // the icons you can use in the font-awesome-icon component
Vue.component('font-awesome-icon', FontAwesomeIcon); // the component to use the icons

Vue.config.productionTip = false;

new Vue({
	router,
	store: store,
	render: (h) => h(App),
}).$mount('#app');

// store.dispatch(ActionTypes.CHECK_PROXY_STATUS);
