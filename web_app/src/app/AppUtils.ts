export namespace AppUtils {
	export function toColumns(items: any[], itemsPerRow: number = 2) {
		let columns: any[][] = [];
		let column: any[] = [];
		items.forEach((item) => {
			column.push(item);
			if (column.length == itemsPerRow) {
				columns.push(column);
				column = [];
			}
		});
		columns.push(column);
		return columns;
	}
}
