export interface ClickContentObject {
	onCount: number;
	message: string;
}

export const pinnedChatClickTriage: ClickContentObject[] = [
	{
		onCount: 0,
		message: 'Eine Anfrage zum beitreten wurde an den Administrator versendet',
	},
	{
		onCount: 1,
		message: 'Du kannst es wirklich nicht abwarten, oder?',
	},
	{
		onCount: 3,
		message: 'Durch häufiges drücken wird es auch nicht besser!',
	},
	{
		onCount: 5,
		message: '...',
	},
	{
		onCount: 8,
		message: 'Du bist wirklich hartnäckig.',
	},
	{
		onCount: 10,
		message: 'Es bringt dir aber nicht viel.',
	},
	{
		onCount: 20,
		message: 'Könntest du jetzt bitte damit aufhören! 😤',
	},
	{
		onCount: 30,
		message: '😠',
	},
	{
		onCount: 40,
		message: '😡',
	},
	{
		onCount: 50,
		message:
			'Okay, hier mein Deal. Du klickst noch 10-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 51,
		message:
			'Okay, hier mein Deal. Du klickst noch 9-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 52,
		message:
			'Okay, hier mein Deal. Du klickst noch 8-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 53,
		message:
			'Okay, hier mein Deal. Du klickst noch 7-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 54,
		message:
			'Okay, hier mein Deal. Du klickst noch 6-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 55,
		message:
			'Okay, hier mein Deal. Du klickst noch 5-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 56,
		message:
			'Okay, hier mein Deal. Du klickst noch 4-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 57,
		message:
			'Okay, hier mein Deal. Du klickst noch 3-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 58,
		message:
			'Okay, hier mein Deal. Du klickst noch 2-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 59,
		message:
			'Okay, hier mein Deal. Du klickst noch 1-mal und ich schicke noch mal eine Nachricht ab.',
	},
	{
		onCount: 60,
		message: 'Hast du das wirklich geglaubt? 😂',
	},
	{
		onCount: 70,
		message: 'So jetzt ist aber mal schluss!',
	},
	{
		onCount: 100,
		message: 'Du hast echt zu viel Zeit...',
	},
	{
		onCount: 300,
		message: 'Du hast jetzt über 300-mal auf diesen Knopf gedrückt.',
	},
	{
		onCount: 1001,
		message:
			'Tausendundein Dalmatiner... Halt Stop ich glaube ich habe mich verzählt.',
	},
];
