export interface PinnedChat {
	name: string;
	description: string;
	chatId: string;
	adminEmail: string;
	disabled?: boolean;
}

export const pinnedChats: PinnedChat[] = [
	{
		name: 'Neuigkeiten rund um das Wohnheim',
		description:
			'Der News-Feed rund ums Wohnheim. Verpasse keine Informationen mehr!',
		chatId: 'chat--Cwe66LV0f=K=7ejt-1613933608593',
		adminEmail: 'benjamin.krause@stud.th-luebeck.de',
		disabled: false,
	},
	{
		name: 'Kleinanzeigen',
		description: 'Kleinanzeigen, zum verkaufen und tauschen von Dingen',
		chatId: 'chat--vmjtDm1iyqaGe5Y4-1613933491635',
		adminEmail: 'benjamin.krause@stud.th-luebeck.de',
		disabled: false,
	},
	{
		name: 'Foodsharing',
		description:
			'Du hast Lebensmittel die du nicht mehr brauchst? Dann tausch oder verschenk sie einfach.',
		chatId: 'chat--RELpCPgw6=Pi8kKD-1613933523415',
		adminEmail: 'benjamin.krause@stud.th-luebeck.de',
		disabled: false,
	},
	{
		name: 'Spiele-Verleih',
		description:
			'Du hast Spiele und möchtest diese mit anderen Teilen, dann bist du hier genau richtig!',
		chatId: 'chat--A7aaJMbve1X0lqYo-1613933551494',
		adminEmail: 'benjamin.krause@stud.th-luebeck.de',
		disabled: false,
	},
	{
		name: 'Paketsuchzentrale',
		description:
			'Du hast Lebensmittel die du nicht mehr brauchst? Dann tausch oder verschenk sie einfach.',
		chatId: 'chat--Me3,qQAmyp5o+LGW-1613933572469',
		adminEmail: 'benjamin.krause@stud.th-luebeck.de',
		disabled: false,
	},
];
