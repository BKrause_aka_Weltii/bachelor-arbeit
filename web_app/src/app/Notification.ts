export class Notification {
	private static lastId = 1;
	readonly id: number = Notification.lastId++;
	constructor(
		readonly title: string,
		readonly message: string,
		readonly showTime: number,
		readonly color: string = 'success',
	) {}
}
