export enum ExtraFieldTypes {
	TEXT = 'TEXT',
	//TEXT_AREA = "TEXT_AREA",
	NUMBER = 'NUMBER',
	SELECT = 'SELECT',
}

export interface ExtraField {
	type: ExtraFieldTypes; // text, number or select
	label: string;
	options?: ICategorie[]; // will be used with select
}

export interface ExtraFieldResult {
	extraField: ExtraField;
	result: [] | string | number | ICategorieResult;
}

export interface ICategorie {
	name: string;
	enum: string;
	subCategories?: ICategorie[];
	extraContent?: ExtraField[];
}

export interface ICategorieResult {
	iCategorie: ICategorie;
	subCategorieResult?: ICategorieResult;
	extraContentResults?: ExtraFieldResult[];
}

const somethingDifferentCategorie: ICategorie = {
	enum: 'SOMETHING_DIFFERENT',
	name: 'Etwas anderes',
	extraContent: [
		{
			label: 'Etwas anderes',
			type: ExtraFieldTypes.TEXT,
		},
	],
};

const somethingDifferent: ExtraField = {
	label: 'Etwas anderes',
	type: ExtraFieldTypes.TEXT,
};

const extra: ExtraField = {
	label: '',
	type: ExtraFieldTypes.TEXT,
};

const notes: ExtraField = {
	label: 'Anmerkungen?',
	type: ExtraFieldTypes.TEXT,
};

export const caretakerRequest: ICategorie[] = [
	{
		name: 'Licht im / am Haus',
		enum: 'LIGHT',
		extraContent: [
			{
				type: ExtraFieldTypes.NUMBER,
				label: 'Gebäude',
			} as ExtraField,
		],
		subCategories: [
			{
				name: 'Treppenhaus',
				enum: 'STAIR_WELL',
				extraContent: [
					{
						type: ExtraFieldTypes.NUMBER,
						label: 'Stockwerk',
					} as ExtraField,
				],
			} as ICategorie,
			{
				name: 'Eingangshalle',
				enum: 'ENTRANCE_HALL',
			},
			{
				name: 'Draußen',
				enum: 'OUTSIDE',
			},
		],
	},
	{
		name: 'Wohnungs Gemeinschaftsraum',
		enum: 'FLAT_COMMON_ROOM',
		extraContent: [
			{
				label: 'Wohnungsnummer',
				type: ExtraFieldTypes.NUMBER,
			},
		],
		subCategories: [
			{
				enum: 'WATER_TAP',
				name: 'Wasserhahn tropft',
				extraContent: [notes],
			},
			{
				name: 'Herdplatte defekt',
				enum: '',
				extraContent: [
					{
						label: 'Platte',
						type: ExtraFieldTypes.SELECT,
						options: [
							{
								enum: 'FR',
								name: 'Vorne Rechts',
							},
							{
								enum: 'FL',
								name: 'Vorne Links',
							},
							{
								enum: 'BR',
								name: 'Hinten Rechts',
							},
							{
								enum: 'FL',
								name: 'Hinten Links',
							},
							{
								enum: 'ALL',
								name: 'Alle Platten',
							},
						],
					},
					notes,
				],
			},
			{
				name: 'Ofen defekt',
				enum: 'OVEN',
				extraContent: [notes],
			},
			{
				name: 'Kühlschrank defekt',
				enum: 'FRIDGE',
				extraContent: [
					{
						label: '',
						type: ExtraFieldTypes.TEXT,
					},
				],
			},
			{
				name: 'Etwas anders',
				enum: 'OTHER',
				extraContent: [somethingDifferent],
			},
		],
	},
	{
		name: 'Meinem Zimmer',
		enum: 'PRIVATE_ROOM',
		extraContent: [
			{
				label: 'Zimmernummer',
				type: ExtraFieldTypes.NUMBER,
			},
		],
		subCategories: [
			{
				name: 'Badezimmer',
				enum: 'BATHROOM',
				subCategories: [
					{
						enum: 'TOILET',
						name: 'Toilette',
						subCategories: [
							{
								name: 'Verstopft',
								enum: 'BLOCKED',
							},
							{
								name: 'Spühlung',
								enum: 'FLUSH',
							},
							somethingDifferentCategorie,
						],
					},
					{
						name: 'Dusche',
						enum: 'SHOWER',
						subCategories: [
							{
								name: 'Duschvorhang',
								enum: 'SHOWER_CURTAION',
								extraContent: [
									{
										label: '',
										type: ExtraFieldTypes.TEXT,
									},
								],
							},
							{
								name: 'Abfluss verstopft',
								enum: 'DRAIN',
							},
						],
					},
					{
						enum: 'MOLD',
						name: 'Schimmel',
						extraContent: [
							{
								label: '',
								type: ExtraFieldTypes.TEXT,
							},
						],
					},
				],
			},
		],
	},
];

export function categorieResultToString(
	categorie: ICategorieResult | null,
): string {
	let ret = '';
	ret += `${categorie?.iCategorie.name}, `;

	categorie?.extraContentResults?.forEach((res) => {
		ret += `${res.extraField.label} ${res.result}, `;
	});
	// ret = ret.substring(0, ret.length - 2)
	if (categorie && categorie.subCategorieResult) {
		ret += `${categorieResultToString(categorie.subCategorieResult)} `;
	}
	return ret;
}
