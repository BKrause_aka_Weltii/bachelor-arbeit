export enum WebWorkerEventType {
	DING = 'DING', // PING
	DING_DONG_DONGELONG = 'DING_DONG_DONGELONG', // PONG
	ACTION = 'ACTION', // used to transport actions
	RESPONSE_EVENT = 'RESPONSE_EVENT',
	READY_TO_WORK = 'READY_TO_WORK', // event that will fired, when every thing is started and ready to work
	PULL = 'PULL',
	START_PULL = 'START_PULL',
	STOP_PULL = 'STOP_PULL',
	CHANGE_PULL_INTERVAL = 'CHANGE_PULL_INTERVAL',
}
