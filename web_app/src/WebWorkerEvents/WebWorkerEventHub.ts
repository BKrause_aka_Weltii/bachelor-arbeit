import { WebWorkerEventListener } from './WebWorkerEventListener';
import { WebWorkerEvent } from './WebWorkerEvent';

export class WebWorkerEventHub {
	protected static listeners: Array<WebWorkerEventListener> = [];

	static subscribe(listener: WebWorkerEventListener) {
		if (WebWorkerEventHub.listeners.indexOf(listener) !== -1) return;
		WebWorkerEventHub.listeners.push(listener);
	}

	static unsubscribe(listener: WebWorkerEventListener) {
		WebWorkerEventHub.listeners.splice(
			WebWorkerEventHub.listeners.indexOf(listener),
			1,
		);
	}

	static sendEvent(event: WebWorkerEvent) {
		WebWorkerEventHub.listeners.forEach((listener: WebWorkerEventListener) => {
			if (event.senderId != listener.id) {
				listener.consumeEvent(event);
			}
		});
	}
}
