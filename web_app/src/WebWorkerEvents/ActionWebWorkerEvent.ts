import { WebWorkerEventType } from './WebWorkerEventType';
import { WebWorkerEventListener } from './WebWorkerEventListener';
import { WebWorkerEvent } from './WebWorkerEvent';

export class ActionWebWorkerEvent extends WebWorkerEvent {
	constructor(
		public readonly actionType: string,
		data: any,
		sender: WebWorkerEventListener | null,
		dontSendToWebWorker: boolean = false,
	) {
		super(WebWorkerEventType.ACTION, data, sender, dontSendToWebWorker);
	}
}
