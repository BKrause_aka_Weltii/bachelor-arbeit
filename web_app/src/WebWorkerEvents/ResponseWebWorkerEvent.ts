import { WebWorkerEventType } from './WebWorkerEventType';
import { WebWorkerEventListener } from './WebWorkerEventListener';
import { WebWorkerEvent } from './WebWorkerEvent';

export type ResponseData = {
	status: string;
	data: any;
};

export class ResponseWebWorkerEvent extends WebWorkerEvent {
	constructor(
		public readonly responseOnEvent: WebWorkerEvent,
		data: ResponseData,
		sender: WebWorkerEventListener | null,
		dontSendToWebWorker: boolean = false,
	) {
		super(WebWorkerEventType.RESPONSE_EVENT, data, sender, dontSendToWebWorker);
	}
}
