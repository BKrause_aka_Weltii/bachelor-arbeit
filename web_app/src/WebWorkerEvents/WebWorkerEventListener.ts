import { WebWorkerEvent } from './WebWorkerEvent';

export abstract class WebWorkerEventListener {
	static lastId: number = 0;
	protected _id = `web_worker_${WebWorkerEventListener.lastId++}`;

	abstract consumeEvent(event: WebWorkerEvent): void;

	get id() {
		return this._id;
	}
}
