import { WebWorkerEventListener } from './WebWorkerEventListener';
import { WebWorkerEvent } from './WebWorkerEvent';
import { WebWorkerEventType } from './WebWorkerEventType';
import { WebWorkerEventHub } from './WebWorkerEventHub';
import { ResponseWebWorkerEvent } from './ResponseWebWorkerEvent';
import { Config } from '../Config';

export class WebWorkerResponseListener extends WebWorkerEventListener {
	private activeEvents: Map<
		string,
		{ resolve: any; reject: any; timeout: any }
	> = new Map();

	constructor() {
		super();
		WebWorkerEventHub.subscribe(this);
	}

	sendEvent(event: WebWorkerEvent): Promise<WebWorkerEvent> {
		return new Promise<WebWorkerEvent>((resolve, reject) => {
			const timeout = setTimeout(() => {
				this.rejectEvent(event);
			}, Config.defaultProxyTimeout);
			this.activeEvents.set(event.id, {
				resolve,
				reject,
				timeout,
			});
			WebWorkerEventHub.sendEvent(event);
		});
	}

	consumeEvent(event: WebWorkerEvent): void {
		if (event.type === WebWorkerEventType.RESPONSE_EVENT) {
			this.resolveEvent(event as ResponseWebWorkerEvent);
		}
	}

	private resolveEvent(event: ResponseWebWorkerEvent) {
		// @ts-ignore the _id obj is there, but normally protected
		const resolveAndReject = this.activeEvents.get(event.responseOnEvent._id);
		if (resolveAndReject) {
			clearTimeout(resolveAndReject.timeout);
			resolveAndReject.resolve(event);
		}
	}

	private rejectEvent(event: WebWorkerEvent) {
		const resolveAndReject = this.activeEvents.get(event.id);
		if (resolveAndReject) {
			resolveAndReject.reject(event);
		}
	}
}
