import { WebWorkerEventListener } from './WebWorkerEventListener';
import { WebWorkerEventType } from './WebWorkerEventType';

export class WebWorkerEvent {
	private static lastId: number = 0;

	protected _id: string = `${WebWorkerEvent.lastId++}`;
	public readonly senderId: string;
	public readonly type: WebWorkerEventType;
	public readonly data: any;
	public readonly dontSendToWebWorker: boolean;

	constructor(
		type: WebWorkerEventType,
		data: any,
		sender: WebWorkerEventListener | null,
		dontSendToWebWorker: boolean = false,
	) {
		this.type = type;
		this.senderId = sender?.id || `no_sender_${new Date().getTime()}`;
		this.data = data;
		this.dontSendToWebWorker = dontSendToWebWorker;
	}

	get id() {
		return this._id;
	}
}
