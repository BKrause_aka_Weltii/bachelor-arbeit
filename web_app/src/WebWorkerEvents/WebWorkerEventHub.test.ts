import { WebWorkerEventListener } from './WebWorkerEventListener';
import { WebWorkerEvent } from './WebWorkerEvent';
import { WebWorkerEventHub } from './WebWorkerEventHub';
import { WebWorkerEventType } from './WebWorkerEventType';

class TestListener extends WebWorkerEventListener {
	public receiveEvents: boolean = false;

	consumeEvent(event: WebWorkerEvent): void {
		this.receiveEvents = true;
	}
}

class TestWebWorkerEventHub extends WebWorkerEventHub {
	static clearListener() {
		WebWorkerEventHub.listeners = [];
	}

	static getListenerCount() {
		return WebWorkerEventHub.listeners.length;
	}
}

describe('Test WebWorkerEventHub', () => {
	afterEach(() => {
		TestWebWorkerEventHub.clearListener();
	});

	it('Subscribe - Adds listener successfully', () => {
		expect(TestWebWorkerEventHub.getListenerCount()).toBe(0);
		TestWebWorkerEventHub.subscribe(new TestListener());
		expect(TestWebWorkerEventHub.getListenerCount()).toBe(1);
		TestWebWorkerEventHub.subscribe(new TestListener());
		expect(TestWebWorkerEventHub.getListenerCount()).toBe(2);
	});

	it('Subscribe - Add multiple time the same', () => {
		const testListener = new TestListener();
		expect(TestWebWorkerEventHub.getListenerCount()).toBe(0);
		TestWebWorkerEventHub.subscribe(testListener);
		expect(TestWebWorkerEventHub.getListenerCount()).toBe(1);
		TestWebWorkerEventHub.subscribe(testListener);
		expect(TestWebWorkerEventHub.getListenerCount()).toBe(1);
	});

	it('Unsubscribe - work expected', () => {
		const testListener = new TestListener();
		expect(TestWebWorkerEventHub.getListenerCount()).toBe(0);
		TestWebWorkerEventHub.subscribe(testListener);
		expect(TestWebWorkerEventHub.getListenerCount()).toBe(1);
		TestWebWorkerEventHub.unsubscribe(testListener);
		expect(TestWebWorkerEventHub.getListenerCount()).toBe(0);
	});

	it('SendEvent - send events to every listener', () => {
		const testListener1 = new TestListener();
		const testListener2 = new TestListener();
		TestWebWorkerEventHub.subscribe(testListener1);
		TestWebWorkerEventHub.subscribe(testListener2);
		TestWebWorkerEventHub.sendEvent(
			new WebWorkerEvent(WebWorkerEventType.DING, '', null, true),
		);

		expect(testListener1.receiveEvents).toBeTrue();
		expect(testListener2.receiveEvents).toBeTrue();
	});
});
