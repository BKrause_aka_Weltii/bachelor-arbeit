import { WebWorkerEventListener } from '../WebWorkerEvents/WebWorkerEventListener';
import { WebWorkerEvent } from '../WebWorkerEvents/WebWorkerEvent';
import { WebWorkerEventHub } from '../WebWorkerEvents/WebWorkerEventHub';
import { WebWorkerEventType } from '../WebWorkerEvents/WebWorkerEventType';
import { ActionWebWorkerEvent } from '../WebWorkerEvents/ActionWebWorkerEvent';
import { ResponseWebWorkerEvent } from '../WebWorkerEvents/ResponseWebWorkerEvent';
import { ActionTypes, WebWorkerActionTypes } from '../app/store/ActionTypes';
import { JsonProvider, Encryption } from '../email/interfaces/provider';
import { LoginUser } from '../email/LoginUser';
import { Config } from '../Config';
import { DB } from '../email/db';
import { ILoginUser } from '../email/dexie_interfaces/ILoginUser';
import { EMail } from '../email';
import { Chat } from '../email/Chat';
import { ChatContentTypes } from '../email/ChatContentTypes';
import { InviteMessage } from '../email/content_messages/InviteMessage';
import { User } from '../email/User';
import { JsonContext } from '../email/interfaces/json-rebuildable';
import { MessageRebuilder } from '../email/content_messages/MessageRebuilder';
import { AMessage } from '../email/dexie_interfaces/AMessage';
import { Utils } from './../email/utils';
import { DeleteMessage } from '../email/content_messages/DeleteMessage';
import { State } from '../app/store';
import { IUseMailbox } from '../email/dexie_interfaces/IUseMailbox';
import { UseMailbox } from '../email/UseMailbox';
import { EMailProxy } from '../email/EMailProxy';
import { Provider } from '../email/Provider';
import { ChatType } from '../email/dexie_interfaces/IChat';
import { AdChat } from '../email/AdChat';
import { AChat } from '../email/AChat';
import { ChatLoader } from '../email/ChatLoader';
import { NewAdMessage } from '../email/content_messages/NewAdMessage';
import { IEMail } from '../email/dexie_interfaces/IEMail';

type PromiseHelper = {
	success: boolean;
	data: any;
};

export class WebWorkerLogic extends WebWorkerEventListener {
	private syncloop: any = null;
	private pullInterval = Config.defaultEmailPullInterval;
	private proxy = EMailProxy.getInstance();

	/**
	 * This class is only for web-worker or service-worker.
	 * If you try to use it on the main thread it will run into errors!
	 */
	constructor() {
		super();
		WebWorkerEventHub.subscribe(this);
		this.startLoop(this.pullInterval);
		self.addEventListener(
			'message',
			(event: MessageEvent) => {
				console.log('Receive from mt', event);
				this.consumeEvent(event.data);
			},
			false,
		);
	}

	private startLoop(seconds: number) {
		console.log(`start pull loop with: ${seconds} seconds`);
		this.stopLoop();
		this.loop();
		this.syncloop = setInterval(this.loop.bind(this), seconds * 1000);
	}

	private stopLoop() {
		if (this.syncloop) {
			console.log('Stop pull loop');
			clearInterval(this.syncloop);
			this.syncloop = null;
		}
	}

	private loop() {
		this.pullMessages();
	}

	consumeEvent(event: WebWorkerEvent): void {
		switch (event.type) {
			case WebWorkerEventType.DING:
				console.log('Receive ding event.');
				break;
			case WebWorkerEventType.ACTION:
				this.handleActionEvents(event as ActionWebWorkerEvent);
				break;
			case WebWorkerEventType.READY_TO_WORK:
				console.log('create inital state');
				this.createInitialStateAction();
				break;
			case WebWorkerEventType.PULL:
				this.pullMessages();
				break;
			case WebWorkerEventType.START_PULL:
				this.startLoop(this.pullInterval);
				this.sendToParent(
					new ResponseWebWorkerEvent(
						event,
						{
							status: '200',
							data: {},
						},
						this,
						true,
					),
				);
				break;
			case WebWorkerEventType.STOP_PULL:
				this.stopLoop();
				this.sendToParent(
					new ResponseWebWorkerEvent(
						event,
						{
							status: '200',
							data: {},
						},
						this,
						true,
					),
				);
				break;
			case WebWorkerEventType.CHANGE_PULL_INTERVAL:
				this.pullInterval = event.data.interval;
				this.startLoop(this.pullInterval);
				this.sendToParent(
					new ResponseWebWorkerEvent(
						event,
						{
							status: '200',
							data: {},
						},
						this,
						true,
					),
				);
				break;
			default:
				console.error(`The WebWorkerLogic won't handle ${event.type} events`);
				break;
		}
	}

	private pullMessages() {
		//console.info('Pull messages');
		const loadedChats: any = {};
		let messages: AMessage[] = [];
		// @ts-ignore
		this.proxy
			?.pullMessages()
			.then((emails: EMail[]) => {
				return Promise.all(
					emails
						.map((email) => {
							return MessageRebuilder.messageFromEmail(email)
								.then((message: AMessage | null) => {
									if (message) {
										return message;
									}
									return null;
								})
								.catch((err) => {
									console.error(
										`transform email ${email.messageId} to message failed and will be ignored!`,
										email,
										err,
									);
									return null;
								});
						})
						.filter((email) => !!email),
				);
			})
			.then((resultMessages: (AMessage | null)[]) => {
				messages = resultMessages.filter(
					(message: AMessage | null) => !!message,
				) as AMessage[];
				const newChats: InviteMessage[] = messages.filter(
					(message: AMessage) =>
						message.chatContent === ChatContentTypes.INVITE.toString(),
				) as InviteMessage[];
				console.log('invites');
				return Promise.all(
					newChats.map((message: InviteMessage) => {
						return ChatLoader.load(message.chatId).catch((err) => {
							console.log('Chat not found create new one from Invite');
							const chat = ChatLoader.fromInviteMessage(message);
							console.log(chat);
							return chat?.save();
						});
					}),
				);
			})
			.then((saveNewChatResult: any) => {
				// Only chats who are affected will be loaded and updated.
				const loadChatIds = [
					...new Set(messages.map((message: AMessage) => message.chatId)),
				];
				return Promise.all(
					loadChatIds.map((chatId: string) => {
						return ChatLoader.load(chatId).catch((err) => {
							console.error(`Cannot load chat ${chatId}`);
							return null;
						});
					}),
				);
			})
			.then((chats: (Chat | null)[]) => {
				(chats.filter((chat: Chat | null) => !!chat) as Chat[]).forEach(
					(chat: Chat) => {
						loadedChats[chat.id] = chat;
					},
				);
				return Promise.all(
					messages
						.map((message) => {
							let chat: AChat = loadedChats[message.chatId];
							if (chat) {
								return this.processMessages(message, chat)
									.catch((err) => {
										console.error('WebWorkerLogic.processmessages', err);
										return Promise.resolve(null);
									})
									.then(() => {
										if (
											chat.type == ChatType.AD &&
											message.chatContent == ChatContentTypes.INVITE
										) {
											this.handleAdChat(
												chat as AdChat,
												message as InviteMessage,
											);
										}
									});
							} else {
								console.error(
									`No chat found, message ${message.messageId} will be ignored!`,
								);
							}
						})
						.filter((message) => !!message),
				);
			})
			.then(() => {
				return Promise.all(
					Object.keys(loadedChats).map((key) => {
						return (loadedChats[key] as Chat).save();
					}),
				);
			})
			.then((saveChatResult: any) => {
				if (Object.keys(loadedChats).length > 0) {
					this.sendToParent(
						new ActionWebWorkerEvent(
							WebWorkerActionTypes.UPDATE_CHATS,
							Object.keys(loadedChats).map((key) => {
								return (loadedChats[key] as Chat).toJson();
							}),
							this,
							true,
						),
					);
				}
			})
			.catch((err: any) => {
				console.error(err);
			});
	}

	private processMessages(message: AMessage, chat: AChat): Promise<any> {
		return chat.addMessage(message).then(() => {
			console.log('process message', message);
			switch (message.chatContent) {
				case ChatContentTypes.DELETE_USER_FROM_CHAT:
					this.getLoggedInUser().then((user: User) => {
						// if the loggedInUser is the user to be deleted.
						if (user.email === (message as DeleteMessage).deleteUserEmail) {
							chat.delete().catch((err: any) => {
								console.error(
									'WebWorkerLogic.pullMessages - failed to delete chat after DeleteMessage',
									err,
								);
							});
						}
					});
					break;
				case ChatContentTypes.JOIN_REQUEST:
					console.log('Join Request', message);
					if (
						this.proxy.loggedInUser &&
						(chat.isPublic || chat.isAdmin(this.proxy.loggedInUser))
					) {
						const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
						const inviteMessage: InviteMessage = new InviteMessage(
							this.proxy.loggedInUser,
							EMail.genId(this.proxy.provider?.smtpUrl),
							chat.id,
							chat.name,
							date,
							[message.sender as User],
							chat.participants,
							chat.admins,
							date,
							chat.isPublic,
							chat.onlyAdminsCanSend,
							chat.type,
						);
						this.handleActionEvents(
							new ActionWebWorkerEvent(
								ActionTypes.SEND_MESSAGE,
								inviteMessage,
								this,
								false,
							),
						);
						console.log('handle chat type', chat.type);
						if (chat.type == ChatType.AD) {
							this.handleAdChat(chat as AdChat, inviteMessage);
						}
					} else {
						// FIXME maybe send a message back, with the information to create a new request.
						// or send the message to a admin.
						console.error(
							'JOIN_REQUEST, you are no admin, send a false back with the hint to an admin',
						);
					}
					break;
			}
			return Promise.resolve();
		});
	}

	private async createInitialStateAction() {
		const proxyData = await this.getProxyStatus();
		const user = await this.getLoggedInUser().catch((err) => {
			console.error('creatInitalStateAction - getLoggedInUser', err);
			return null;
		});
		const chats: Chat[] =
			user?.chats.map((chat) => {
				return chat.toJson(false, JsonContext.INTERNAL) as Chat;
			}) || [];
		if (chats.length == 0)
			console.error(
				'creatInitalStateAction - getLoggedInUser',
				'chats are empty!',
			);
		const mailboxes: IUseMailbox[] = await this.getInitialMailboxes()
			.then((result) => result)
			.catch((err) => {
				console.error('creatInitalStateAction - await mailboxes', err);
				return [];
			});
		const initialData: State = {
			proxyStatus: {
				available: proxyData.data.status == 200 || false,
				statusMessage: proxyData.data.statusText || 'Unexpected Error!',
			},
			loginUser: {
				email: user?.email || '',
				password: user?.password || '',
				loginName: user?.loginName || '',
				savePassword: false,
				chosenProviderId: user?.chosenProviderId || '',
			},
			loginFailed: {
				reason: '',
			},
			providers: (await this.getProviders()).map((provider) => {
				return provider.toJson();
			}),
			chats: chats || [],
			filter: '',
			mailboxes: mailboxes || [],
			providerEdit: {
				provider: {
					id: '',
					name: '',
					baseUrl: '',
					smtpUrl: '',
					smtpPort: 587,
					smtpEncryption: Encryption.SSL_TLS,
					imapUrl: '',
					imapPort: 993,
					imapEncryption: Encryption.STARTTLS,
				},
				isValid: false,
				reason: '',
			},
			users: await User.getAllUsers(),
			notifications: [],
		};
		this.sendToParent(
			new ActionWebWorkerEvent(
				ActionTypes.INIT_STORE_FROM_WEB_WORKER,
				initialData,
				this,
				true,
			),
		);
	}

	async handleActionEvents(event: ActionWebWorkerEvent) {
		switch (event.actionType) {
			case ActionTypes.CHECK_PROXY_STATUS:
				this.getProxyStatus()
					.then((data: PromiseHelper) => {
						this.sendToParent(
							new ResponseWebWorkerEvent(event, data.data, this, true),
						);
					})
					.catch((err: PromiseHelper) => {
						console.error('Proxy pinging returns with an error!', err.data);
						this.sendToParent(
							new ResponseWebWorkerEvent(
								event,
								err.data.toString(),
								this,
								true,
							),
						);
					});
				break;
			case ActionTypes.LOGIN_USER:
				const loginUser: LoginUser = LoginUser.fromJson(event.data);
				const providerId = event.data.chosenProviderId;
				console.log('Login user');
				this.proxy.cachedProviderId = providerId;
				this.proxy
					.getProvider(providerId)
					.then((provider) => {
						this.proxy.provider = provider;
						return this.proxy?.loginUser(loginUser);
					})
					.then(() => {
						return loginUser.save();
					})
					.then((saveResult) => {
						console.log(loginUser);
						this.sendToParent(
							new ResponseWebWorkerEvent(
								event,
								{
									status: '200',
									data: {
										user: loginUser.toJson(),
										provider: this.proxy.provider,
									},
								},
								this,
								true,
							),
						);
						this.createInitialStateAction();
					})
					.catch((err) => {
						if (err.status) {
							this.sendToParent(
								new ResponseWebWorkerEvent(
									event,
									{
										status: err.status,
										data: err,
									},
									this,
									true,
								),
							);
						} else {
							this.sendToParent(
								new ResponseWebWorkerEvent(
									event,
									{
										status: '500',
										data: err.toString(),
									},
									this,
									true,
								),
							);
						}
					});
				break;
			case ActionTypes.LOGOUT_USER:
				this.proxy?.logoutUser();
				const deleteFromDB = await DB.getInstance().clearAll();
				console.log('Delete User from db: ', deleteFromDB);
				this.sendToParent(
					new ResponseWebWorkerEvent(
						event,
						{
							status: '200',
							data: 'Database is cleared!q',
						},
						this,
						true,
					),
				);
				break;
			case ActionTypes.CREATE_CHAT:
				const emails: string[] = [
					...new Set(
						event.data.inviteMembers.filter((email: string) => !!email),
					),
				] as string[];
				if (event.data.name.length > 0 && emails.length > 0) {
					let chat: AChat;
					let inviteMessage: InviteMessage;
					Promise.resolve().then(() => {
						// Get users for chat!
						return Promise.all(
							emails.map((email: string) => {
								return User.loadOrCreateNew(email);
							}),
						)
							.then((allUsers: User[]) => {
								console.log(allUsers.concat([]));
								const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
								// create chat and save
								const creator = (this.proxy
									?.loggedInUser as LoginUser).toUser();
								allUsers.push(creator);
								switch (event.data.type) {
									case ChatType.NORMAL:
										chat = new Chat(
											Chat.genId(),
											event.data.name,
											date,
											[],
											allUsers,
											[creator],
											event.data.isPublic,
											event.data.onlyAdminsCanSend,
										);
										break;
									case ChatType.AD:
										chat = new AdChat(
											Chat.genId(),
											event.data.name,
											date,
											[],
											allUsers,
											[creator],
											[],
											event.data.isPublic,
											event.data.onlyAdminsCanSend,
										);
										break;
								}
								inviteMessage = new InviteMessage(
									(this.proxy?.loggedInUser as LoginUser).toUser(),
									EMail.genId(this.proxy.provider?.smtpUrl),
									chat.id,
									chat.name,
									date,
									allUsers,
									[],
									chat.admins,
									date,
									chat.isPublic,
									chat.onlyAdminsCanSend,
									chat.type,
								);
								return chat.addMessage(inviteMessage);
							})
							.then(() => {
								const email = inviteMessage.toEmail(chat);
								return this.proxy?.sendMail(email);
							})
							.then((chatSaveResult: any) => {
								return chat.save();
							})
							.then((sendResult) => {
								console.log('Send chat', chat);
								this.sendToParent(
									new ResponseWebWorkerEvent(
										event,
										{
											status: '200',
											data: {
												message: 'Email was send successfully!',
												newChat: chat.toJson(false, JsonContext.INTERNAL),
											},
										},
										this,
										true,
									),
								);
							})
							.catch((err) => {
								this.sendToParent(
									new ResponseWebWorkerEvent(
										event,
										{
											status: '400',
											data: {
												message: 'Create new chat run into an error!',
												error: err.toString(),
											},
										},
										this,
										true,
									),
								);
							});
					});
					// send invite message
				} else {
					this.sendToParent(
						new ResponseWebWorkerEvent(
							event,
							{
								status: '400',
								data: {
									message: 'One or multiple emails are wrong!',
									emails: emails,
								},
							},
							this,
							true,
						),
					);
				}
				break;
			case ActionTypes.SEND_MESSAGE:
				const message = MessageRebuilder.messageRebuilder(
					event.data,
				) as AMessage;
				let chat: AChat;
				ChatLoader.load(message.chatId)
					.then((resultChat: AChat) => {
						chat = resultChat;
						return this.processMessages(message, chat);
					})
					.then(() => {
						return chat.save();
					})
					.then(() => {
						const email: EMail = message.toEmail(chat) as EMail;
						if (email.to.length > 0) {
							return this.proxy?.sendMail(email);
						} else {
							return Promise.resolve(true);
						}
					})
					.then((result) => {
						this.sendToParent(
							new ResponseWebWorkerEvent(
								event,
								{
									status: '200',
									data: result,
								},
								this,
								true,
							),
						);
						console.log('Debug chat', chat);
						this.sendToParent(
							new ActionWebWorkerEvent(
								WebWorkerActionTypes.UPDATE_CHATS,
								[chat.toJson()],
								this,
								true,
							),
						);
						console.log('is ad chat', chat.type == ChatType.AD);
						if (chat.type == ChatType.AD) {
							this.handleAdChat(chat as AdChat, message as InviteMessage);
						}
					})
					.catch((err) => {
						this.sendToParent(
							new ResponseWebWorkerEvent(
								event,
								{
									status: '400',
									data: err,
								},
								this,
								true,
							),
						);
					});
				break;
			case ActionTypes.SEND_EMAIL:
				this.proxy
					?.sendMail(EMail.fromJson(event.data))
					.then((result) => {
						this.sendToParent(
							new ResponseWebWorkerEvent(
								event,
								{
									status: '200',
									data: result,
								},
								this,
								true,
							),
						);
					})
					.catch((err) => {
						this.sendToParent(
							new ResponseWebWorkerEvent(
								event,
								{
									status: '400',
									data: err,
								},
								this,
								true,
							),
						);
					});
				break;
			case ActionTypes.CHANGE_USE_MAILBOX:
				const useMailbox = UseMailbox.fromJson(event.data);
				console.log(useMailbox);
				useMailbox.save().then(() => {
					this.sendToParent(
						new ResponseWebWorkerEvent(
							event,
							{
								status: '200',
								data: useMailbox,
							},
							this,
							true,
						),
					);
				});
				break;
			case ActionTypes.CREATE_PROVIDER:
				const payload = event.data;
				let savedResponse: any;
				fetch(`${Config.proxyUrl}providers`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(
						Utils.replaceCamelCaseToSnakeCaseRecursively(payload.provider),
					),
				})
					.then((response) => {
						savedResponse = response;
						return response.json();
					})
					.then((json) => {
						this.sendToParent(
							new ResponseWebWorkerEvent(
								event,
								{
									status: savedResponse.status,
									data: json.message || 'Fatal Error',
								},
								this,
								true,
							),
						);
					})
					.catch((err) => {
						this.sendToParent(
							new ResponseWebWorkerEvent(
								event,
								{
									status: '400',
									data: err,
								},
								this,
								true,
							),
						);
					});
				break;
		}
	}

	private async getProxyStatus() {
		console.log('get proxy status', Config.proxyUrl);
		return new Promise<PromiseHelper>((resolve, reject) => {
			fetch(`${Config.proxyUrl}ding`, {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
				},
			})
				.then((response) => {
					console.log('proxy pings');
					const data = {
						status: response.status,
						message: response.statusText,
					};
					resolve({
						success: true,
						data: data,
					});
				})
				.catch((err) => {
					console.log('proxy no anwser');
					resolve({
						success: false,
						data: err,
					});
				});
		});
	}

	private getInitialMailboxes(): Promise<IUseMailbox[]> {
		let transformedMailboxes: UseMailbox[];
		return this.proxy
			.getMailboxes()
			.then((mailboxes) => {
				transformedMailboxes = mailboxes.map((mailbox: any) => {
					return new UseMailbox(mailbox, false);
				});
				return UseMailbox.loadAll();
			})
			.then((resultDatabaseUseMailboxes) => {
				const database = resultDatabaseUseMailboxes.map(
					(mailbox) => mailbox.mailbox,
				);
				const newMailboxes = transformedMailboxes.filter(
					(mailbox) => !database.includes(mailbox.mailbox),
				);
				return UseMailbox.bulkSave(newMailboxes);
			})
			.then(() => {
				return UseMailbox.loadAll();
			})
			.catch((err) => {
				console.error(
					'Cannot load mailboxes from proxy! Mailboxes from the database will served only.',
					err,
				);
				return UseMailbox.loadAll();
			});
	}

	private async getLoggedInUser() {
		let user: LoginUser;
		if (this.proxy?.loggedInUser) {
			return this.proxy.loggedInUser;
		} else {
			return DB.getInstance()
				.lastUser.toArray()
				.then((data: ILoginUser[]) => {
					const user = data[0] || null;
					console.log('getLoggedInUser user', user);
					return LoginUser.load(user?.email || '');
				})
				.then(async (loginUser: LoginUser) => {
					console.log('getLoggedInUser loginUser', loginUser);
					user = loginUser;
					return this.proxy
						.getProvider(user.chosenProviderId)
						.then((provider) => {
							return this.proxy.cacheProvider(provider);
						})
						.then(() => Promise.resolve(true))
						.catch(() => Promise.resolve(false));
				})
				.then((isProxyAvailable: boolean) => {
					if (!isProxyAvailable) {
						throw 'Proxy is not available';
					}
					return this.proxy?.loginUser(user);
				})
				.then(() => {
					console.log('user is logged in', user, this.proxy.loggedInUser);
					return user;
				})
				.catch((err: any) => {
					if (user) {
						user
							.delete()
							.then((result) => {
								console.log('Old user is deleted', result);
							})
							.catch((err) => {
								console.error(err);
							});
					}
					console.error('Last user can not be found, or is invalid.', err);
					return new LoginUser('', '', [], '', '', '');
				});
		}
	}

	private handleAdChat(chat: AdChat, message: InviteMessage) {
		console.log('Handle Ad Chat');
		if (
			chat.type == ChatType.AD &&
			message.chatContent == ChatContentTypes.INVITE
		) {
			const ads = (chat as AdChat).ads.filter(
				(ad) => ad.creator.email === this.proxy.loggedInUser?.email,
			);
			console.log('This are my ads', ads);
			return Promise.all(
				ads.map((ad) => {
					const jsonEmail = new NewAdMessage(
						ad.creator,
						EMail.genId(this.proxy.provider?.smtpUrl),
						chat.id,
						Utils.dateToTimeString(Utils.getCurrentDateTime()),
						ad.id,
						ad.title,
						ad.description,
						ad.priceType,
						ad.priceAmount,
					)
						.toEmail(chat)
						.toJson() as IEMail;
					jsonEmail.to = (message as InviteMessage).newMembers.map(
						(member) => member.email,
					);
					(message as InviteMessage).newMembers.map((member) => {
						return member.email;
					});
					this.handleActionEvents(
						new ActionWebWorkerEvent(
							ActionTypes.SEND_EMAIL,
							EMail.fromJson(jsonEmail),
							this,
							true,
						),
					);
				}),
			);
		}
	}

	private async getProviders(): Promise<Array<Provider>> {
		return this.proxy.fetchProviders().catch((err) => {
			console.error('creatInitalStateAction - await mailboxes', err);
		});
	}

	private async logoutUser() {
		const user = await this.getLoggedInUser();
		return DB.getInstance()
			.lastUser.where('email')
			.equals(user?.email)
			.delete();
	}

	sendToParent(event: WebWorkerEvent) {
		console.log('send event', event);
		// @ts-ignore
		self.postMessage(event);
	}
}
