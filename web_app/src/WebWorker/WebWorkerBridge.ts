import { WebWorkerEventListener } from '../WebWorkerEvents/WebWorkerEventListener';
import { WebWorkerEvent } from '../WebWorkerEvents/WebWorkerEvent';
import { register } from 'register-service-worker';
import { WebWorkerEventHub } from '../WebWorkerEvents/WebWorkerEventHub';
import { WebWorkerEventType } from '../WebWorkerEvents/WebWorkerEventType';
import { ActionWebWorkerEvent } from '../WebWorkerEvents/ActionWebWorkerEvent';
import store from '../app/store';
import { Utils } from '../email/utils';

export class WebWorkerBridge extends WebWorkerEventListener {
	private readonly worker: Worker;

	constructor() {
		super();
		// use on non localhost environemnts a service-worker to destribute data
		if (!Utils.isLocalhost()) {
			// on other environments use a service-worker!
			register('/service-worker.js', {
				registrationOptions: { scope: './' },
				ready(registration: ServiceWorkerRegistration) {
					console.log('Service worker is active.');
				},
				registered(registration: ServiceWorkerRegistration) {
					console.log('Service worker has been registered.');
				},
				cached(registration: ServiceWorkerRegistration) {
					console.log('Content has been cached for offline use.');
				},
				updatefound(registration: ServiceWorkerRegistration) {
					console.log('New content is downloading.');
				},
				updated(registration: ServiceWorkerRegistration) {
					console.log('New content is available; please refresh.');
				},
				offline() {
					console.log(
						'No internet connection found. App is running in offline mode.',
					);
				},
				error(error: Error) {
					console.error('Error during service worker registration:', error);
				},
			});
		}

		this.worker = new Worker('../../dist/local-dev-web-worker.js');
		this.registerToWebWorkerEventHub();
	}

	private registerToWebWorkerEventHub() {
		WebWorkerEventHub.subscribe(this);
		this.worker?.addEventListener('message', (event: MessageEvent) => {
			event.data.senderId = this.id;
			this.consumeEvent(event.data);
		});
	}

	consumeEvent(event: WebWorkerEvent): void {
		if (event.type === WebWorkerEventType.RESPONSE_EVENT) {
			WebWorkerEventHub.sendEvent(event);
		} else if (this.worker && !event.dontSendToWebWorker) {
			this.worker.postMessage(event);
		} else {
			switch (event.type) {
				case WebWorkerEventType.DING_DONG_DONGELONG:
					console.log(`Ding Dong Dongelong`);
					break;
				case WebWorkerEventType.ACTION:
					const actionEvent = event as ActionWebWorkerEvent;
					store.dispatch(actionEvent.actionType, actionEvent.data);
					break;
				case WebWorkerEventType.READY_TO_WORK:
					this.worker?.postMessage(event);
					break;
			}
		}
	}
}
