import { WebWorkerLogic } from './WebWorker/WebWorkerLogic';

self.addEventListener('activate', (event) => {
	new WebWorkerLogic();
});
