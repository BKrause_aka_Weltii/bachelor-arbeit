import { ISetting } from './email/dexie_interfaces/ISetting';
import { SettingKeys } from './email/UserSettings';
import { DateTime } from 'luxon';
import { State } from './app/store';
import { Encryption } from './email/interfaces/provider';

export namespace Config {
	// hard coded, because this might not work in a browser?...
	export const proxyUrl = 'http://127.0.0.1:8000/';
	export const defaultProxyTimeout = 4000; // 4 seconds
	export const appName = 'Wohmheim-App';
	export const appDatabaseName = 'wohmheim-app-db'; // be careful with changes on prod environments!
	export const defaultEmailPullInterval = 5;
	export const defaultSettings: ISetting[] = [
		{
			key: SettingKeys.LAST_PULL,
			value: DateTime.fromMillis(0).toRFC2822(),
		},
	];
	export const possibleChars =
		'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890+-=.,';
}

export namespace EMailDefaults {
	export const chatVersion = '1.0';
	export const chatContent = 'plain';
	export const mimeVersion = '1.0';
	export const contentType = 'text/plain; charset=UTF-8; format="flowed"';
	export const userAgent = 'WohnheimPWA';
}

export const emptyState: State = {
	proxyStatus: {
		available: false,
		statusMessage: 'Initial Value, please wait.',
	},
	loginUser: {
		email: '',
		password: '',
		loginName: '',
		savePassword: false,
		chosenProviderId: '',
	},
	loginFailed: {
		reason: '',
	},
	providers: {},
	chats: [],
	filter: '',
	mailboxes: [],
	providerEdit: {
		provider: {
			id: '',
			name: '',
			baseUrl: '',
			smtpUrl: '',
			smtpPort: 587,
			smtpEncryption: Encryption.STARTTLS,
			imapUrl: '',
			imapPort: 993,
			imapEncryption: Encryption.SSL_TLS,
		},
		isValid: false,
		reason: '',
	},
	users: [],
	currentHomeTab: '',
	notifications: [],
};
