import { Chat } from './Chat';
import { AMessage } from './dexie_interfaces/AMessage';
import { User } from './User';
import { ChatContentTypes } from './ChatContentTypes';
import { ChatContentIsBlacklistedError } from './exceptions/ChatContentIsBlacklistedError';

export class NewsChat extends Chat {
	private messageBlacklist = [ChatContentTypes.TEXT_MESSAGE.toString()];

	constructor(
		id: string | null,
		name: string | null,
		creationDate: string,
		messages?: AMessage[],
		participants?: User[],
		admins?: User[],
		isPublic?: boolean,
		onlyAdminsCanSend: boolean = true,
	) {
		super(
			id,
			name,
			creationDate,
			messages,
			participants,
			admins,
			isPublic,
			onlyAdminsCanSend,
		);
	}

	isAllowedToPerformeAction(message: AMessage) {
		return this.isAdmin(message.sender.email);
	}

	addMessage(message: AMessage) {
		if (this.messageBlacklist.includes(message.chatContent)) {
			return Promise.reject(new ChatContentIsBlacklistedError(message, this));
		}
		return super.addMessage(message);
	}
}
