import { DB } from './db';
import { ISetting } from './dexie_interfaces/ISetting';
import { DbClass } from './interfaces/db-class';
import { Config } from './../Config';

export enum SettingKeys {
	LAST_PULL = 'LAST_PULL',
}

export class UserSettings implements DbClass {
	private constructor(private _settings: ISetting[]) {}

	static load(): Promise<UserSettings> {
		return DB.getInstance()
			.settings.toArray()
			.then((allSettings: ISetting[]) => {
				if (allSettings.length == 0) {
					throw '';
				}
				return new UserSettings(allSettings);
			})
			.catch(() => {
				return new UserSettings(Config.defaultSettings)
					.save()
					.then(() => UserSettings.load());
			});
	}

	save() {
		return DB.getInstance().settings.bulkPut(this._settings);
	}

	delete() {
		return Promise.reject('UserSettings.delete is not implemented');
	}

	equals() {
		return false;
	}

	get settings() {
		return this._settings;
	}

	getSetting(key: SettingKeys) {
		return this.settings.filter((setting) => setting.key === key)[0];
	}

	setSetting(key: string, value: any) {
		this._settings = this._settings.map((setting) => {
			return setting.key === key
				? {
						key: key,
						value: value,
				  }
				: setting;
		});
		return this.save();
	}
}
