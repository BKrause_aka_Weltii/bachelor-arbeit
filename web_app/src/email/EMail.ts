import { JsonContext, JsonRebuildable } from './interfaces/json-rebuildable';
import { Utils } from './utils';
import { IEMail } from './dexie_interfaces/IEMail';
import { EMailDefaults } from '../Config';
import transformAllSnakeCaseToCamelCase = Utils.transformAllSnakeCaseToCamelCase;

export type JsonEMail = {
	header: {
		from: string;
		to: string[];
		subject: string;
		date: string;
		message_id: string;
		mime_version: string;
		content_type: string;
		additional_header_fields: any;
		chat_version: string;
		chat_content: string;
		chat_id: string;
	};
	body: string;
};

export class EMail implements JsonRebuildable, IEMail {
	readonly from: string;
	readonly to: Array<string>; // FIXME an email must have one to.  I never check it.
	readonly subject: string;
	readonly date: string;
	readonly messageId: string;
	readonly chatVersion: string;
	readonly mimeVersion: string;
	readonly contentType: string;
	readonly chatContent: string;
	readonly additionalHeaderFields: any;
	readonly body: string;
	readonly chatId: string;

	constructor(
		header: {
			from: string;
			to: Array<string>;
			subject: string;
			date: string;
			messageId: string;
			mimeVersion: string;
			contentType: string;
			additionalHeaderFields?: any;
			chatId: string;
			chatVersion: string;
			chatContent: string;
		},
		body: string,
	) {
		this.from = header.from;
		this.to = header.to;
		this.subject = header.subject;
		this.date = header.date;
		this.messageId = header.messageId;
		this.mimeVersion = header.mimeVersion;
		this.contentType = header.contentType;
		this.additionalHeaderFields = header.additionalHeaderFields || {};
		this.body = body;
		this.chatVersion = header.chatVersion;
		this.chatId = header.chatId;
		this.chatContent = header.chatContent;
	}

	public toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | JsonEMail | IEMail {
		let json;
		if (context == JsonContext.INTERNAL) {
			json = {
				from: this.from,
				// @ts-ignore FIXME
				to: this.to.map((user) => {
					return user;
				}),
				subject: this.subject,
				date: this.date,
				messageId: this.messageId,
				mimeVersion: this.mimeVersion,
				contentType: this.contentType,
				additionalHeaderFields: this.additionalHeaderFields,
				body: this.body,
				chatVersion: this.chatVersion,
				chatContent: this.chatContent,
				chatId: this.chatId,
			};
		} else {
			json = {
				header: {
					from: this.from,
					to: this.to.map((user) => {
						return user;
					}),
					subject: this.subject,
					date: this.date,
					message_id: this.messageId,
					mime_version: this.mimeVersion,
					content_type: this.contentType,
					additional_header_fields: Utils.replaceCamelCaseToSnakeCaseRecursively(
						this.additionalHeaderFields,
					),
					chat_version: this.chatVersion,
					chat_content: this.chatContent,
					chat_id: this.chatId,
				},
				body: this.body,
			};
		}
		if (asString) {
			return JSON.stringify(json);
		}
		return json;
	}

	public static fromJson(json: any | JsonEMail | IEMail) {
		if (typeof json === 'string') {
			json = JSON.parse(json);
		}
		json = transformAllSnakeCaseToCamelCase(json); // replace recursively all snake_case keys

		const body = Utils.getAndDel('body', json);
		const headerFields = Utils.getAndDel('header', json) || json;
		const from = Utils.getAndDel('from', headerFields);
		const to = Utils.getAndDel('to', headerFields);
		const subject = Utils.getAndDel('subject', headerFields);
		const date = Utils.getAndDel('date', headerFields);
		const messageId = Utils.getAndDel('messageId', headerFields);
		const mimeVersion =
			Utils.getAndDel('mimeVersion', headerFields) || EMailDefaults.mimeVersion;
		const contentType =
			Utils.getAndDel('contentType', headerFields) || EMailDefaults.contentType;
		const chatId = Utils.getAndDel('chatId', headerFields);
		const chatContent =
			Utils.getAndDel('chatContent', headerFields) || EMailDefaults.chatContent;
		const chatVersion =
			Utils.getAndDel('chatVersion', headerFields) || EMailDefaults.chatVersion;
		let additionalHeaderFields =
			Utils.getAndDel('additionalHeaderFields', headerFields) || {};

		// The rest will add into the additionalHeaderFields
		Object.keys(headerFields).forEach((key: string) => {
			// TODO add rules for header field filter
			// X-... fields shouldn't added to the database.
			additionalHeaderFields[key] = headerFields[key];
		});

		return new EMail(
			{
				from: from,
				to: to,
				subject: subject,
				date: date,
				messageId: messageId,
				mimeVersion: mimeVersion,
				contentType: contentType,
				additionalHeaderFields: additionalHeaderFields,
				chatId: chatId,
				chatContent: chatContent,
				chatVersion: chatVersion,
			},
			body,
		);
	}

	/*public static load(pk: string) {
		const db = DB.getInstance();
		let emailTo: Array<string>;
		let additionalHeaderFields: any = {};
		return Promise.resolve()
			.then(() => {
				return db.emailTo.where('messageId').equals(pk).toArray();
			})
			.then((result) => {
				emailTo = result.map((entity) => {
					return entity.email;
				});
				return db.additionalHeaderFields
					.where('messageId')
					.equals(pk)
					.toArray();
			})
			.then((result) => {
				result.forEach((entity) => {
					additionalHeaderFields[entity.key] = entity.value;
				});
				return db.emails.where('messageId').equals(pk).first();
			})
			.then((result) => {
				if (result) {
					return EMail.fromJson(
						Utils.mergeObjects(Utils.mergeObjects(result, { to: emailTo }), {
							additionalHeaderFields: additionalHeaderFields,
						}),
					);
				} else {
					throw new NotFoundInDatabase(`Email with messageId: ${pk}`);
				}
			});
	}*/

	public static genId(provider: string = 'example.net') {
		if (!provider) {
			throw 'A email message id needs a provider!';
		}
		const currentMillis = new Date().getTime();
		return `${EMailDefaults.userAgent.toLowerCase()}-${currentMillis}-${Utils.getRandomString(
			16,
		)}@${provider}`;
	}

	/*save(): Promise<any> {
		return new Promise<any>((resolve, reject) => {
			const stats: any = {};
			const email: IEMail = this.toJson(false, true) as IEMail;
			const emailTo = getAndDel('to', email).map((emailAddress: string) => {
				return {
					email: emailAddress,
					messageId: email.messageId,
				} as IEmailTo;
			});
			const additionalHeaderFieldsData =
				getAndDel('additionalHeaderFields', email) || {};
			const additionalHeaderFields = Object.keys(
				additionalHeaderFieldsData,
			).map((key) => {
				return {
					messageId: email.messageId,
					key: key,
					value: additionalHeaderFieldsData[key],
				} as IAdditionalHeaderFields;
			});

			return Promise.resolve()
				.then(() => {
					return DB.getInstance().emails.put(email);
				})
				.then((ret) => {
					stats['email'] = ret;
					return DB.getInstance().emailTo.bulkPut(emailTo, { allKeys: true });
				})
				.then((ret) => {
					stats['emailTo'] = ret;
					return DB.getInstance().additionalHeaderFields.bulkPut(
						additionalHeaderFields,
						{ allKeys: true },
					);
				})
				.then((ret) => {
					stats['additionalHeaderFields'] = ret;
					resolve(stats);
				})
				.catch((err) => {
					console.error('Error on saving EMail', err);
					return err;
				});
		});
	}*/

	/*delete(): Promise<any> {
		const stats: any = {};
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.emailTo.where('messageId')
					.equals(this.messageId)
					.delete();
			})
			.then((result) => {
				stats['emailTo'] = result;
				return DB.getInstance()
					.additionalHeaderFields.where('messageId')
					.equals(this.messageId)
					.delete();
			})
			.then((result) => {
				stats['additionalHeaderFields'] = result;
				return DB.getInstance()
					.emails.where('messageId')
					.equals(this.messageId)
					.delete();
			})
			.then((result) => {
				stats['email'] = result;
				return stats;
			});
	}*/

	equals(other: EMail) {
		return (
			this.from === other.from &&
			this.to === other.to &&
			this.subject === other.subject &&
			this.date === other.date &&
			this.messageId === other.messageId &&
			this.mimeVersion === other.mimeVersion &&
			this.contentType === other.contentType &&
			this.chatContent === other.chatContent &&
			this.chatVersion === other.chatVersion &&
			this.chatId === other.chatId &&
			this.to.length === other.to.length &&
			this.additionalHeaderFields.length === other.additionalHeaderFields.length
		);
	}
}
