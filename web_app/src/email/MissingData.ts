export class MissingData extends Error {
	constructor(public readonly key: string) {
		super(`${key} is missing in data set`);
	}
}
