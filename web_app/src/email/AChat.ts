import { ChatContentTypes } from './ChatContentTypes';
import { DeleteMessage } from './content_messages/DeleteMessage';
import { InviteMessage } from './content_messages/InviteMessage';
import { PromoteMessage } from './content_messages/PromoteMessage';
import { DB } from './db';
import { AMessage } from './dexie_interfaces/AMessage';
import { ChatType, IChat } from './dexie_interfaces/IChat';
import { IUser } from './dexie_interfaces/IUser';
import { UserHasNoRightsToPerformeEmailError } from './exceptions/UserHasNoRightsToPerformeEmailError';
import { WrongChatError } from './exceptions/WrongChatError';
import { DbClass } from './interfaces/db-class';
import { JsonContext, JsonRebuildable } from './interfaces/json-rebuildable';
import { User } from './User';
import { Utils } from './utils';

export abstract class AChat implements DbClass, JsonRebuildable, IChat {
	protected constructor(
		readonly id: string,
		readonly name: string,
		readonly creationDate: string,
		public messages: AMessage[],
		public participants: User[],
		public admins: User[],
		readonly isPublic: boolean,
		readonly onlyAdminsCanSend: boolean,
		readonly type: ChatType,
	) {}

	get lastMessage() {
		return Utils.sortMessages(this.messages)[this.messages.length - 1];
	}

	static fromInviteMessage(message: InviteMessage): AChat {
		console.error(new Error('You must implement AChat.fromInviteMessage!'));
		return null as any;
	}

	getMessage(messageId: string) {
		for (const message of this.messages) {
			if (message.messageId === messageId) {
				return message;
			}
		}
		return null;
	}

	hasMessage(messageId: string) {
		return !!this.getMessage(messageId);
	}

	createInviteMessage(sender: User): InviteMessage {
		return InviteMessage.fromChat(this, sender, []);
	}

	isParticipant(user: IUser | string): boolean {
		let email: string = typeof user === 'object' ? user.email : user;
		return !!this.participants.find(
			(participant) => participant.email === email,
		);
	}

	isAdmin(user: IUser | string): boolean {
		let email: string = typeof user === 'object' ? user.email : user;
		return !!this.admins.find((user) => user.email === email);
	}

	addParticipant(participant: User) {
		if (
			!this.participants.find((member) => member.email === participant.email)
		) {
			this.participants.push(participant);
		}
	}

	addMessage(message: AMessage): Promise<any> {
		if (!this.isAllowedToPerformeAction(message)) {
			return Promise.reject(new UserHasNoRightsToPerformeEmailError(message));
		}
		if (!this.hasMessage(message.messageId)) {
			switch (message.chatContent) {
				case ChatContentTypes.TEXT_MESSAGE:
				case ChatContentTypes.JOIN_REQUEST:
				case ChatContentTypes.NEWS:
					this.messages.push(message);
					return Promise.resolve();
				case ChatContentTypes.DELETE_USER_FROM_CHAT:
					const deleteEmail = (message as DeleteMessage).deleteUserEmail;
					// then the user will be deleted from the chat.
					this.admins = this.admins.filter(
						(admin) => admin.email !== deleteEmail,
					);
					this.participants = this.participants.filter(
						(participant) => participant.email !== deleteEmail,
					);
					return DB.getInstance()
						.chatParticipants.where('email')
						.equals(deleteEmail)
						.delete()
						.then(() => {
							this.messages.push(message);
							return Promise.resolve();
						});
				case ChatContentTypes.INVITE:
					(message as InviteMessage).newMembers.forEach((newMember) => {
						this.addParticipant(newMember);
					});
					this.messages.push(message);
					return Promise.resolve();
				case ChatContentTypes.PROMOTE:
					if ((message as PromoteMessage).toAdmin) {
						this.admins.push(
							this.participants.filter(
								(participant) =>
									participant.email ===
									(message as PromoteMessage).promoteUserEmail,
							)[0],
						);
					} else {
						this.admins = this.participants.filter(
							(participant) =>
								participant.email !==
								(message as PromoteMessage).promoteUserEmail,
						);
					}
					this.messages.push(message);
					return Promise.resolve();
				default:
					console.log(message);
					const err = `${message.chatContent} won't be processed in Chat.addMessage`;
					console.error(err);
					return Promise.reject(err);
			}
		} else {
			const error = new Error(
				`Chat already has the message ${message.messageId}`,
			);
			console.error(error);
			return Promise.reject(error);
		}
	}

	isAllowedToPerformeAction(message: AMessage): boolean {
		if (!this.isPublic && !this.isParticipant(message.sender)) {
			return false;
		}
		if (message.chatId !== this.id) {
			console.error(new WrongChatError(message, this.id));
			return false;
		}
		return true;
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | object {
		let json;
		switch (context) {
			case JsonContext.DATABASE:
				json = {
					id: this.id,
					name: this.name,
					creationDate: this.creationDate,
					isPublic: this.isPublic,
					onlyAdminsCanSend: this.onlyAdminsCanSend,
					type: this.type,
				} as any;
				break;
			case JsonContext.INTERNAL:
				json = {
					id: this.id,
					name: this.name,
					creationDate: this.creationDate,
					participants: this.participants.map((participant) => {
						return participant.toJson(false, context);
					}),
					messages: this.messages.map((message) => {
						return message.toJson(false, context);
					}),
					admins: this.admins.map((admin) => {
						return admin.toJson(false, context);
					}),
					isPublic: this.isPublic,
					onlyAdminsCanSend: this.onlyAdminsCanSend,
					type: this.type,
				};
				break;
			case JsonContext.PROXY:
				throw new Error("Chats doesn't support JsonContext.DATABASE!");
		}
		return asString ? JSON.stringify(json) : json;
	}

	static fromJson(json: string | any): AChat {
		console.error(new Error('You must implement AChat.fromJson!'));
		return null as any;
	}

	equals(other: any) {
		console.error(new Error('You must implement AChat.equals!'));
		return false;
	}

	save(): Promise<any> {
		console.error(new Error('You must implement AChat.save!'));
		return Promise.reject();
	}

	delete(): Promise<any> {
		console.error(new Error('You must implement AChat.delete!'));
		return Promise.reject();
	}

	static load(pk: string): Promise<any> {
		console.error(new Error('You must implement AChat.load!'));
		return Promise.reject();
	}

	static genId(name: string = ''): string {
		return `chat-${name.replace(' ', '_')}-${Utils.getRandomString(
			16,
		)}-${new Date().getTime()}`;
	}
}
