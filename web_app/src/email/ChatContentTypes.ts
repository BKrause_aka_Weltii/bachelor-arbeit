export enum ChatContentTypes {
	INVITE = 'invite',
	TEXT_MESSAGE = 'text-message',
	DELETE_USER_FROM_CHAT = 'delete-user-from-chat',
	PROMOTE = 'promote',
	JOIN_REQUEST = 'join-request',
	NEWS = 'news',
	NEW_AD = 'NEW_AD',
	DELETE_AD = 'DELETE_AD',
}
