import { DB } from '../db';

describe('Test DB', function () {
	it('getInstance - returns instance', () => {
		const db = DB.getInstance();
		expect(db).toBeDefined();
		expect(db.lastUser).toBeDefined();
		expect(db.providers).toBeDefined();
		expect(db.messages).toBeDefined();
		expect(db.chats).toBeDefined();
		expect(db.chatParticipants).toBeDefined();
		expect(db.users).toBeDefined();
	});
});
