import sinon from 'sinon';
import { DB } from '../db';

beforeAll(() => {
	spyOn(console, 'log').and.callThrough();
});

afterEach(function () {
	sinon.restore();
});

afterAll(async () => {
	await DB.getInstance().users.clear();
});
