import { DateTime } from 'luxon';
import { Utils } from '../utils';
import { TestUtils } from './TestUtils';
import { EMail } from '../EMail';

describe('Test Utils - snakeCaseToCamelCase', () => {
	it('should replace all underscores ', function () {
		const key = 'i_am_snake_case';
		expect(Utils.snakeCaseToCamelCase(key)).toBe('iAmSnakeCase');
	});

	it('should not fail with _ at the end of string', function () {
		const key = 'i_am_snake_case_';
		expect(Utils.snakeCaseToCamelCase(key)).toBe('iAmSnakeCase');
	});

	it('should replace nothing', function () {
		const key = 'iAmSnakeCase';
		expect(Utils.snakeCaseToCamelCase(key)).toBe('iAmSnakeCase');
	});

	it('should not fail with _ at the start of the string', () => {
		const key = '_i_am_snake_case';
		expect(Utils.snakeCaseToCamelCase(key)).toBe('iAmSnakeCase');
	});
});

describe('Test Utils - camelCaseToSnakeCase', function () {
	it('should replace all upper letters', function () {
		const key = 'iAmCamelCase';
		expect(Utils.camelCaseToSnakeCase(key)).toBe('i_am_camel_case');
	});

	it('should not fail upper case letters at the start', function () {
		const key = 'IAmCamelCase';
		expect(Utils.camelCaseToSnakeCase(key)).toBe('i_am_camel_case');
	});

	it('should not fail with upper case letters at the end', function () {
		const key = 'IAmCamelCaseA';
		expect(Utils.camelCaseToSnakeCase(key)).toBe('i_am_camel_case_a');
	});
});

describe('Test Utils - replaceCamelCaseToSnakeCaseRecursively', () => {
	it('should replace all camel case keys', function () {
		const obj = {
			IAmACamelCaseString: '',
			IAmACamelCaseStringA: '',
			AmACamelCaseString: '',
			AnOtherObject: {
				i_am_valid: '',
				IAmCamelCaseB: '',
			},
		};
		expect(Utils.replaceCamelCaseToSnakeCaseRecursively(obj)).toEqual({
			i_am_a_camel_case_string: '',
			i_am_a_camel_case_string_a: '',
			am_a_camel_case_string: '',
			an_other_object: {
				i_am_valid: '',
				i_am_camel_case_b: '',
			},
		});
	});
});

describe('Test Utils - timeStringToDate', () => {
	it('should parse the string successfully', () => {
		const timeString = 'Wed, 30 Dec 2020 14:46:16 +0000';
		expect(Utils.timeStringToDate(timeString)).toBeDefined();
	});

	it('should fail because its not the right string format', () => {
		const timeString = '30.12.2020';
		try {
			Utils.timeStringToDate(timeString).invalidReason;
		} catch (e) {
			expect(e).toEqual(
				new Error(
					`timestring ${timeString} doesn't match the RFC2822 specification!`,
				),
			);
		}
	});
});

describe('Test Utils - getLastDateFromEMails', () => {
	it('should sort successfully', () => {
		let mills = 1610219010700;
		let iterations = 10;
		const expectedDate = Utils.dateToTimeString(
			DateTime.fromMillis((mills += 10 * 3600 * 1000)),
		);
		let dates = [];
		for (let i = 0; i < iterations; i++) {
			dates.push(Utils.dateToTimeString(DateTime.fromMillis(mills)));
			mills += 3600 * 1000;
		}

		for (let i = 0; i < 20; i++) {
			const first: number = Math.floor(Math.random() * dates.length);
			const second: number = Math.floor(Math.random() * dates.length);
			const tmp: string = dates[first];
			dates[first] = dates[second];
			dates[second] = tmp;
		}

		console.log(dates);

		const emails = dates.map((date) =>
			EMail.fromJson({
				to: [],
				from: '',
				subject: '',
				date: date,
				messageId: '',
				body: '',
			}),
		);

		expect(Utils.getLastDateFromEmails(emails)).toBe(expectedDate);
	});
});

describe('Test Utils - userArrayToString', () => {
	it('should work expected', () => {
		const users = [
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
		];
		const expectedString = `${users[0].nickname}, ${users[1].nickname}, ${users[2].nickname}, ${users[3].nickname}`;
		expect(Utils.userArrayToString(users)).toBe(expectedString);
	});
});
