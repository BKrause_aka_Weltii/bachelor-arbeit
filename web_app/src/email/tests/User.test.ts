import { IUser } from '../dexie_interfaces/IUser';
import { User } from '../User';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { TestUtils } from './TestUtils';
import { JsonContext } from '../interfaces/json-rebuildable';

describe('Test User - fromJson', () => {
	it('should return valid object from valid object', function () {
		const data: IUser = TestUtils.generateRandomUser().toJson(
			false,
			JsonContext.INTERNAL,
		) as IUser;
		const user = User.fromJson(data);
		expect(user.email).toBe(data.email);
		expect(user.nickname).toBe(data.nickname as string);
	});

	it('should return valid object from invalid object', function () {
		const data: any = {
			email: null,
			nickname: undefined,
		};
		const user = User.fromJson(data);
		expect(user.email).toBe('');
		expect(user.nickname).toBe('' as string);
	});

	it('should return valid object from valid string', function () {
		const expectedUser: User = TestUtils.generateRandomUser();
		const string = expectedUser.toJson(true);
		const user = User.fromJson(string);
		expect(user.email).toBe(expectedUser.email);
		expect(user.nickname).toBe(expectedUser.nickname);
	});

	it('should return valid object from invalid string', function () {
		const string = '{}';
		const user = User.fromJson(string);
		expect(user.email).toBe('');
		expect(user.nickname).toBe('');
	});
});

describe('Test User - toJson', () => {
	it('should return valid json string', function () {
		const user: User = TestUtils.generateRandomUser();
		expect(user.toJson(true)).toBe(
			`{"email":"${user.email}","nickname":"${user.nickname}"}`,
		);
	});
	it('should return valid json object', function () {
		const user: User = TestUtils.generateRandomUser();
		expect(user.toJson()).toEqual({
			email: user.email,
			nickname: user.nickname,
		});
	});
	it('should return valid object for database context', function () {
		const user: User = TestUtils.generateRandomUser();
		expect(user.toJson(false, JsonContext.DATABASE)).toEqual({
			email: user.email,
			nickname: user.nickname,
		});
	});
});

describe('Test User - save', () => {
	it('should save user to database', function (done) {
		const user: User = TestUtils.generateRandomUser();
		user.save().then((msg) => {
			expect(msg).toBe(user.email);
			done();
		});
	});

	it('should save user to database with invalid data', function (done) {
		const user = new User('', '');
		user.save().then((msg) => {
			expect(msg).toBe('');
			done();
		});
	});
});

describe('Test User - load', () => {
	it('should load user from database', async function (done) {
		const expectedUser = TestUtils.generateRandomUser();
		await expectedUser.save();
		User.load(expectedUser.email).then((user) => {
			expect(user).toEqual(expectedUser);
			done();
		});
	});

	it('should fail because no user can be found', function (done) {
		const user: User = TestUtils.generateRandomUser();
		User.load(user.email).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`User with email: ${user.email}`),
			);
			done();
		});
	});
});

describe('Test User - delete', () => {
	it('should delete user from database', async function (done) {
		const expectedUser = TestUtils.generateRandomUser();
		expectedUser
			.save()
			.then((msg) => {
				expect(msg).toBe(expectedUser.email);
				return User.load(expectedUser.email);
			})
			.then((user) => {
				expect(user).toEqual(expectedUser);
				return user.delete();
			})
			.then((data) => {
				expect(data).toBe(1);
				done();
			});
	});

	it('should remove nothing from the database', function (done) {
		const expectedUser = TestUtils.generateRandomUser();
		expectedUser.delete().then((err) => {
			expect(err).toBe(0);
			done();
		});
	});
});
