import { UserSettings } from '../UserSettings';

describe('Test UserSettings - init', () => {
	it('should init successfully', (done) => {
		UserSettings.load().then((settings: UserSettings) => {
			console.log(settings.settings);
			expect(settings).toBeDefined();
			expect(settings.settings);
			done();
		});
	});
});
