import { Ad } from '../Ad';
import { TestUtils } from './TestUtils';
import { PriceType } from '../dexie_interfaces/IAd';
import { JsonContext } from '../interfaces/json-rebuildable';
import { NotUseableInJsonContextError } from '../exceptions/NotUseableInJsonContextError';
import { Chat } from '../Chat';
import { NotFoundInDatabase } from '../NotFoundInDatabase';

function generateAd(custom?: {
	id?: string;
	creator?: User;
	chatId?: string;
	priceType?: PriceType;
	priceAmount?: string;
}) {
	const id = custom?.id || Ad.genId();
	const creator = custom?.creator || TestUtils.generateRandomUser();
	const chatId =
		custom?.chatId ||
		Chat.genId(`A-random-chat-id-${TestUtils.generateNumber()}`);
	return new Ad(
		id,
		creator,
		chatId,
		`title-${id}`,
		'A test description',
		custom?.priceType || PriceType.SELL,
		custom?.priceAmount || '200',
	);
}

describe('Test Ad - toJson', () => {
	it('should return expected json for internal context', () => {
		const message = generateAd();
		const expectedJson = {
			id: message.id,
			creator: {
				email: message.creator.email,
				nickname: message.creator.nickname,
			},
			chatId: message.chatId,
			title: message.title,
			description: message.description,
			priceType: message.priceType,
			priceAmount: message.priceAmount,
		};
		expect(message.toJson()).toEqual(expectedJson);
	});

	it('should return expected json for database context', () => {
		const message = generateAd();
		const expectedJson = {
			id: message.id,
			creator: message.creator.email,
			chatId: message.chatId,
			title: message.title,
			description: message.description,
			priceType: message.priceType,
			priceAmount: message.priceAmount,
		};
		expect(message.toJson(false, JsonContext.DATABASE)).toEqual(expectedJson);
	});

	it('should throw NotUseableInJsonContext with context prox', () => {
		const message = generateAd();
		try {
			message.toJson(false, JsonContext.PROXY);
		} catch (e) {
			expect(e).toEqual(
				new NotUseableInJsonContextError('Ad.toJson', JsonContext.PROXY),
			);
		}
	});
});

describe('Test Ad - save', () => {
	it('should save successfully', (done) => {
		const ad: Ad = generateAd();
		ad.creator
			.save()
			.then((email) => {
				expect(email).toBe(ad.creator.email);
				return ad.save();
			})
			.then((adId: string) => {
				expect(adId).toBe(ad.id);
				done();
			});
	});
});

describe('Test Ad - delete', () => {
	it('should delete successfully', (done) => {
		const ad: Ad = generateAd();
		ad.creator
			.save()
			.then((email) => {
				expect(email).toBe(ad.creator.email);
				return ad.save();
			})
			.then((adId: string) => {
				expect(adId).toBe(ad.id);
				return ad.delete();
			})
			.then((deletedRowsCount: number) => {
				expect(deletedRowsCount).toBe(1);
				done();
			});
	});

	it('should delete nothing because there is no ad to delete.', (done) => {
		const ad: Ad = generateAd();
		ad.delete().then((deletedRowsCount: number) => {
			expect(deletedRowsCount).toBe(0);
			done();
		});
	});
});

describe('Test Ad - equals', () => {
	it('should be equal', () => {
		const ad = generateAd();
		const ad2 = generateAd({
			id: ad.id,
			creator: ad.creator,
			chatId: ad.chatId,
		});
		expect(ad.equals(ad2)).toBeTrue();
	});

	it('should not be equal', () => {
		const ad = generateAd();
		const ad2 = generateAd({
			priceType: PriceType.FOUND,
		});

		expect(ad.equals(ad2)).toBeFalse();
	});
});

describe('Test Ad - load', () => {
	it('should load successfully', (done) => {
		const ad: Ad = generateAd();
		ad.creator
			.save()
			.then((email) => {
				expect(email).toBe(ad.creator.email);
				return ad.save();
			})
			.then((adId: string) => {
				expect(adId).toBe(ad.id);
				return Ad.load(adId);
			})
			.then((adResult: Ad) => {
				expect(ad.equals(adResult)).toBeTrue();
				done();
			});
	});

	it('should faile because there is no data', (done) => {
		const id = 'id-not-found';
		Ad.load(id).catch((err) => {
			expect(err).toEqual(new NotFoundInDatabase(`Ad with id ${id}`));
			done();
		});
	});
});
