import { EMail } from '../EMail';
import { User } from '../User';
import { Chat } from '../Chat';
import { LoginUser } from '../LoginUser';
import { EMailDefaults } from '../../Config';
import { InviteMessage } from '../content_messages/InviteMessage';
import { Utils } from '../utils';
// @ts-ignore
import sinonStubPromise from 'sinon-stub-promise';
import sinon from 'sinon';
import { ChatType } from '../dexie_interfaces/IChat';
sinonStubPromise(sinon);

export namespace TestUtils {
	export function generateRandomUser() {
		const random = generateNumber();
		return new User(`${random}-user@example.net`, random);
	}

	export function generateNumber() {
		return Math.random().toString(16);
	}

	export function getFormattedTimeString() {
		return 'Sun, 20 Sep 2020 12:08:42 +0200';
	}

	export function generateEmails(count: number, chatId: string): Array<EMail> {
		const emails: Array<EMail> = [];
		for (let x = 0; x < count; x++) {
			const random = generateNumber();
			const email = `${random}-user@example.net`;
			emails.push(
				new EMail(
					{
						from: email,
						to: ['herbert@von-schlecht.er', 'sophie@example.net'],
						chatId: chatId,
						subject: '',
						date: getFormattedTimeString(),
						messageId: EMail.genId(),
						mimeVersion: EMailDefaults.mimeVersion,
						contentType: EMailDefaults.contentType,
						additionalHeaderFields: {},
						chatVersion: EMailDefaults.chatVersion,
						chatContent: EMailDefaults.chatContent,
					},
					'some body',
				),
			);
		}
		return emails;
	}

	export function generateParticipants(count: number): Array<User> {
		const participants: Array<User> = [];
		for (let x = 0; x < count; x++) {
			participants.push(generateRandomUser());
		}
		return participants;
	}

	export function generateMessages(count: number, chatId: string) {
		let messages = [];
		for (let x = 0; x < count; x++) {
			const from = generateRandomUser();
			const chatIdNumber = generateNumber();
			const creationDate = getFormattedTimeString();
			messages.push(
				new InviteMessage(
					from,
					`a-message-id-${generateNumber()}`,
					chatId,
					chatId,
					creationDate,
					[generateRandomUser()],
					[from],
					[from],
					creationDate,
					false,
					false,
					ChatType.NORMAL,
				),
			);
		}
		return messages;
	}

	export function generateChat(
		messageCount: number,
		participantsCount: number,
		isPublic?: boolean,
		onlyAdminsCanSend?: boolean,
		type?: ChatType,
	) {
		const id = `a-id-${new Date().getTime()}`;
		const participants = generateParticipants(participantsCount);
		const admins = [participants[0]];
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		let messages = [];
		for (let i = 0; i < messageCount; i++) {
			messages.push(
				new InviteMessage(
					participants[0],
					EMail.genId(),
					id,
					id,
					date,
					participants,
					admins,
					admins,
					date,
					isPublic || false,
					onlyAdminsCanSend || false,
					ChatType.NORMAL,
				),
			);
		}
		return new Chat(
			id,
			id,
			Utils.dateToTimeString(Utils.getCurrentDateTime()),
			messages,
			participants,
			admins,
			isPublic || false,
			onlyAdminsCanSend || false,
		);
	}

	export function generateRandomLoginUser() {
		const random = generateNumber();
		const email = `${random}-user@example.net`;
		const id = Chat.genId();
		const chats = [
			Chat.fromJson({
				id: `${id}-${generateNumber()}`,
				name: `Chat-${id}`,
				participants: [new User(email, id)],
			}),
		];
		return new LoginUser(
			email,
			random,
			chats,
			email,
			`a-${random}-password`,
			'example.net',
		);
	}

	export function generateEMail(additionalHeaderFields: any = {}) {
		return new EMail(
			{
				from: 'herbert@von-schlecht.er',
				to: ['sophie@example.net', 'somebody@example.net'],
				subject: 'Something in here',
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: `a-test-message-id-${new Date().getTime()}`,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				additionalHeaderFields: additionalHeaderFields,
				chatId: '0',
				chatVersion: EMailDefaults.chatVersion,
				chatContent: EMailDefaults.chatContent,
			},
			'A test message.',
		);
	}

	export function mockFetch(mockFunction: Function) {
		sinon.stub(window, 'fetch');
		// @ts-ignore
		window.fetch.returns(Promise.resolve(mockFunction()));
	}
}
