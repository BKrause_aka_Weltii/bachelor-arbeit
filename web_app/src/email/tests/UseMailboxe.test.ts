import { User } from '../User';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { TestUtils } from './TestUtils';
import { UseMailbox } from '../UseMailbox';
import { DB } from '../db';

function generateUseMailbox(use: boolean = false) {
	return new UseMailbox(`Mailbox-${TestUtils.generateNumber()}`, use);
}

describe('Test UseMailbox - save', () => {
	it('should save user to database', function (done) {
		const useMailbox: UseMailbox = generateUseMailbox();
		useMailbox.save().then((msg: string) => {
			expect(msg).toBe(useMailbox.mailbox);
			done();
		});
	});
});

describe('Test UseMailbox - load', () => {
	it('should load useMailbox from database', async function (done) {
		const useMailbox: UseMailbox = generateUseMailbox();
		await useMailbox.save();
		UseMailbox.load(useMailbox.mailbox).then((mailbox) => {
			expect(mailbox).toEqual(useMailbox);
			done();
		});
	});

	it('should fail because no mailbox can be found', function (done) {
		const useMailbox: UseMailbox = generateUseMailbox();
		UseMailbox.load(useMailbox.mailbox).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`Mailbox: ${useMailbox.mailbox}`),
			);
			done();
		});
	});
});

describe('Test UseMailbox - delete', () => {
	it('should delete useMailbox from database', async function (done) {
		const useMailbox: UseMailbox = generateUseMailbox();
		useMailbox
			.save()
			.then((msg) => {
				expect(msg).toBe(useMailbox.mailbox);
				return UseMailbox.load(useMailbox.mailbox);
			})
			.then((useMailboxResult) => {
				expect(useMailboxResult).toEqual(useMailbox);
				return useMailboxResult.delete();
			})
			.then((data) => {
				expect(data).toBe(1);
				done();
			});
	});

	it('should remove nothing from the database', function (done) {
		const useMailbox: UseMailbox = generateUseMailbox();
		useMailbox.delete().then((err) => {
			expect(err).toBe(0);
			done();
		});
	});
});

describe('Test UseMailbox - loadAll', () => {
	it('should load all successfully', (done) => {
		const useMailboxes: UseMailbox[] = [
			generateUseMailbox(),
			generateUseMailbox(),
			generateUseMailbox(),
			generateUseMailbox(),
		];
		DB.getInstance()
			.mailboxes.clear()
			.then(() => {
				return Promise.all(
					useMailboxes.map((mailbox: UseMailbox) => {
						mailbox.save();
					}),
				)
					.then(() => {
						return UseMailbox.loadAll();
					})
					.then((mailboxesResult) => {
						expect(mailboxesResult.length).toBe(4);
						done();
					});
			});
	});

	it('should return empty array because there are not useMailboxes', (done) => {
		DB.getInstance()
			.mailboxes.clear()
			.then(() => {
				return UseMailbox.loadAll();
			})
			.then((mailboxesResult) => {
				expect(mailboxesResult.length).toBe(0);
				done();
			});
	});
});

describe('Test UseMailbox - bulkSave', () => {
	it('should load all successfully', (done) => {
		const useMailboxes: UseMailbox[] = [
			generateUseMailbox(),
			generateUseMailbox(),
			generateUseMailbox(),
			generateUseMailbox(),
		];
		UseMailbox.bulkSave(useMailboxes).then((result) => {
			expect(result.length).toBe(4);
			done();
		});
	});
});

describe('Test UseMailbox - getOnlyUsed', () => {
	it('should return only used mailboxes', (done) => {
		const useMailboxes: UseMailbox[] = [
			generateUseMailbox(),
			generateUseMailbox(true),
			generateUseMailbox(),
			generateUseMailbox(),
		];
		DB.getInstance()
			.mailboxes.clear()
			.then(() => {
				return Promise.all(
					useMailboxes.map((mailbox: UseMailbox) => {
						mailbox.save();
					}),
				)
					.then(() => {
						return UseMailbox.getOnlyUsed();
					})
					.then((mailboxesResult) => {
						expect(mailboxesResult.length).toBe(1);
						expect(mailboxesResult[0].mailbox).toEqual(useMailboxes[1].mailbox);
						done();
					});
			});
	});

	it('should return empty array because there are not useMailboxes', (done) => {
		DB.getInstance()
			.mailboxes.clear()
			.then(() => {
				return UseMailbox.getOnlyUsed();
			})
			.then((mailboxesResult) => {
				expect(mailboxesResult.length).toBe(0);
				done();
			});
	});
});
