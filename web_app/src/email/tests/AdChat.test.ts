import { ChatType } from '../dexie_interfaces/IChat';
import { TestUtils } from './TestUtils';
import { AdChat } from '../AdChat';
import { Ad } from '../Ad';
import { User } from '../User';
import { PriceType } from '../dexie_interfaces/IAd';
import { JsonContext } from '../interfaces/json-rebuildable';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { NewAdMessage } from '../content_messages/NewAdMessage';
import { EMail } from '../EMail';
import { Utils } from '../utils';
import { UserHasNoRightsToPerformeEmailError } from '../exceptions/UserHasNoRightsToPerformeEmailError';
import { DeleteAdMessage } from '../content_messages/DeleteAdMessage';
import { InviteMessage } from '../content_messages/InviteMessage';
import { AChat } from '../AChat';

function generateNewAd(creator: User, chatId: string) {
	return new Ad(
		Ad.genId(),
		creator,
		chatId,
		`ad.title-${chatId}`,
		'A description',
		PriceType.SELL,
		'200€',
	);
}

function generateAdChat(messages: number = 2, ads: number = 2) {
	const chat = TestUtils.generateChat(messages, 2, true, true);
	let adsList = [];
	for (let i = 0; i < ads; i++) {
		adsList.push(generateNewAd(chat.participants[0], chat.id));
	}
	return new AdChat(
		chat.id,
		chat.name,
		chat.creationDate,
		chat.messages,
		chat.participants,
		chat.admins,
		adsList,
		chat.isPublic,
		chat.onlyAdminsCanSend,
	);
}

describe('Test AdChat - fromJson', () => {
	it('should load successfully', function () {
		const expectedChat: AdChat = generateAdChat();
		const chatJson: any = {
			id: expectedChat.id,
			name: expectedChat.name,
			creationDate: expectedChat.creationDate,
			participants: [
				{
					email: expectedChat.participants[0].email,
					nickname: expectedChat.participants[0].nickname,
				},
				{
					email: expectedChat.participants[1].email,
					nickname: expectedChat.participants[1].nickname,
				},
			],
			messages: [
				expectedChat.messages[0].toJson(false, JsonContext.INTERNAL),
				expectedChat.messages[1].toJson(false, JsonContext.INTERNAL),
			],
			admins: [expectedChat.admins[0].toJson(false, JsonContext.INTERNAL)],
			isPublic: true,
			ads: expectedChat.ads.map((ad) => ad.toJson()),
			onlyAdminsCanSend: true,
			type: ChatType.AD,
		};
		const chat: AdChat = AdChat.fromJson(chatJson);
		expect(chat).toEqual(expectedChat);
	});
});

describe('Test AdChat - save', () => {
	it('should save successfully', (done) => {
		const chat = generateAdChat();
		chat.save().then((result: any) => {
			expect(result.participants.length).toEqual(2);
			expect(result.messages.length).toEqual(2);
			expect(result.chat).toBe(chat.id);
			expect(result.users.length).toBe(2);
			expect(result.ads.length).toBe(2);
			done();
		});
	});
});

describe('Test AdChat - load', () => {
	it('should load successfully', function (done) {
		const expectedChat = generateAdChat();
		expectedChat
			.save()
			.then((result: any) => {
				expect(result.users).toEqual([
					expectedChat.participants[0].email,
					expectedChat.participants[1].email,
				]);
				expect(result.participants).toEqual([
					[expectedChat.id, expectedChat.participants[0].email],
					[expectedChat.id, expectedChat.participants[1].email],
				]);
				expect(result.messages.length).toEqual(2);
				expect(result.chat).toBe(expectedChat.id);
				expect(result.ads.length).toBe(2);
				return AdChat.load(expectedChat.id);
			})
			.then((chat: AdChat) => {
				expect(chat.equals(expectedChat)).toBeTrue();
				done();
			});
	});

	it('should fail because no data is available', function (done) {
		const expectedChat = generateAdChat();
		AdChat.load(expectedChat.id).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`AdChat with id: ${expectedChat.id}`),
			);
			done();
		});
	});
});

describe('Test AdChat - addMessage', () => {
	it('should add new ad with NewAdMessage', (done) => {
		const chat = generateAdChat(0, 0);
		const message = new NewAdMessage(
			chat.participants[0],
			EMail.genId(),
			chat.id,
			Utils.dateToTimeString(Utils.getCurrentDateTime()),
			Ad.genId(),
			'A title',
			'A description',
			PriceType.SELL,
			'200€',
		);
		chat.addMessage(message).then(() => {
			expect(chat.ads.length).toBe(1);
			expect(chat.ads[0].equals(Ad.fromNewAdMessage(message))).toBeTrue();
			done();
		});
	});

	it('should fail because the user is not permitted', (done) => {
		const chat = generateAdChat(0, 0);
		const message = new NewAdMessage(
			TestUtils.generateRandomUser(),
			EMail.genId(),
			chat.id,
			Utils.dateToTimeString(Utils.getCurrentDateTime()),
			Ad.genId(),
			'A title',
			'A description',
			PriceType.SELL,
			'200€',
		);
		chat.addMessage(message).catch((err) => {
			expect(err).toEqual(new UserHasNoRightsToPerformeEmailError(message));
			done();
		});
	});

	it('should replace vars from an existing ad', (done) => {
		const chat: AdChat = generateAdChat(1, 1);
		const expectedAd = new Ad(
			chat.ads[0].id,
			chat.participants[0],
			chat.id,
			'An other title',
			'An new description',
			PriceType.FOUND,
			'-',
		);
		const message = new NewAdMessage(
			expectedAd.creator,
			EMail.genId(),
			expectedAd.chatId,
			Utils.dateToTimeString(Utils.getCurrentDateTime()),
			expectedAd.id,
			expectedAd.title,
			expectedAd.description,
			expectedAd.priceType,
			expectedAd.priceAmount,
		);
		chat.addMessage(message).then(() => {
			expect(chat.ads.length).toBe(1);
			console.log(chat.ads[0]);
			expect(chat.ads[0].equals(expectedAd)).toBeTrue();
			done();
		});
	});

	it('should delete ad with DeleteAdMessage', (done) => {
		const chat = generateAdChat(1, 1);
		const message = new DeleteAdMessage(
			chat.ads[0].creator,
			EMail.genId(),
			chat.id,
			Utils.dateToTimeString(Utils.getCurrentDateTime()),
			chat.ads[0].id,
			chat.ads[0].title,
		);
		chat.addMessage(message).then(() => {
			expect(chat.ads.length).toBe(0);
			done();
		});
	});

	it('should fail to delete ad with DeleteAdMessage because user is not permitted', (done) => {
		const chat = generateAdChat(1, 1);
		const message = new DeleteAdMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			Utils.dateToTimeString(Utils.getCurrentDateTime()),
			chat.ads[0].id,
			chat.ads[0].title,
		);
		chat.addMessage(message).catch((err) => {
			expect(err).toEqual(new UserHasNoRightsToPerformeEmailError(message));
			done();
		});
	});

	it('should fail to delete ad with DeleteAdMessage because no ad was found', (done) => {
		const chat = generateAdChat(1, 1);
		const message = new DeleteAdMessage(
			chat.ads[0].creator,
			EMail.genId(),
			chat.id,
			Utils.dateToTimeString(Utils.getCurrentDateTime()),
			'non existing id',
			chat.ads[0].title,
		);
		chat.addMessage(message).catch((err) => {
			expect(err).toEqual(new Error(`Ad ${message.adId} not found!`));
			done();
		});
	});
});

describe('Test AdChat - fromInviteMessage', () => {
	it('should create successfully', () => {
		const to = [
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
		];
		const chatId = AChat.genId();
		const invite = new InviteMessage(
			to[0],
			EMail.genId(),
			chatId,
			chatId,
			'',
			[TestUtils.generateRandomUser()],
			to,
			[to[0]],
			'',
			false,
			false,
			ChatType.AD,
		);
		const expectedChat = new AdChat(
			chatId,
			chatId,
			'',
			[invite],
			invite.newMembers.concat(to),
			[to[0]],
			[],
			false,
			false,
		);
		expect(AdChat.fromInviteMessage(invite)).toEqual(expectedChat);
	});
});
