import { Chat } from '../Chat';
import { TestUtils } from './TestUtils';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { JsonContext } from '../interfaces/json-rebuildable';
import generateChat = TestUtils.generateChat;
import { InviteMessage } from '../content_messages/InviteMessage';
import { EMail } from '../EMail';
import { Utils } from '../utils';
import { UserHasNoRightsToPerformeEmailError } from '../exceptions/UserHasNoRightsToPerformeEmailError';
import { TextMessage } from '../content_messages/TextMessage';
import { DeleteMessage } from '../content_messages/DeleteMessage';
import { PromoteMessage } from '../content_messages/PromoteMessage';
import { ChatType } from '../dexie_interfaces/IChat';
import { NewAdMessage } from '../content_messages/NewAdMessage';
import { Ad } from '../Ad';
import { PriceType } from '../dexie_interfaces/IAd';

describe('Test Chat - toJson', () => {
	it('should return correct json for internal context', function () {
		const chat = generateChat(1, 2);
		const json = chat.toJson(false, JsonContext.INTERNAL);
		const expectedJson = {
			id: chat.id,
			name: chat.name,
			creationDate: chat.creationDate,
			participants: [
				{
					email: chat.participants[0].email,
					nickname: chat.participants[0].nickname,
				},
				{
					email: chat.participants[1].email,
					nickname: chat.participants[1].nickname,
				},
			],
			messages: [chat.messages[0].toJson(false, JsonContext.INTERNAL)],
			admins: [chat.admins[0].toJson(false, JsonContext.INTERNAL)],
			isPublic: false,
			onlyAdminsCanSend: false,
			type: ChatType.NORMAL,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should return correct json for database context', function () {
		const chat = generateChat(1, 2, false);
		expect(chat.toJson(false, JsonContext.DATABASE)).toEqual({
			id: chat.id,
			name: chat.name,
			creationDate: chat.creationDate,
			isPublic: false,
			onlyAdminsCanSend: false,
			type: ChatType.NORMAL,
		});
	});
});

describe('Test Chat - fromJson', () => {
	it('should load successfully', function () {
		const expectedChat = generateChat(1, 2, true, false, ChatType.AD);
		const chatJson: any = {
			id: expectedChat.id,
			name: expectedChat.name,
			creationDate: expectedChat.creationDate,
			participants: [
				{
					email: expectedChat.participants[0].email,
					nickname: expectedChat.participants[0].nickname,
				},
				{
					email: expectedChat.participants[1].email,
					nickname: expectedChat.participants[1].nickname,
				},
			],
			messages: [expectedChat.messages[0].toJson(false, JsonContext.INTERNAL)],
			admins: [expectedChat.admins[0].toJson(false, JsonContext.INTERNAL)],
			isPublic: true,
			onlyAdminsCanSend: false,
			type: ChatType.AD,
		};
		const chat = Chat.fromJson(chatJson);
		expect(chat).toEqual(expectedChat);
	});

	it('should load successfully without type', function () {
		const expectedChat = generateChat(1, 2, true);
		const chatJson: any = {
			id: expectedChat.id,
			name: expectedChat.name,
			creationDate: expectedChat.creationDate,
			participants: [
				{
					email: expectedChat.participants[0].email,
					nickname: expectedChat.participants[0].nickname,
				},
				{
					email: expectedChat.participants[1].email,
					nickname: expectedChat.participants[1].nickname,
				},
			],
			messages: [expectedChat.messages[0].toJson(false, JsonContext.INTERNAL)],
			admins: [expectedChat.admins[0].toJson(false, JsonContext.INTERNAL)],
			isPublic: true,
			onlyAdminsCanSend: false,
			type: ChatType.NORMAL,
		};
		const chat = Chat.fromJson(chatJson);
		expect(chat).toEqual(expectedChat);
	});
});

describe('Test Chat - save', () => {
	it('should save successfully', (done) => {
		const chat = generateChat(4, 2);
		chat.save().then((result: any) => {
			expect(result.participants.length).toEqual(2);
			expect(result.messages.length).toEqual(4);
			expect(result.chat).toBe(chat.id);
			expect(result.users.length).toBe(2);
			done();
		});
	});
});

describe('Test Chat - load', () => {
	it('should load successfully', function (done) {
		const expectedChat = generateChat(1, 2);
		expectedChat
			.save()
			.then((result: any) => {
				expect(result.users).toEqual([
					expectedChat.participants[0].email,
					expectedChat.participants[1].email,
				]);
				expect(result.participants).toEqual([
					[expectedChat.id, expectedChat.participants[0].email],
					[expectedChat.id, expectedChat.participants[1].email],
				]);
				expect(result.messages.length).toEqual(1);
				expect(result.chat).toBe(expectedChat.id);
				return Chat.load(expectedChat.id);
			})
			.then((chat: Chat) => {
				expect(chat.equals(expectedChat)).toBeTrue();
				done();
			});
	});

	it('should fail because no data is available', function (done) {
		const expectedChat = generateChat(4, 2);
		Chat.load(expectedChat.id).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`Chat with id: ${expectedChat.id}`),
			);
			done();
		});
	});
});

describe('Test Chat - delete', () => {
	it('should delete successfully', function (done) {
		const expectedChat = generateChat(4, 2);
		expectedChat
			.save()
			.then((result: any) => {
				expect(result.participants.length).toEqual(2);
				console.log('saveResult', result);
				expect(result.messages.length).toEqual(4);
				expect(result.chat).toBe(expectedChat.id);
				return expectedChat.delete();
			})
			.then((deleteResult: any) => {
				expect(deleteResult).toEqual({
					messages: 4,
					participants: 2,
					chat: 1,
				});
				done();
			});
	});

	it('should delete nothing, because there is no chat', function (done) {
		generateChat(4, 2)
			.delete()
			.then((deleteResult: any) => {
				expect(deleteResult).toEqual({
					messages: 0,
					participants: 0,
					chat: 0,
				});
				done();
			});
	});
});

describe('Test Chat - isAllowedToPerformeAction', () => {
	it('should dissmiss all message not from', () => {
		const chat = TestUtils.generateChat(2, 2, false, true);
		const admin = chat.admins[0];
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const hasRightTextMessage = new TextMessage(
			admin,
			EMail.genId(),
			chat.id,
			date,
			'A simple text message',
		);
		const hasNoRightTextMessage = new TextMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			date,
			'A simple text message',
		);
		expect(chat.isAllowedToPerformeAction(hasRightTextMessage)).toBeTrue();
		expect(chat.isAllowedToPerformeAction(hasNoRightTextMessage)).toBeFalse();
	});

	it('should allow INVITE correctly', () => {
		const chat = TestUtils.generateChat(2, 2);
		const admin = chat.admins[0];
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const hasRightInviteMessage = new InviteMessage(
			admin,
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[TestUtils.generateRandomUser()],
			chat.participants,
			[admin],
			date,
			false,
			false,
			ChatType.NORMAL,
		);
		const hasNoRightInviteMessage = new InviteMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[TestUtils.generateRandomUser()],
			chat.participants,
			[admin],
			date,
			false,
			false,
			ChatType.NORMAL,
		);
		expect(chat.isAllowedToPerformeAction(hasRightInviteMessage)).toBeTrue();
		expect(chat.isAllowedToPerformeAction(hasNoRightInviteMessage)).toBeFalse();
	});

	it('should allow INVITE for public chat correctly', () => {
		const chat = TestUtils.generateChat(2, 2, true, false);
		const admin = chat.admins[0];
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const hasRightInviteMessage = new InviteMessage(
			admin,
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[TestUtils.generateRandomUser()],
			chat.participants,
			[admin],
			date,
			true,
			false,
			ChatType.NORMAL,
		);
		const hasRightInviteMessage2 = new InviteMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[TestUtils.generateRandomUser()],
			chat.participants,
			[admin],
			date,
			true,
			false,
			ChatType.NORMAL,
		);
		expect(chat.isAllowedToPerformeAction(hasRightInviteMessage)).toBeTrue();
		expect(chat.isAllowedToPerformeAction(hasRightInviteMessage2)).toBeTrue();
	});

	it('should dismiss TEXT_MESSAGE for onyAdminCanSend chats', () => {
		const chat = TestUtils.generateChat(2, 2, false, true);
		const admin = chat.admins[0];
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const hasRightInviteMessage = new InviteMessage(
			admin,
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[TestUtils.generateRandomUser()],
			chat.participants,
			[admin],
			date,
			false,
			true,
			ChatType.NORMAL,
		);
		const hasNoRightInviteMessage = new InviteMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[TestUtils.generateRandomUser()],
			chat.participants,
			[admin],
			date,
			false,
			true,
			ChatType.NORMAL,
		);
		expect(chat.isAllowedToPerformeAction(hasRightInviteMessage)).toBeTrue();
		expect(chat.isAllowedToPerformeAction(hasNoRightInviteMessage)).toBeFalse();
	});

	it('should allow TEXT_MESSAGE correctly', () => {
		const chat = TestUtils.generateChat(2, 2);
		const admin = chat.participants[0];
		chat.admins = [admin];
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const hasRightTextMessage = new TextMessage(
			admin,
			EMail.genId(),
			chat.id,
			date,
			'Has rights to performe action',
		);
		const hasRightTextMessage2 = new TextMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			date,
			'Has rights to performe action',
		);
		// Has no rights, because he is not in the chat
		const hasNoRightTextMessage = new TextMessage(
			TestUtils.generateRandomUser(),
			EMail.genId(),
			chat.id,
			date,
			'Has no rights to performe action!',
		);
		expect(chat.isAllowedToPerformeAction(hasRightTextMessage)).toBeTrue();
		expect(chat.isAllowedToPerformeAction(hasRightTextMessage2)).toBeTrue();
		expect(chat.isAllowedToPerformeAction(hasNoRightTextMessage)).toBeFalse();
	});

	it('should allow DELETE_USER_FROM_CHAT correctly', () => {
		const chat = TestUtils.generateChat(2, 2);
		const admin = chat.participants[0];
		chat.admins = [admin];
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const hasRightDeleteMessage = new DeleteMessage(
			admin,
			EMail.genId(),
			chat.id,
			date,
			chat.participants[1].email,
		);
		const hasRightDeleteMessage2 = new DeleteMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			date,
			chat.participants[1].email,
		);
		// Has no rights, because he is not in the chat
		const hasNoRightDeleteMessage = new DeleteMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			date,
			admin.email,
		);
		expect(chat.isAllowedToPerformeAction(hasRightDeleteMessage)).toBeTrue();
		expect(chat.isAllowedToPerformeAction(hasRightDeleteMessage2)).toBeTrue();
		expect(chat.isAllowedToPerformeAction(hasNoRightDeleteMessage)).toBeFalse();
	});

	it('should allow PROMOTE correctly', () => {
		const chat = TestUtils.generateChat(2, 2);
		const admin = chat.participants[0];
		chat.admins = [admin];
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const hasRightPromoteMessage = new PromoteMessage(
			admin,
			EMail.genId(),
			chat.id,
			date,
			chat.participants[1].email,
			true,
		);
		const hasRightPromoteMessage2 = new PromoteMessage(
			admin,
			EMail.genId(),
			chat.id,
			date,
			chat.participants[1].email,
			false,
		);
		// Has no rights, because he is not in the chat
		const hasNoRightPromoteMessage = new PromoteMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			date,
			chat.participants[1].email,
			true,
		);
		expect(chat.isAllowedToPerformeAction(hasRightPromoteMessage)).toBeTrue();
		expect(chat.isAllowedToPerformeAction(hasRightPromoteMessage2)).toBeTrue();
		expect(
			chat.isAllowedToPerformeAction(hasNoRightPromoteMessage),
		).toBeFalse();
	});

	// it('should allow NEW_AD correctly', () => {
	// 	const chat = TestUtils.generateChat(2, 2);
	// 	const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
	// 	const hasRightNewAdMessage = new NewAdMessage(
	// 		chat.participants[0],
	// 		EMail.genId(),
	// 		chat.id,
	// 		date,
	// 		Ad.genId(),
	// 		`NewAdMessage-1-${new Date().getTime()}`,
	// 		'Description',
	// 		PriceType.SELL,
	// 		'200',
	// 	);
	// 	// Has no rights, because he is not in the chat
	// 	const hasNoRightNewAdMessage = new NewAdMessage(
	// 		TestUtils.generateRandomUser(),
	// 		EMail.genId(),
	// 		chat.id,
	// 		date,
	// 		Ad.genId(),
	// 		`NewAdMessage-2-${new Date().getTime()}`,
	// 		'Description',
	// 		PriceType.SELL,
	// 		'200',
	// 	);
	// 	expect(chat.isAllowedToPerformeAction(hasRightNewAdMessage)).toBeTrue();
	// 	expect(chat.isAllowedToPerformeAction(hasNoRightNewAdMessage)).toBeFalse();
	// });
});

describe('Tests Chat - addMessage', () => {
	it('should faile because the message has not the right chat-id', (done) => {
		const chat = TestUtils.generateChat(0, 2);
		const message = new TextMessage(
			chat.participants[0],
			EMail.genId(),
			'not-the-right-chat-id',
			Utils.dateToTimeString(Utils.getCurrentDateTime()),
			'A message',
		);
		chat.addMessage(message).catch((err: any) => {
			expect(err).toEqual(new UserHasNoRightsToPerformeEmailError(message));
			done();
		});
	});

	it('should consume INVITE message successfully', (done) => {
		const chat = TestUtils.generateChat(0, 2);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new InviteMessage(
			chat.admins[0],
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[TestUtils.generateRandomUser()],
			chat.participants,
			[chat.participants[0]],
			date,
			false,
			false,
			ChatType.NORMAL,
		);
		expect(chat.messages.length).toBe(0);
		expect(chat.participants.length).toBe(2);
		chat.addMessage(message).then(() => {
			expect(chat.participants.length).toBe(3);
			expect(chat.messages.length).toBe(1);
			done();
		});
	});

	it('should not fail on add new already known user', (done) => {
		const chat = TestUtils.generateChat(0, 2);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new InviteMessage(
			chat.admins[0],
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[chat.participants[0], chat.participants[1]],
			chat.participants,
			[chat.participants[0]],
			date,
			false,
			false,
			ChatType.NORMAL,
		);
		expect(chat.messages.length).toBe(0);
		expect(chat.participants.length).toBe(2);
		chat.addMessage(message).then(() => {
			expect(chat.messages.length).toBe(1);
			expect(chat.participants.length).toBe(2);
			done();
		});
	});

	it('should consume INVITE message failed', (done) => {
		const chat = TestUtils.generateChat(0, 2);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new InviteMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[TestUtils.generateRandomUser()],
			chat.participants,
			[chat.participants[0]],
			date,
			false,
			false,
			ChatType.NORMAL,
		);
		chat
			.addMessage(message)
			.catch((err: UserHasNoRightsToPerformeEmailError) => {
				expect(err).toEqual(new UserHasNoRightsToPerformeEmailError(message));
				done();
			});
	});

	it('should consume TEXT_MESSAGE message successfully', (done) => {
		const chat = TestUtils.generateChat(0, 2);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new TextMessage(
			chat.participants[0],
			EMail.genId(),
			chat.id,
			date,
			'Some Text',
		);
		expect(chat.messages.length).toBe(0);
		chat.addMessage(message).then(() => {
			expect(chat.messages.length).toBe(1);
			done();
		});
	});

	it('should consume TEXT_MESSAGE message failed', (done) => {
		const chat = TestUtils.generateChat(0, 2, false, false);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new TextMessage(
			TestUtils.generateRandomUser(),
			EMail.genId(),
			chat.id,
			date,
			'Some Text',
		);
		expect(chat.messages.length).toBe(0);
		chat.addMessage(message).catch((err: any) => {
			expect(chat.messages.length).toBe(0);
			expect(err).toEqual(new UserHasNoRightsToPerformeEmailError(message));
			done();
		});
	});

	it('should consume DELETE_USER_FROM_CHAT message successfully', (done) => {
		const chat = TestUtils.generateChat(0, 2);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new DeleteMessage(
			chat.admins[0],
			EMail.genId(),
			chat.id,
			date,
			chat.participants[1].email,
		);
		expect(chat.messages.length).toBe(0);
		expect(chat.participants.length).toBe(2);
		chat.addMessage(message).then(() => {
			expect(chat.messages.length).toBe(1);
			expect(chat.participants.length).toBe(1);
			done();
		});
	});

	it('should consume DELETE_USER_FROM_CHAT message failed', (done) => {
		const chat = TestUtils.generateChat(0, 2, false, false);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new DeleteMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			date,
			chat.participants[0].email,
		);
		expect(chat.messages.length).toBe(0);
		expect(chat.participants.length).toBe(2);
		chat
			.addMessage(message)
			.catch((err: UserHasNoRightsToPerformeEmailError) => {
				expect(chat.messages.length).toBe(0);
				expect(chat.participants.length).toBe(2);
				expect(err).toEqual(new UserHasNoRightsToPerformeEmailError(message));
				done();
			});
	});

	it('should consume PROMOTE message promote to admins success', (done) => {
		const chat = TestUtils.generateChat(0, 2, false, false);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new PromoteMessage(
			chat.admins[0],
			EMail.genId(),
			chat.id,
			date,
			chat.participants[1].email,
			true,
		);
		expect(chat.messages.length).toBe(0);
		expect(chat.admins.length).toBe(1);
		chat.addMessage(message).then(() => {
			expect(chat.messages.length).toBe(1);
			expect(chat.admins.length).toBe(2);
			done();
		});
	});

	it('should consume PROMOTE message promote to user success', (done) => {
		const chat = TestUtils.generateChat(0, 2);
		chat.admins.push(chat.participants[1]);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new PromoteMessage(
			chat.participants[0],
			EMail.genId(),
			chat.id,
			date,
			chat.participants[1].email,
			false,
		);
		expect(chat.messages.length).toBe(0);
		expect(chat.admins.length).toBe(2);
		chat.addMessage(message).then(() => {
			expect(chat.messages.length).toBe(1);
			expect(chat.admins.length).toBe(1);
			done();
		});
	});

	it('should consume PROMOTE message failed', (done) => {
		const chat = TestUtils.generateChat(0, 2);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new PromoteMessage(
			chat.participants[1],
			EMail.genId(),
			chat.id,
			date,
			chat.participants[0].email,
			true,
		);
		expect(chat.messages.length).toBe(0);
		expect(chat.admins.length).toBe(1);
		chat
			.addMessage(message)
			.catch((err: UserHasNoRightsToPerformeEmailError) => {
				expect(chat.messages.length).toBe(0);
				expect(chat.admins.length).toBe(1);
				expect(err).toEqual(new UserHasNoRightsToPerformeEmailError(message));
				done();
			});
	});
});
