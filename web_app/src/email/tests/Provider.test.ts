import { Encryption, JsonProvider } from '../interfaces/provider';
import { IProvider } from '../dexie_interfaces/IProvider';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { JsonContext } from '../interfaces/json-rebuildable';
import { TestUtils } from './TestUtils';
import { Provider } from '../Provider';

function generateProvider() {
	return new Provider(
		`example-id-${TestUtils.generateNumber()}`,
		'example.net',
		'example-name',
		'smtp.example.de',
		587,
		Encryption.STARTTLS,
		'imap.example.de',
		993,
		Encryption.SSL_TLS,
	);
}

describe('Test Provider - toJson', () => {
	it('should return valid json JsonProvider', function () {
		const provider = generateProvider();
		const expected: JsonProvider = {
			id: provider.id,
			base_url: provider.baseUrl,
			name: provider.name,
			smtp_url: provider.smtpUrl,
			smtp_port: provider.smtpPort,
			smtp_encryption: provider.smtpEncryption,
			imap_url: provider.imapUrl,
			imap_port: provider.imapPort,
			imap_encryption: provider.imapEncryption,
		};
		expect(provider.toJson(false, JsonContext.PROXY)).toEqual(expected);
	});

	it('should return valid json JsonProvider string', function () {
		const provider = generateProvider();
		const expected: string = JSON.stringify({
			id: provider.id,
			base_url: provider.baseUrl,
			name: provider.name,
			smtp_url: provider.smtpUrl,
			smtp_port: provider.smtpPort,
			smtp_encryption: provider.smtpEncryption,
			imap_url: provider.imapUrl,
			imap_port: provider.imapPort,
			imap_encryption: provider.imapEncryption,
		});
		expect(provider.toJson(true, JsonContext.PROXY)).toEqual(expected);
	});

	it('should return valid json IProvider', function () {
		const provider = generateProvider();
		const expected: IProvider = {
			id: provider.id,
			baseUrl: provider.baseUrl,
			name: provider.name,
			smtpUrl: provider.smtpUrl,
			smtpPort: provider.smtpPort,
			smtpEncryption: provider.smtpEncryption,
			imapUrl: provider.imapUrl,
			imapPort: provider.imapPort,
			imapEncryption: provider.imapEncryption,
		};
		expect(provider.toJson(false, JsonContext.INTERNAL)).toEqual(expected);
	});

	it('should return valid json IProvider', function () {
		const provider = generateProvider();
		const expected: string = JSON.stringify({
			id: provider.id,
			baseUrl: provider.baseUrl,
			name: provider.name,
			smtpUrl: provider.smtpUrl,
			smtpPort: provider.smtpPort,
			smtpEncryption: provider.smtpEncryption,
			imapUrl: provider.imapUrl,
			imapPort: provider.imapPort,
			imapEncryption: provider.imapEncryption,
		});
		expect(provider.toJson(true, JsonContext.INTERNAL)).toEqual(expected);
	});
});

describe('Test Provider - fromJson', () => {
	it('should load IProvider fromJson successfully', function () {
		const provider = generateProvider();
		expect(Provider.fromJson(provider.toJson()).equals(provider)).toBeTrue();
	});

	it('should load JsonProvider fromJson successfully', function () {
		const provider = generateProvider();
		expect(
			Provider.fromJson(provider.toJson(false, JsonContext.PROXY)).equals(
				provider,
			),
		).toBeTrue();
	});
});

describe('Test Provider - save', function () {
	it('should save successfully', function (done) {
		const provider = generateProvider();
		provider.save().then((result) => {
			expect(result).toBe(provider.id);
			done();
		});
	});
});

describe('Test Provider - load', () => {
	it('should load successfully', function (done) {
		const provider = generateProvider();
		provider
			.save()
			.then((result) => {
				expect(result).toBe(provider.id);
				return Provider.load(provider.id);
			})
			.then((resultProvider: Provider) => {
				expect(resultProvider.equals(provider)).toBeTrue();
				done();
			});
	});

	it('should fail to load because there is no data', function (done) {
		const id = 'no-provider-id-found';
		Provider.load(id).catch((err) => {
			expect(err).toEqual(new NotFoundInDatabase(`Provider with id: ${id}`));
			done();
		});
	});
});

describe('Test Provider - delete', () => {
	it('should delete successfully and returns 1', function (done) {
		const provider = generateProvider();
		provider
			.save()
			.then((result) => {
				expect(result).toBe(provider.id);
				return provider.delete();
			})
			.then((deleteResult) => {
				expect(deleteResult).toBe(1);
				done();
			});
	});

	it('should delete returns 0 because there is no data', function (done) {
		const provider = generateProvider();
		provider.delete().then((deleteResult) => {
			expect(deleteResult).toBe(0);
			done();
		});
	});
});
