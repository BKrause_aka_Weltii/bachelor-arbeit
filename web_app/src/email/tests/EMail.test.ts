import { EMail, JsonEMail } from '../EMail';
import { EMailDefaults } from '../../Config';
import { TestUtils } from './TestUtils';
import { Utils } from '../utils';
import { JsonContext } from '../interfaces/json-rebuildable';

describe('Test EMail - fromJson', () => {
	it('should load email from json success', function () {
		const expectedEMail = TestUtils.generateEMail();
		const json = {
			from: 'herbert@von-schlecht.er',
			to: ['sophie@example.net', 'somebody@example.net'],
			subject: 'Something in here',
			date: expectedEMail.date,
			messageId: expectedEMail.messageId,
			chatVersion: EMailDefaults.chatVersion,
			mimeVersion: EMailDefaults.mimeVersion,
			contentType: EMailDefaults.contentType,
			chatContent: EMailDefaults.chatContent,
			additionalHeaderFields: {},
			body: 'A test message.',
			chatId: '0',
		};
		const jsonString = JSON.stringify(json);
		const email = EMail.fromJson(json);
		const email2 = EMail.fromJson(jsonString);
		expect(email).toEqual(expectedEMail);
		expect(email2).toEqual(expectedEMail);
	});

	it('should load email from json with reduced data success', function () {
		const expectedEMail = TestUtils.generateEMail();
		const json = {
			from: 'herbert@von-schlecht.er',
			to: ['sophie@example.net', 'somebody@example.net'],
			subject: 'Something in here',
			date: expectedEMail.date,
			messageId: expectedEMail.messageId,
			body: 'A test message.',
			chatId: '0',
		};
		const jsonString = JSON.stringify(json);
		const email = EMail.fromJson(json);
		const email2 = EMail.fromJson(jsonString);
		expect(email).toEqual(expectedEMail);
		expect(email2).toEqual(expectedEMail);
	});

	it('should load email from json with more special header fields', function () {
		const expectedEMail = TestUtils.generateEMail({
			anOtherHeaderField1: 'data',
			anOtherHeaderField2: 'data',
			anOtherHeaderField3: 'data',
		});
		const json = {
			from: 'herbert@von-schlecht.er',
			to: ['sophie@example.net', 'somebody@example.net'],
			subject: 'Something in here',
			date: expectedEMail.date,
			messageId: expectedEMail.messageId,
			body: 'A test message.',
			chatId: '0',
			anOtherHeaderField1: 'data',
			anOtherHeaderField2: 'data',
			anOtherHeaderField3: 'data',
		};
		const jsonString = JSON.stringify(json);
		const email = EMail.fromJson(json);
		const email2 = EMail.fromJson(jsonString);
		expect(email).toEqual(expectedEMail);
		expect(email2).toEqual(expectedEMail);
	});

	it('should load email from json snake_case fields', function () {
		const expectedEMail = TestUtils.generateEMail({
			anOtherHeaderField1: 'data',
			anOtherHeaderField2: 'data',
			anOtherHeaderField3: 'data',
		});
		const json = {
			from: 'herbert@von-schlecht.er',
			to: ['sophie@example.net', 'somebody@example.net'],
			subject: 'Something in here',
			date: expectedEMail.date,
			message_id: expectedEMail.messageId,
			body: 'A test message.',
			chat_id: '0',
			an_otherHeader_field1: 'data',
			an_otherHeader_field2: 'data',
			an_otherHeader_field3: 'data',
		};
		const jsonString = JSON.stringify(json);
		const email = EMail.fromJson(json);
		const email2 = EMail.fromJson(jsonString);
		expect(email).toEqual(expectedEMail);
		expect(email2).toEqual(expectedEMail);
	});
});

describe('Test EMail - toJson', function () {
	it('should return the EMail instance as correct IEmail json', function () {
		const email = TestUtils.generateEMail();
		const expectedJson = {
			from: email.from,
			to: email.to,
			subject: email.subject,
			date: email.date,
			messageId: email.messageId,
			chatVersion: email.chatVersion,
			mimeVersion: email.mimeVersion,
			contentType: email.contentType,
			chatContent: email.chatContent,
			additionalHeaderFields: email.additionalHeaderFields,
			body: email.body,
			chatId: email.chatId,
		};
		expect(email.toJson(false, JsonContext.INTERNAL)).toEqual(expectedJson);
	});

	it('should return the EMail instance as correct IEmail json string', function () {
		const email = TestUtils.generateEMail();
		const expectedJson = JSON.stringify({
			from: email.from,
			to: email.to,
			subject: email.subject,
			date: email.date,
			messageId: email.messageId,
			mimeVersion: email.mimeVersion,
			contentType: email.contentType,
			additionalHeaderFields: email.additionalHeaderFields,
			body: email.body,
			chatVersion: email.chatVersion,
			chatContent: email.chatContent,
			chatId: email.chatId,
		});
		expect(email.toJson(true, JsonContext.INTERNAL)).toBe(expectedJson);
	});

	it('should return the EMail instance as correct JsonEmail json', function () {
		const email = TestUtils.generateEMail();
		const expectedJson: JsonEMail = {
			header: {
				from: email.from,
				to: email.to,
				subject: email.subject,
				date: email.date,
				message_id: email.messageId,
				chat_version: email.chatVersion,
				mime_version: email.mimeVersion,
				content_type: email.contentType,
				chat_content: email.chatContent,
				additional_header_fields: email.additionalHeaderFields,
				chat_id: email.chatId,
			},
			body: email.body,
		};
		expect(email.toJson(false, JsonContext.PROXY)).toEqual(expectedJson);
	});

	it('should return the EMail instance as correct JsonEmail json', function () {
		const email = TestUtils.generateEMail({
			someData: 'someData',
		});
		const expectedJson: JsonEMail = {
			header: {
				from: email.from,
				to: email.to,
				subject: email.subject,
				date: email.date,
				message_id: email.messageId,
				chat_version: email.chatVersion,
				mime_version: email.mimeVersion,
				content_type: email.contentType,
				chat_content: email.chatContent,
				additional_header_fields: Utils.replaceCamelCaseToSnakeCaseRecursively(
					email.additionalHeaderFields,
				),
				chat_id: email.chatId,
			},
			body: email.body,
		};
		expect(email.toJson(false, JsonContext.PROXY)).toEqual(expectedJson);
	});
});

/*describe('Test EMail - save', () => {
	it('should save successfully', function (done) {
		const email = generateEMail({
			key: 'value',
			key2: 'value2',
		});
		email.save().then((msg) => {
			expect(msg.emailTo.length).toBe(2);
			expect(msg.additionalHeaderFields.length).toBe(2);
			expect(msg.email).toEqual(email.messageId);
			done();
		});
	});

	it('should save success without full data', function (done) {
		const email = generateEMail();
		email.save().then((msg) => {
			// FIXME After fixing the clear, this can be more detailed.
			expect(msg.emailTo.length).toBe(2);
			expect(msg.additionalHeaderFields.length).toBe(0);
			expect(msg.email).toEqual(email.messageId);
			done();
		});
	});
});*/

/*describe('Test EMail - load', () => {
	it('should load saved object successfully!', function (done) {
		const email = generateEMail();
		email
			.save()
			.then(() => {
				return EMail.load(email.messageId);
			})
			.then((result) => {
				expect(result).toEqual(email);
				done();
			});
	});

	it('should fail because no user can be found!', function (done) {
		EMail.load('a-message-id').catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`Email with messageId: a-message-id`),
			);
			done();
		});
	});
});*/

/*describe('Test EMail - delete', function () {
	it('should delete email from database', function (done) {
		const email = generateEMail({
			key: 'value',
		});
		email
			.save()
			.then((msg) => {
				return email.delete();
			})
			.then((result) => {
				expect(result).toEqual({
					email: 1,
					emailTo: 2,
					additionalHeaderFields: 1,
				});
				done();
			});
	});
}); */
