import { TestUtils } from './TestUtils';
import { EMailProxy } from '../EMailProxy';
import { Provider } from '../Provider';
import { Encryption } from '../interfaces/provider';
import { JsonContext } from '../interfaces/json-rebuildable';
import { EMail } from '../EMail';
import { LoginUser } from '../LoginUser';
import { UserSettings, SettingKeys } from '../UserSettings';

function generateProvider() {
	return new Provider(
		`example-id-${TestUtils.generateNumber()}`,
		'example.net',
		'example-name',
		'smtp.example.de',
		587,
		Encryption.STARTTLS,
		'imap.example.de',
		993,
		Encryption.SSL_TLS,
	);
}

describe('Test EMailProxy - ping', () => {
	it('should return dong dongelong', (done) => {
		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			return new window.Response('dong dongelong', {
				status: 200,
				headers: { 'Content-type': 'application/json' },
			});
		}

		const proxy = new EMailProxy();
		proxy.ping().then((json: any) => {
			expect(json).toBe(true);
			done();
		});
	});
});

describe('Test EMailProxy - cacheProvider', () => {
	it('should cache successfully', (done) => {
		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			const body = {
				message: 'Your provider is successfully added to the system',
			};
			return new window.Response(JSON.stringify(body), {
				status: 200,
				headers: { 'Content-type': 'application/json' },
			});
		}

		const proxy = new EMailProxy();
		proxy.cacheProvider(generateProvider()).then((json) => {
			expect(
				(json.message = 'Your provider is successfully added to the system'),
			);
			done();
		});
	});

	it('should fail with because wrong data', (done) => {
		const message =
			"Something goes wrong with the data of your Provider. Please check the urls, ports, and encryption's and try it again!";
		function fetchMock() {
			const body = {
				message: message,
				error: [
					false,
					{
						imap: false,
						smtp: false,
					},
				],
			};
			return new window.Response(JSON.stringify(body), {
				status: 400,
				headers: { 'Content-type': 'application/json' },
			});
		}
		TestUtils.mockFetch(fetchMock);

		const proxy = new EMailProxy();
		proxy.cacheProvider(generateProvider()).catch((err: any) => {
			expect(err.status).toBe(400);
			expect(err.json.message).toBe(message);
			expect(err.json.error).toEqual([
				false,
				{
					imap: false,
					smtp: false,
				},
			]);
			done();
		});
	});

	it('should fail with because the id is already known', (done) => {
		const message =
			'I already have a proxy with the id {provider.id} in my system and the data are different. Please check your data or change the wanted id.';
		function fetchMock() {
			const body = {
				message: message,
			};
			return new window.Response(JSON.stringify(body), {
				status: 403,
				headers: { 'Content-type': 'application/json' },
			});
		}
		TestUtils.mockFetch(fetchMock);

		const proxy = new EMailProxy();
		proxy.cacheProvider(generateProvider()).catch((err: any) => {
			expect(err.status).toBe(403);
			expect(err.json.message).toEqual(message);
			done();
		});
	});
});

describe('Test EMailProxy - fetchProviders', () => {
	it('should fetch provider successfully', (done) => {
		const expectedProviders: any = [
			generateProvider().toJson(false, JsonContext.PROXY),
			generateProvider().toJson(false, JsonContext.PROXY),
			generateProvider().toJson(false, JsonContext.PROXY),
		];
		function fetchMock() {
			return new window.Response(JSON.stringify(expectedProviders), {
				status: 200,
				headers: { 'Content-type': 'application/json' },
			});
		}
		TestUtils.mockFetch(fetchMock);

		const proxy = new EMailProxy();
		proxy.fetchProviders().then((providers: Provider[]) => {
			expect(providers.length).toBe(3);
			done();
		});
	});
});

describe('Test EMailProxy - pullMessages', () => {
	it('should return emails successfully', (done) => {
		const user: LoginUser = TestUtils.generateRandomLoginUser();
		const emails: EMail[] = [
			TestUtils.generateEMail(),
			TestUtils.generateEMail(),
		];
		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			return new window.Response(
				JSON.stringify({
					mails: emails.map((email) => {
						return email.toJson(false, JsonContext.PROXY);
					}),
				}),
				{
					status: 200,
					headers: { 'Content-type': 'application/json' },
				},
			);
		}
		const provider = generateProvider();
		const proxy = new EMailProxy(provider, user);

		proxy
			.pullMessages()
			.then((json: EMail[]) => {
				expect(json).toEqual(emails);
				return UserSettings.load();
			})
			.then((settings: UserSettings) => {
				expect(settings.getSetting(SettingKeys.LAST_PULL).value).toBe(
					emails[1].date,
				);
				done();
			});
	});

	it('PullMessages - no emails can be found', (done) => {
		const user = TestUtils.generateRandomLoginUser();
		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			return new window.Response(
				JSON.stringify({
					mails: [],
				}),
				{
					status: 404,
					headers: { 'Content-type': 'application/json' },
				},
			);
		}

		const provider = generateProvider();
		const proxy = new EMailProxy(provider, user);
		proxy.pullMessages().catch((json) => {
			expect(json.status).toEqual(404);
			expect(json.json.mails).toEqual([]);
			done();
		});
	});
});

describe('Test EMailProxy - sendEmail', () => {
	it('should send email successfully', (done) => {
		const user = TestUtils.generateRandomLoginUser();
		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			return new window.Response(
				JSON.stringify({
					message: 'E-Mail was send successfully!',
				}),
				{
					status: 200,
					headers: { 'Content-type': 'application/json' },
				},
			);
		}
		const provider = generateProvider();
		const proxy = new EMailProxy(provider, user);
		const mail = TestUtils.generateEMail();
		proxy.sendMail(mail).then((json) => {
			expect(json).toBeTrue;
			done();
		});
	});

	it('should faile because data is missing', (done) => {
		const user = TestUtils.generateRandomLoginUser();
		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			return new window.Response(
				JSON.stringify({
					message: 'Something goes wrong.',
					err: 'To is missing!',
					err_type: 'Exception',
				}),
				{
					status: 400,
					headers: { 'Content-type': 'application/json' },
				},
			);
		}

		const provider = generateProvider();
		const proxy = new EMailProxy(provider, user);
		const mail = TestUtils.generateEMail();
		proxy.sendMail(mail).catch((json) => {
			expect(json.status).toEqual(400);
			expect(json.json.message).toEqual('Something goes wrong.');
			expect(json.json.err).toEqual('To is missing!');
			expect(json.json.err_type).toEqual('Exception');
			done();
		});
	});
});

describe('Test EMailProxy - getMailboxes', () => {
	it('GetMailboxes - Success', (done) => {
		const user = TestUtils.generateRandomLoginUser();

		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			return new window.Response(
				JSON.stringify({ mailboxes: ['INBOX', 'SPAM', 'TRASH'] }),
				{
					status: 200,
					headers: { 'Content-type': 'application/json' },
				},
			);
		}

		const provider = generateProvider();
		const proxy = new EMailProxy(provider, user);
		proxy.getMailboxes().then((mailboxes) => {
			expect(mailboxes).toEqual(['INBOX', 'SPAM', 'TRASH']);
			done();
		});
	});
});

describe('Test EMailProxy - loginUser', () => {
	it('should successfully login user', (done) => {
		const user = TestUtils.generateRandomLoginUser();

		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			return new window.Response(
				JSON.stringify({
					message: 'The user data are valid',
				}),
				{
					status: 200,
					headers: { 'Content-type': 'application/json' },
				},
			);
		}

		const provider = generateProvider();
		const proxy = new EMailProxy(provider);
		expect(proxy.loggedInUser).toBe(null);
		proxy.loginUser(user).then((json) => {
			expect(json.message).toBe('The user data are valid');
			expect(proxy.loggedInUser).toEqual(user);
			done();
		});
	});

	it('should fail with correct login data, but wrong provider', (done) => {
		const user = TestUtils.generateRandomLoginUser();

		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			return new window.Response(
				JSON.stringify({
					message:
						'The user data seems wrong! Please check the login data and the chosen provider and try it again!',
					status: {
						imap: false,
						smtp: false,
					},
				}),
				{
					status: 400,
					headers: { 'Content-type': 'application/json' },
				},
			);
		}

		const provider = generateProvider();
		const proxy = new EMailProxy(provider, user);
		proxy.loginUser(user).catch((json) => {
			expect(json.status).toBe(400);
			expect(json.json.message).toBe(
				'The user data seems wrong! Please check the login data and the chosen provider and try it again!',
			);
			expect(json.json.status).toEqual({
				imap: false,
				smtp: false,
			});
			done();
		});
	});

	xit('should fail because inocrrect login data', (done) => {
		// same as above (LoginUser - success with correct login data, but wrong provider)
	});
});

describe('Test EMailProxy - logoutUser', () => {
	it('should logout user successfully', (done) => {
		const user = TestUtils.generateRandomLoginUser();

		TestUtils.mockFetch(fetchMock);
		function fetchMock() {
			return new window.Response(
				JSON.stringify({
					message: 'The user data are valid',
				}),
				{
					status: 200,
					headers: { 'Content-type': 'application/json' },
				},
			);
		}

		const provider = generateProvider();
		const proxy = new EMailProxy(provider, user);
		expect(proxy.loggedInUser).toBe(user);
		proxy.logoutUser().then((status) => {
			expect(status).toBeTrue();
			done();
		});
	});
});
