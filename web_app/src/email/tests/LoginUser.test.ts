import { JsonLoginUser, LoginUser } from '../LoginUser';
import { TestUtils } from './TestUtils';
import generateRandomLoginUser = TestUtils.generateRandomLoginUser;
import { Chat } from '../Chat';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { JsonContext } from '../interfaces/json-rebuildable';
import { DB } from '../db';

describe('Test LoginUser - toJson', () => {
	it('should return correct json for proxy context', function () {
		const loginUser = generateRandomLoginUser();
		const expected: any = {
			chosen_provider_id: loginUser.chosenProviderId,
			login_name: loginUser.loginName,
			email: loginUser.email,
			nickname: loginUser.nickname,
			password: loginUser.password,
		};
		const loginJson = loginUser.toJson(
			false,
			JsonContext.PROXY,
		) as JsonLoginUser;
		expect(expected).toEqual(loginJson);
	});

	it('should return correct json for internal context', function () {
		const loginUser = generateRandomLoginUser();
		const expected = {
			email: loginUser.email,
			nickname: loginUser.nickname,
			chats: loginUser.chats.map((chat: Chat) => {
				return chat.toJson();
			}),
			loginName: loginUser.loginName,
			password: loginUser.password,
			chosenProviderId: loginUser.chosenProviderId,
		};
		expect(expected).toEqual(
			loginUser.toJson(false, JsonContext.INTERNAL) as any,
		);
	});

	it('should return correct json for database context', function () {
		const loginUser = generateRandomLoginUser();
		const expected = {
			email: loginUser.email,
			nickname: loginUser.nickname,
			chats: loginUser.chats.map((chat: Chat) => {
				return chat.id;
			}),
			loginName: loginUser.loginName,
			password: loginUser.password,
			chosenProviderId: loginUser.chosenProviderId,
		};
		expect(expected).toEqual(
			loginUser.toJson(false, JsonContext.DATABASE) as any,
		);
	});
});

describe('Test LoginUser - fromJson', () => {
	it('should load successfully', function () {
		const loginUser = generateRandomLoginUser();
		const json: JsonLoginUser = {
			email: loginUser.email,
			nickname: loginUser.nickname,
			chats: loginUser.chats.map((chat: Chat) => {
				return chat.toJson();
			}),
			login_name: loginUser.loginName,
			password: loginUser.password,
			chosen_provider_id: loginUser.chosenProviderId,
		};
		expect(LoginUser.fromJson(json).equals(loginUser)).toBeTrue();
	});

	it('should load with missing data', function () {
		const json: any = {
			login_name: 'a_login_name',
			password: '',
			chosen_provider_id: 'a_provider',
		};
		const expectedLoginUser = new LoginUser(
			'',
			'',
			[],
			json.login_name,
			json.password,
			json.chosen_provider_id,
		);
		expect(LoginUser.fromJson(json).equals(expectedLoginUser)).toBeTrue();
	});
});

describe('Test LoginUser - save', () => {
	it('should successfully save to lastUser', function (done) {
		const loginUser = generateRandomLoginUser();
		loginUser.save().then((result) => {
			expect(result).toBe(loginUser.email);
			done();
		});
	});
});

describe('Test LoginUser - load', () => {
	beforeEach(async () => {
		await DB.getInstance().clearAll();
	});
	it('should load successfully', function (done) {
		const loginUser = generateRandomLoginUser();
		Promise.all(loginUser.chats.map((chat: Chat) => chat.save()))
			.then((result) => {
				return loginUser.save();
			})
			.then((result) => {
				expect(result).toBe(loginUser.email);
				return LoginUser.load(loginUser.email);
			})
			.then((result: LoginUser) => {
				expect(loginUser.equals(result)).toBeTrue();
				done();
			});
	});

	it('load should fail', function (done) {
		const loginUser = generateRandomLoginUser();
		LoginUser.load(loginUser.email)
			.then((result: LoginUser) => {
				expect(loginUser.equals(result)).toBeTrue();
				done();
			})
			.catch((err) => {
				expect(err).toEqual(
					new NotFoundInDatabase(`LastUser with email: ${loginUser.email}`),
				);
				done();
			});
	});
});

describe('Test LoginUser - delete', () => {
	it('should delete successfully', function (done) {
		const loginUser = generateRandomLoginUser();
		Promise.all(loginUser.chats.map((chat: Chat) => chat.save()))
			.then((result) => {
				return loginUser.save();
			})
			.then((result) => {
				expect(result).toBe(loginUser.email);
				return loginUser.delete();
			})
			.then((result) => {
				expect(result).toBe(1);
				done();
			});
	});

	it('should delete nothing, because no user can be found', function (done) {
		const loginUser = generateRandomLoginUser();
		return loginUser.delete().then((result) => {
			expect(result).toBe(0);
			done();
		});
	});
});
