import { IEMail } from './dexie_interfaces/IEMail';
import { ILoginUser } from './dexie_interfaces/ILoginUser';

export interface IProxy {
	loggedInUser: ILoginUser | null;

	pullMessages(): Promise<Array<IEMail>>;
	sendMail(mail: IEMail): Promise<boolean>;
	getMailboxes(): Promise<Array<String>>;
	loginUser(user: ILoginUser): Promise<boolean>;
	logoutUser(): Promise<boolean>;
	ping(): Promise<boolean>;
}
