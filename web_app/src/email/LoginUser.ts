import { User } from './User';
import { DB } from './db';
import { ILoginUser } from './dexie_interfaces/ILoginUser';
import { Chat } from './Chat';
import { ChatType, IChat } from './dexie_interfaces/IChat';
import { Utils } from './utils';
import { IChatParticipants } from './dexie_interfaces/IChatParticipants';
import { JsonContext, JsonRebuildable } from './interfaces/json-rebuildable';
import { NotFoundInDatabase } from './NotFoundInDatabase';
import getAndDel = Utils.getAndDel;
import mergeObjects = Utils.mergeObjects;
import { ChatLoader } from './ChatLoader';
import { AChat } from './AChat';
import { MessageRebuilder } from './content_messages/MessageRebuilder';
import { ChatContentTypes } from './ChatContentTypes';

export type JsonLoginUser = {
	email: string;
	nickname: string;
	chats: Array<any>;
	password: string;
	chosen_provider_id: string;
	login_name: string;
};

export class LoginUser extends User implements ILoginUser {
	chats: Array<Chat>;
	loginName: string;
	password: string;
	chosenProviderId: string;

	constructor(
		email: string,
		nickname: string,
		chats: Array<Chat>,
		loginName: string,
		password: string,
		chosenProviderId: string,
	) {
		super(email, nickname);
		this.chats = chats || [];
		this.loginName = loginName;
		this.password = password;
		this.chosenProviderId = chosenProviderId;
	}

	public static fromJson(json: string | any | JsonLoginUser | ILoginUser) {
		if (typeof json === 'string') {
			json = JSON.parse(json) as JsonLoginUser;
		}
		return new LoginUser(
			json.email || '',
			json.nickname || '',
			(json.chats || []).map((chat: IChat) => {
				return ChatLoader.fromJson(chat);
			}),
			json.login_name || json.loginName || '',
			json.password || '',
			json.chosen_provider_id || json.chosenProviderId || '',
		);
	}

	public toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | JsonLoginUser | ILoginUser {
		let json;
		switch (context) {
			case JsonContext.INTERNAL:
				json = {
					chosenProviderId: this.chosenProviderId,
					loginName: this.loginName,
					email: this.email,
					nickname: this.nickname,
					password: this.password,
					chats: this.chats.map((chat) => chat.toJson(false, context)),
				} as ILoginUser;
				break;
			case JsonContext.PROXY:
				json = {
					chosen_provider_id: this.chosenProviderId,
					login_name: this.loginName,
					email: this.email,
					nickname: this.nickname,
					password: this.password,
				} as JsonLoginUser;
				break;
			case JsonContext.DATABASE:
				json = {
					chats: this.chats.map((chat: Chat) => {
						return chat.id;
					}),
					chosenProviderId: this.chosenProviderId,
					loginName: this.loginName,
					email: this.email,
					nickname: this.nickname,
					password: this.password,
				} as any;
				break;
		}
		return asString ? JSON.stringify(json) : json;
	}

	public toUser() {
		return new User(this.email, this.nickname);
	}

	save(): Promise<any> {
		const data = this.toJson();
		getAndDel('chats', data);
		return Promise.resolve().then(() => {
			return DB.getInstance().lastUser.put(data as any);
		});
	}

	static load(email: string = ''): Promise<LoginUser> {
		let chats: Array<AChat> = [];
		return Promise.resolve()
			.then(() => {
				return DB.getInstance().chats.toArray();
			})
			.then((chatsResult) => {
				return Promise.all(
					(chatsResult || []).map((chat: any) => {
						return ChatLoader.load(chat.id).then((chat) => chat);
					}),
				);
			})
			.then((result) => {
				chats = result || [];
				return DB.getInstance().lastUser.where('email').equals(email).first();
			})
			.then((result) => {
				if (!result)
					throw new NotFoundInDatabase(`LastUser with email: ${email}`);
				const finalJson = mergeObjects(result, { chats: chats });
				return LoginUser.fromJson(finalJson);
			})
			.catch((err) => {
				console.error('LoginUser - email', err);
				throw err;
			});
	}

	equals(other: LoginUser): boolean {
		console.log(this);
		console.log(other);
		let chatsAreEquals = true;
		this.chats.forEach((chat1: Chat) => {
			let status = false;
			other.chats.map((chat2: Chat) => {
				if (chat1.id === chat2.id && chat1.equals(chat2)) {
					status = true;
				}
			});
			if (!status) {
				chatsAreEquals = status;
			}
		});
		return (
			super.equals(other as User) &&
			this.loginName === other.loginName &&
			this.password === other.password &&
			this.chosenProviderId === other.chosenProviderId &&
			this.chats.length === other.chats.length &&
			chatsAreEquals
		);
	}

	delete(): Promise<any> {
		return DB.getInstance().lastUser.where('email').equals(this.email).delete();
	}
}
