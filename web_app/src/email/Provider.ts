import { DB } from './db';
import { Encryption, JsonProvider } from './interfaces/provider';
import { LoginUser } from './LoginUser';
import { EMail, JsonEMail } from './EMail';
import { Utils } from './utils';
import { IProvider } from './dexie_interfaces/IProvider';
import { NotFoundInDatabase } from './NotFoundInDatabase';
import { JsonContext, JsonRebuildable } from './interfaces/json-rebuildable';
import { Config, EMailDefaults } from '../Config';
import { UserSettings, SettingKeys } from './UserSettings';
import { DateTime } from 'luxon';
import { UseMailbox } from './UseMailbox';
import { DbClass } from './interfaces/db-class';

export class Provider implements IProvider, DbClass, JsonRebuildable {
	readonly id: string;
	readonly name: string;
	readonly baseUrl: string;
	readonly imapEncryption: Encryption;
	readonly imapPort: number;
	readonly imapUrl: string;
	readonly smtpEncryption: Encryption;
	readonly smtpPort: number;
	readonly smtpUrl: string;

	constructor(
		id: string,
		baseUrl: string,
		name: string,
		smtpUrl: string,
		smtpPort: number,
		smtpEncryption: Encryption,
		imapUrl: string,
		imapPort: number,
		imapEncryption: Encryption,
	) {
		this.id = id;
		this.name = name;
		this.baseUrl = baseUrl;
		this.smtpEncryption = smtpEncryption || Encryption.STARTTLS;
		this.smtpUrl = smtpUrl;
		this.smtpPort = smtpPort || 587;
		this.imapEncryption = imapEncryption || Encryption.SSL_TLS;
		this.imapUrl = imapUrl;
		this.imapPort = imapPort || 993;
	}
	/*
	async cacheProviderInProxy() {
		const url = `${this.proxyUrl}providers`;
		return fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: this.toJson(true, JsonContext.PROXY) as string,
		});
	}

	async ping() {
		const url = `${this.proxyUrl}ding`;
		const response = fetch(url, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		});
		return response;
	}

	async pullMessages(): Promise<EMail[]> {
		if (!this.user) {
			console.error('No user is logged in!');
			return Promise.reject('No user is logged in!');
		}
		const url = `${this.proxyUrl}provider/user/get_mails`;
		let searchSinceDate: DateTime;
		return UserSettings.load()
			.then((userSettings: UserSettings) => {
				searchSinceDate = Utils.timeStringToDate(
					userSettings.getSetting(SettingKeys.LAST_PULL).value,
				);
				return UseMailbox.getOnlyUsed();
			})
			.then((mailboxes) => {
				return fetch(url, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(
						Utils.mergeObjects(this.getProviderDataForBody(), {
							user: this.user?.toJson(false, JsonContext.PROXY),
							search_string: `HEADER user-agent ${
								EMailDefaults.userAgent
							} SINCE ${searchSinceDate.toFormat('dd-LLL-kkkk')}`,
							mailboxes: mailboxes.map((mailboxe) => mailboxe.mailbox),
						}),
					),
				});
			})
			.then((response) => {
				return response.json().then((json: any) => {
					if (response.status != 200) {
						return Promise.reject({
							status: response.status,
							statusText: response.statusText,
							json: json,
						});
					}
					// FIXME Move the advanced date time check to the email-proxy! it will be faster
					const emails: EMail[] =
						json.mails
							.filter(
								(rawMail: JsonEMail) =>
									Utils.timeStringToDate(rawMail.header.date) > searchSinceDate,
							)
							.map((email: any) => {
								return EMail.fromJson(email);
							}) || [];
					return UserSettings.load().then((settings: UserSettings) => {
						settings.setSetting(
							SettingKeys.LAST_PULL,
							Utils.getLastDateFromEmails(emails) ||
								Utils.dateToTimeString(searchSinceDate),
						);
						return Promise.resolve(emails);
					});
				});
			});
	}

	async sendMail(mail: EMail) {
		if (!this.user) {
			console.error('No user is logged in!');
			return Promise.reject('No user is logged in!');
		}
		const url = `${this.proxyUrl}provider/user/send_mail`;
		// overwrite fields
		mail.additionalHeaderFields['userAgent'] = EMailDefaults.userAgent;

		const response = await fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(
				Utils.mergeObjects(this.getProviderDataForBody(), {
					user: this.user.toJson(false, JsonContext.PROXY),
					email: mail.toJson(false, JsonContext.PROXY),
				}),
			),
		});

		if (response.status != 200) {
			throw {
				status: response.status,
				statusText: response.statusText,
				json: await response.json(),
			};
		}
		return await response.json();
	}

	async loginUser(user: LoginUser) {
		const url = `${this.proxyUrl}provider/check_user`;
		const response = await fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(
				Utils.mergeObjects(this.getProviderDataForBody(), {
					user: user.toJson(false, JsonContext.PROXY),
				}),
			),
		});
		if (response.status != 200) {
			throw {
				status: response.status,
				statusText: response.statusText,
				json: await response.json(),
			};
		}
		this.user = user;
		return response.json();
	}

	logoutUser() {
		return new Promise<boolean>((resolve, reject) => {
			this.user = null;
			resolve(true);
		});
	}

	async getMailboxes() {
		if (!this.user) {
			console.error('No user is logged in!');
			return Promise.reject('No user is logged in!');
		}
		const url = `${this.proxyUrl}provider/user/get_mailboxes`;
		const response = await fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(
				Utils.mergeObjects(this.getProviderDataForBody(), {
					user: this.user.toJson(false, JsonContext.PROXY),
				}),
			),
		});
		if (response.status != 200) {
			throw {
				status: response.status,
				statusText: response.statusText,
				json: await response.json(),
			};
		}
		return await response.json().then((json) => {
			return json.mailboxes;
		});
	}
*/
	save(): Promise<any> {
		return DB.getInstance().providers.put(this);
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | JsonProvider | IProvider {
		let json;
		switch (context) {
			case JsonContext.INTERNAL:
			case JsonContext.DATABASE:
				json = {
					id: this.id,
					baseUrl: this.baseUrl,
					name: this.name,
					smtpUrl: this.smtpUrl,
					smtpPort: this.smtpPort,
					smtpEncryption: this.smtpEncryption,
					imapUrl: this.imapUrl,
					imapPort: this.imapPort,
					imapEncryption: this.imapEncryption,
				} as IProvider;
				break;
			case JsonContext.PROXY:
				json = {
					id: this.id,
					base_url: this.baseUrl,
					name: this.name,
					smtp_url: this.smtpUrl,
					smtp_port: this.smtpPort,
					smtp_encryption: this.smtpEncryption,
					imap_url: this.imapUrl,
					imap_port: this.imapPort,
					imap_encryption: this.imapEncryption,
				} as JsonProvider;
				break;
		}
		return asString ? JSON.stringify(json) : json;
	}

	static load(pk: string): Promise<Provider> {
		return DB.getInstance()
			.providers.where('id')
			.equals(pk)
			.first()
			.then((providerResult: IProvider | undefined) => {
				if (providerResult)
					return Provider.fromJson(providerResult as IProvider);
				throw new NotFoundInDatabase(`Provider with id: ${pk}`);
			});
	}

	static fromJson(json: string | JsonProvider | IProvider) {
		if (typeof json === 'string') {
			json = JSON.parse(json as string) as JsonProvider;
		}
		return new Provider(
			json.id,
			(json as JsonProvider).base_url || (json as IProvider).baseUrl,
			json.name,
			(json as JsonProvider).smtp_url || (json as IProvider).smtpUrl,
			(json as JsonProvider).smtp_port || (json as IProvider).smtpPort,
			(json as JsonProvider).smtp_encryption ||
				(json as IProvider).smtpEncryption,
			(json as JsonProvider).imap_url || (json as IProvider).imapUrl,
			(json as JsonProvider).imap_port || (json as IProvider).imapPort,
			(json as JsonProvider).imap_encryption ||
				(json as IProvider).imapEncryption,
		);
	}

	static genId() {
		throw Error('genId() is not necessary for a provider.');
	}

	delete(): Promise<any> {
		return DB.getInstance().providers.where('id').equals(this.id).delete();
	}

	equals(other: Provider): boolean {
		return (
			this.id === other.id &&
			this.name === other.name &&
			this.baseUrl === other.baseUrl &&
			this.smtpEncryption === other.smtpEncryption &&
			this.smtpUrl === other.smtpUrl &&
			this.imapUrl === other.imapUrl &&
			this.imapEncryption === other.imapEncryption &&
			this.imapPort === other.imapPort
		);
	}
}
