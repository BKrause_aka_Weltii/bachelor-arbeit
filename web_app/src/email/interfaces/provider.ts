import { LoginUser } from '../LoginUser';
import { EMail } from '../EMail';
import { DbClass } from './db-class';
import { JsonRebuildable } from './json-rebuildable';
import { IProvider } from '../dexie_interfaces/IProvider';

export enum Encryption {
	SSL_TLS = 'SSL_TLS',
	STARTTLS = 'STARTTLS',
}

export type Connection = {
	url: string;
	port?: number;
	encryption?: Encryption;
};

export type JsonProvider = {
	id: string; // pk
	name: string;
	base_url: string;
	smtp_url: string;
	smtp_port: number;
	smtp_encryption: Encryption;
	imap_url: string;
	imap_port: number;
	imap_encryption: Encryption;
};
