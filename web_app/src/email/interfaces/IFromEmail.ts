import { EMail } from '../EMail';
import { IEMail } from '../dexie_interfaces/IEMail';
import { Chat } from '../Chat';

export abstract class IFromEmail {
	static fromEMail(email: EMail): IFromEmail {
		throw Error('You must implement fromEMail!');
	}
}

export interface IToEmail {
	toEmail(chat: Chat): IEMail;
}
