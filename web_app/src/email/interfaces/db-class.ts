export abstract class DbClass {
	public static load(pk: string): Promise<DbClass> {
		throw Error('You must implement me!');
	}

	public static genId(): string {
		throw Error('You must implement me!');
	}

	public abstract save(): Promise<any>;

	public abstract delete(): Promise<any>;

	public abstract equals(other: DbClass): boolean;
}
