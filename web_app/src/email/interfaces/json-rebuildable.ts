export enum JsonContext {
	INTERNAL = 'INTERNAL',
	PROXY = 'PROXY',
	DATABASE = 'DATABASE',
}

export abstract class JsonRebuildable {
	public static fromJson(json: string | object) {
		throw Error('You must implement me!');
	}

	public abstract toJson(asString: boolean, context: JsonContext): void;
}
