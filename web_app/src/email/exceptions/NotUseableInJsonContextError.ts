import { JsonContext } from '../interfaces/json-rebuildable';

export class NotUseableInJsonContextError extends Error {
	constructor(readonly classFunction: string, readonly context: JsonContext) {
		super(`${classFunction} cannot be used with ${context} context`);
	}
}
