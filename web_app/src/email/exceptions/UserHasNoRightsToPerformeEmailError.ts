import { AMessage } from '../dexie_interfaces/AMessage';

export class UserHasNoRightsToPerformeEmailError extends Error {
	constructor(message: AMessage) {
		super(
			`Email with messageId: ${message.messageId}` +
				'cannot be processed, because the sender of the email has no rights to performe the action! ' +
				'The message will be ignored!',
		);
	}
}
