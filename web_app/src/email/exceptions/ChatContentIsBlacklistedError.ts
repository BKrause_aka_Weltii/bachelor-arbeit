import { AMessage } from '../dexie_interfaces/AMessage';

export class ChatContentIsBlacklistedError extends Error {
	constructor(message: AMessage) {
		super();
	}
}
