export class MustBeImplementedError extends Error {
	constructor(functionName: string) {
		super(`You must implement ${functionName} by your own!`);
	}
}
