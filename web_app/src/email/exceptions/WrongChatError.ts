import { AMessage } from '../dexie_interfaces/AMessage';

export class WrongChatError extends Error {
	constructor(message: AMessage, expectedChatId: string) {
		super(
			`The chatId ${message.chatId} of message ${message.messageId} is not matching with the chat id ${expectedChatId}!`,
		);
	}
}
