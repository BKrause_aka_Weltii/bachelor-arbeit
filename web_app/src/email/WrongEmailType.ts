import { EMail } from './EMail';
import { ChatContentTypes } from './ChatContentTypes';

export class WrongEmailType extends Error {
	constructor(email: EMail, expectedType: ChatContentTypes) {
		super(
			`The email with the messageId: ${email.messageId} is a ${email.chatContent} not an ${expectedType} email!`,
		);
	}
}
