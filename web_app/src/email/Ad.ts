import { JsonRebuildable, JsonContext } from './interfaces/json-rebuildable';
import { DbClass } from './interfaces/db-class';
import { IAd, PriceType } from './dexie_interfaces/IAd';
import { User } from './User';
import { NotUseableInJsonContextError } from './exceptions/NotUseableInJsonContextError';
import { DB } from './db';
import { Utils } from './utils';
import { NotFoundInDatabase } from './NotFoundInDatabase';
import { NewAdMessage } from './content_messages/NewAdMessage';

export class Ad implements JsonRebuildable, DbClass, IAd {
	constructor(
		readonly id: string,
		readonly creator: User,
		readonly chatId: string,
		readonly _title: string,
		readonly _description: string,
		readonly _priceType: PriceType,
		readonly _priceAmount: string,
	) {}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): any | string {
		if (context == JsonContext.PROXY) {
			throw new NotUseableInJsonContextError('Ad.toJson', JsonContext.PROXY);
		}
		const json = {
			id: this.id,
			creator:
				context == JsonContext.DATABASE
					? this.creator.email
					: this.creator.toJson(false, context),
			chatId: this.chatId,
			title: this._title,
			description: this._description,
			priceType: this._priceType,
			priceAmount: this._priceAmount,
		};
		return asString ? JSON.stringify(json) : json;
	}

	save(): Promise<any> {
		const ad: IAd = this.toJson(false, JsonContext.DATABASE) as IAd;
		return DB.getInstance()
			.ad.put(ad)
			.catch((err) => {
				console.error('Ad.save', this, err);
				throw err;
			});
	}

	delete(): Promise<any> {
		return DB.getInstance().ad.where('id').equals(this.id).delete();
	}

	equals(other: Ad): boolean {
		return (
			this.id === other.id &&
			this.creator.equals(other.creator) &&
			this.chatId === other.chatId &&
			this._title === other._title &&
			this._description === other._description &&
			this._priceType === other._priceType &&
			this._priceAmount === other._priceAmount
		);
	}

	static fromJson(json: string | any): Ad {
		json = Utils.getAsObject(json);
		const creator = User.fromJson(Utils.getDelAndThrow('creator', json));
		return new Ad(
			Utils.getDelAndThrow('id', json),
			creator,
			Utils.getDelAndThrow('chatId', json),
			Utils.getDelAndThrow('title', json),
			Utils.getDelAndThrow('description', json),
			Utils.getDelAndThrow('priceType', json),
			Utils.getDelAndThrow('priceAmount', json),
		);
	}

	static load(id: string): Promise<Ad> {
		let iAd: any;
		return DB.getInstance()
			.ad.where('id')
			.equals(id)
			.first()
			.then((adResult: any) => {
				if (!adResult) {
					throw new NotFoundInDatabase(`Ad with id ${id}`);
				}
				iAd = adResult;
				return User.load(iAd.creator);
			})
			.then((userResult: User) => {
				return new Ad(
					iAd.id,
					userResult,
					iAd.chatId,
					iAd.title,
					iAd.description,
					iAd.priceType,
					iAd.priceAmount,
				);
			})
			.catch((err) => {
				console.error('Ad.load', err);
				return Promise.reject(err);
			});
	}

	static genId(): string {
		const currentMillis = new Date().getTime();
		return `ad-${currentMillis}-${Utils.getRandomString(16)}`;
	}

	static fromNewAdMessage(message: NewAdMessage) {
		return new Ad(
			message.adId,
			message.sender as User,
			message.chatId,
			message.title,
			message.description,
			message.priceType,
			message.priceAmount,
		);
	}

	get title() {
		return this._title;
	}

	get description() {
		return this._description;
	}

	get priceType() {
		return this._priceType;
	}

	get priceAmount() {
		return this._priceAmount;
	}
}
