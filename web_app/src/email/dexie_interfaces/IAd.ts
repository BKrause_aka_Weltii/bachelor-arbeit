import { IUser } from './IUser';

export enum PriceType {
	SELL = 'SELL',
	CHANGE = 'CHANGE',
	GIVE_AWAY = 'GIVE_AWAY',
	SEARCH = 'SEARCH',
	FOUND = 'FOUND',
	BORROW = 'BORROW',
}

export interface IAd {
	id: string;
	creator: IUser;
	_title: string;
	_description: string;
	_priceType: PriceType;
	_priceAmount: string;
}

export const adValues = {
	SELL: {
		type: PriceType.SELL,
		icon: 'coins',
		description: 'verkaufe %s',
		title: 'Verkaufen',
	},
	CHANGE: {
		type: PriceType.CHANGE,
		icon: 'recycle',
		description: 'tausche %s',
		title: 'Tauschen',
	},
	GIVE_AWAY: {
		type: PriceType.GIVE_AWAY,
		icon: 'gift',
		description: 'verschenke %s',
		title: 'Verschenken',
	},
	SEARCH: {
		type: PriceType.SEARCH,
		icon: 'search',
		description: 'suche %s',
		title: 'Suche',
	},
	FOUND: {
		type: PriceType.FOUND,
		icon: 'exchange-alt',
		description: 'habe %s gefunden',
		title: 'Gefunden',
	},
	BORROW: {
		type: PriceType.BORROW,
		icon: 'exchange-alt',
		description: 'möchte %s verleihen',
		title: 'Verleihen',
	},
};
