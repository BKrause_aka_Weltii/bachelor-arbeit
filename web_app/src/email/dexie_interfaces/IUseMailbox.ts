export interface IUseMailbox {
	mailbox: string;
	use: boolean;
}
