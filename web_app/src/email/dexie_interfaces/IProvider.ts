import { Encryption } from '../interfaces/provider';

export interface IProvider {
	id: string; // pk
	name: string;
	baseUrl: string;
	smtpUrl: string;
	smtpPort: number;
	smtpEncryption: Encryption;
	imapUrl: string;
	imapPort: number;
	imapEncryption: Encryption;
}
