import { IUser } from './IUser';
import { AMessage } from './AMessage';
import { IAd } from './IAd';

export enum ChatType {
	NORMAL = 'NORMAL',
	AD = 'AD',
}

export interface IChat {
	id: string; // pk
	type: ChatType;
	creationDate: string;
	name: string;
	participants: IUser[];
	messages: AMessage[];
	admins: IUser[];
	isPublic: boolean;
	onlyAdminsCanSend: boolean;
	ads?: IAd[];
}
