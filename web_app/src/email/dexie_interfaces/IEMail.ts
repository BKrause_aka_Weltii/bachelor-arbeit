export interface IEMail {
	id?: string;
	messageId: string; // pk
	subject: string;
	date: string;
	body: string;
	chatVersion: string;
	mimeVersion: string;
	contentType: string;
	from: string; // will be an email address of the creator
	to: string[];
	chatId: string; // fk of the chat
}
