export interface IAdditionalHeaderFields {
	messageId: string; // fk of an email
	key: string;
	value: string;
}
