import { JsonContext, JsonRebuildable } from '../interfaces/json-rebuildable';
import { IUser } from './IUser';
import { DB } from '../db';
import { DbClass } from '../interfaces/db-class';
import { IFromEmail, IToEmail } from '../interfaces/IFromEmail';
import { IEMail } from './IEMail';
import { MustBeImplementedError } from '../exceptions/MustBeImplementedError';
import { Chat } from '../Chat';

export abstract class AMessage
	implements DbClass, IFromEmail, IToEmail, JsonRebuildable {
	protected constructor(
		readonly sender: IUser,
		readonly messageId: string,
		readonly chatId: string,
		readonly messageDate: string,
		readonly chatContent: string,
	) {}

	public abstract toString(): string;

	save(): Promise<any> {
		const json: any = this.toJson(false, JsonContext.DATABASE);
		return Promise.resolve()
			.then(() => {
				return DB.getInstance().messages.put(json as AMessage);
			})
			.catch((err) => {
				return err;
			});
	}

	static load(pk: string) {
		throw new Error('You must implement IMessage.load!');
	}

	delete(): Promise<number> {
		return DB.getInstance()
			.messages.where('messageId')
			.equals(this.messageId)
			.delete();
	}

	equals(other: AMessage): boolean {
		return (
			this.sender.email === other.sender.email &&
			this.sender.nickname === other.sender.nickname &&
			this.messageId === other.messageId &&
			this.messageDate === other.messageDate &&
			this.chatContent === other.chatContent &&
			this.chatId === other.chatId
		);
	}

	toJson(
		toString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | any {
		throw new MustBeImplementedError('IMessage.toJson');
	}

	toEmail(chat: Chat): IEMail {
		throw new MustBeImplementedError('IMessage.toEmail');
	}
}
