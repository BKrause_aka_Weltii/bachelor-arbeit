import { IUser } from './IUser';
import { DbClass } from '../interfaces/db-class';
import { JsonRebuildable, JsonContext } from '../interfaces/json-rebuildable';
import { IChat } from './IChat';

export interface ILoginUser extends IUser, DbClass, JsonRebuildable {
	email: string; // pk
	nickname: string;
	loginName: string;
	password: string;
	chosenProviderId: string; // fk of an provider
	chats: IChat[];

	save(): void;
	delete(): void;
	equals(other: DbClass): boolean;
	toJson(asString: boolean, context: JsonContext): void;
	toUser(): void;
}
