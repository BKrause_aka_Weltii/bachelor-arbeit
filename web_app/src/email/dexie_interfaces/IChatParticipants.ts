export interface IChatParticipants {
	email: string; // fk of an user
	chatId: string; // fk of an chat
	isAdmin: boolean; // is the user an admin in the group
}
