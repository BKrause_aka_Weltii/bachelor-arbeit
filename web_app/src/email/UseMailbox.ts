import { IUseMailbox } from './dexie_interfaces/IUseMailbox';
import { DbClass } from './interfaces/db-class';
import { DB } from './db';
import { NotFoundInDatabase } from './NotFoundInDatabase';
import { JsonRebuildable, JsonContext } from './interfaces/json-rebuildable';
import { Utils } from './utils';

export class UseMailbox implements IUseMailbox, DbClass, JsonRebuildable {
	constructor(readonly mailbox: string, public use: boolean) {}

	save(): Promise<any> {
		return DB.getInstance().mailboxes.put(this);
	}

	static bulkSave(mailboxes: UseMailbox[]) {
		return DB.getInstance().mailboxes.bulkPut(mailboxes, {
			allKeys: true,
		});
	}

	static load(mailbox: string) {
		return DB.getInstance()
			.mailboxes.where('mailbox')
			.equals(mailbox)
			.first()
			.then((useMailboxResult: IUseMailbox | undefined) => {
				if (useMailboxResult) {
					return new UseMailbox(useMailboxResult.mailbox, useMailboxResult.use);
				} else {
					throw new NotFoundInDatabase(`Mailbox: ${mailbox}`);
				}
			});
	}

	static loadAll() {
		return DB.getInstance()
			.mailboxes.toArray()
			.then((allMailboxesResult: IUseMailbox[]) => {
				return allMailboxesResult.map((useMailboxResult) => {
					return new UseMailbox(useMailboxResult.mailbox, useMailboxResult.use);
				});
			});
	}

	delete() {
		return DB.getInstance()
			.mailboxes.where('mailbox')
			.equals(this.mailbox)
			.delete();
	}

	equals(other: UseMailbox) {
		return this.mailbox === other.mailbox && this.use === other.use;
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	) {
		let json = {
			mailbox: this.mailbox,
			use: this.use,
		};
		return asString ? JSON.stringify(json) : json;
	}

	static fromJson(json: string | any) {
		json = Utils.getAsObject(json);
		const use = Utils.getDelAndThrow('use', json);
		return new UseMailbox(
			Utils.getDelAndThrow('mailbox', json),
			use === 'true' || use === true,
		);
	}

	static getOnlyUsed() {
		return DB.getInstance()
			.mailboxes.toArray()
			.then((allMailboxesResult: IUseMailbox[]) => {
				allMailboxesResult = allMailboxesResult.filter(
					(mailbox) => mailbox.use,
				);
				return allMailboxesResult.map((useMailboxResult) => {
					return new UseMailbox(useMailboxResult.mailbox, useMailboxResult.use);
				});
			})
			.catch((err: any) => {
				console.error('UseMailbox.getOnlyUsed', err);
				throw err;
			});
	}
}
