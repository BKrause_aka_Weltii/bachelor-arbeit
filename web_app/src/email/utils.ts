import { MissingData } from './MissingData';
import { DateTime } from 'luxon';
import { AMessage } from './dexie_interfaces/AMessage';
import { EMail } from './EMail';
import { Config } from '../Config';
import { User } from './User';

export namespace Utils {
	export function getAndDel(key: string, json: any) {
		const tmp = json[key];
		delete json[key];
		return tmp;
	}

	export function getDelAndThrow(key: string, json: any) {
		const value = getAndDel(key, json);
		if (value == null || value == undefined) {
			throw new MissingData(key);
		}
		return value;
	}

	export function mergeObjects(...objects: Array<any>) {
		const newObj: any = {};
		objects.forEach((obj) => {
			Object.keys(obj || {}).forEach((key) => {
				newObj[key] = obj[key];
			});
		});
		return newObj;
	}

	/**
	 * Makes snake_case strings to camelCase strings
	 * @param input
	 */
	export function snakeCaseToCamelCase(input: string): string {
		input = input.replace('-', '_');
		const place = input.search('_');
		if (place >= 0) {
			const maxPlace = input.length - 1; // to prevent errors with _ at the end of a string
			if (place == maxPlace) {
				return input.substring(0, input.length - 1);
			} else {
				const substr = input.substring(
					Math.min(place, maxPlace),
					Math.min(place + 2, maxPlace),
				); // will be _l
				const afterSubstr = input.substring(
					Math.min(place + 1, maxPlace),
					Math.min(place + 2, maxPlace),
				);
				// to prevent a upper case letter at the start
				// example: _i_am_a_string => IAmAString is incorrect
				// example: _i_am_a_string => iAmAString is correct
				const isFirst = place === 0;
				input = input.replace(
					substr,
					isFirst ? afterSubstr : afterSubstr.toUpperCase(),
				);
				return snakeCaseToCamelCase(input);
			}
		} else {
			return input;
		}
	}

	export function transformAllSnakeCaseToCamelCase(object: any) {
		// FIXME add unit test!
		let newObject: any = {};
		Object.keys(object).forEach((key) => {
			const oldKey = key;
			key = Utils.snakeCaseToCamelCase(key);
			if (
				typeof object[oldKey] === 'object' &&
				!Array.isArray(object[oldKey])
			) {
				newObject[key] = transformAllSnakeCaseToCamelCase(object[oldKey]);
			} else {
				newObject[key] = object[oldKey];
			}
		});
		return newObject;
	}

	/**
	 * Replaces all uppercase letters with _<the letter>
	 * @param input
	 */
	export function camelCaseToSnakeCase(input: string): string {
		const index = input.search(/[A-Z]/);
		if (index > 0) {
			const start = input.substring(0, index);
			const end = input.substring(index + 1);
			const newPart = '_' + input[index].toLowerCase();
			return camelCaseToSnakeCase(`${start}${newPart}${end}`);
		} else if (index === 0) {
			// make only the first letter to lower case and continue;
			return camelCaseToSnakeCase(input[0].toLowerCase() + input.substring(1));
		} else {
			return input;
		}
	}

	export function replaceCamelCaseToSnakeCaseRecursively(object: any) {
		let newObject: any = {};
		Object.keys(object).forEach((key) => {
			const oldKey = key;
			key = camelCaseToSnakeCase(key);
			if (
				typeof object[oldKey] === 'object' &&
				!Array.isArray(object[oldKey])
			) {
				newObject[key] = replaceCamelCaseToSnakeCaseRecursively(object[oldKey]);
			} else {
				newObject[key] = object[oldKey];
			}
		});
		return newObject;
	}

	export function isLocalhost() {
		return process.env.ENVIRONMENT === 'LOCAL';
	}

	export function getCurrentDateTime(): DateTime {
		return DateTime.local();
	}

	export function dateToTimeString(date: DateTime): string {
		// (Wed, 30 Dec 2020 14:46:16 +0000).
		return date.toFormat('ccc, dd LLL yyyy HH:mm:ss ZZZ');
	}

	export function timeStringToDate(timeString: string): DateTime {
		const date = DateTime.fromRFC2822(timeString);
		if (!date.isValid) {
			throw new Error(
				`timestring ${timeString} doesn't match the RFC2822 specification!`,
			);
		}
		return date;
	}

	export function sortMessages(messages: AMessage[]) {
		return messages.sort(
			(b, a) =>
				Utils.timeStringToDate(b.messageDate).toMillis() -
				Utils.timeStringToDate(a.messageDate).toMillis(),
		);
	}

	/**
	 * Parse the input if it's a string
	 * @param input json string or object
	 */
	export function getAsObject(input: string | any): any {
		if (typeof input === 'string') {
			return JSON.parse(input);
		}
		return input;
	}

	export function isValidEmailAddress(emailAddress: string) {
		return !!emailAddress.match(/[^@]+@[^@.]+\.[^@.]+/);
	}

	export function getLastDateFromEmails(emails: EMail[]): string {
		const sortedDates = emails
			.map((email) => email.date)
			.sort((a, b) => {
				const aDate = Utils.timeStringToDate(a);
				const bDate = Utils.timeStringToDate(b);
				return bDate.toMillis() - aDate.toMillis();
			});
		return sortedDates[sortedDates.length - 1];
	}

	export function getRandomString(length: number) {
		const possibleChars = Config.possibleChars;
		const possibleCharsLength = possibleChars.length;
		let res = '';
		for (let i = 0; i < length; i++) {
			res += possibleChars[Math.floor(Math.random() * possibleCharsLength)];
		}
		return res;
	}

	export function userArrayToString(users: User[]) {
		let ret = '';
		users.forEach((user) => (ret = `${ret}, ${user.displayName}`));
		return ret.substring(2);
	}
}
