import { EMailDefaults } from '../../Config';
import { Chat } from '../Chat';
import { ChatContentTypes } from '../ChatContentTypes';
import { DB } from '../db';
import { IEMail } from '../dexie_interfaces/IEMail';
import { AMessage } from '../dexie_interfaces/AMessage';
import { EMail } from '../EMail';
import { NotUseableInJsonContextError } from '../exceptions/NotUseableInJsonContextError';
import { IFromEmail, IToEmail } from '../interfaces/IFromEmail';
import { JsonContext, JsonRebuildable } from '../interfaces/json-rebuildable';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { User } from '../User';
import { Utils } from '../utils';
import { WrongEmailType } from '../WrongEmailType';

export class DeleteMessage
	extends AMessage
	implements JsonRebuildable, IFromEmail {
	constructor(
		sender: User,
		messageId: string,
		chatId: string,
		messageDate: string,
		readonly deleteUserEmail: string,
	) {
		super(
			sender,
			messageId,
			chatId,
			messageDate,
			ChatContentTypes.DELETE_USER_FROM_CHAT,
		);
	}

	static fromJson(json: string | any): DeleteMessage {
		json = Utils.getAsObject(json);
		return new DeleteMessage(
			User.fromJson(json.sender),
			json.messageId,
			json.chatId,
			json.messageDate,
			json.deleteUserEmail,
		);
	}

	toJson(
		toString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | any {
		let json: any = {
			sender: null,
			messageId: this.messageId,
			chatId: this.chatId,
			messageDate: this.messageDate,
			deleteUserEmail: this.deleteUserEmail,
			chatContent: ChatContentTypes.DELETE_USER_FROM_CHAT,
		};
		switch (context) {
			case JsonContext.DATABASE:
				json['sender'] = this.sender.email;
				break;
			case JsonContext.INTERNAL:
				json['sender'] = (this.sender as User).toJson(false, context);
				break;
			case JsonContext.PROXY:
				throw new NotUseableInJsonContextError(
					'DeleteMessage.toJson',
					JsonContext.PROXY,
				);
		}
		return toString ? JSON.stringify(json) : json;
	}

	static load(messageId: string): Promise<DeleteMessage> {
		let iMessage: AMessage;
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.messages.where('messageId')
					.equals(messageId)
					.first();
			})
			.then((iMessageResult: AMessage | undefined) => {
				if (!iMessageResult) {
					console.log('messageId', messageId);
					throw new NotFoundInDatabase(`DeleteMessage ${messageId}`);
				}
				if (
					iMessageResult?.chatContent != ChatContentTypes.DELETE_USER_FROM_CHAT
				) {
					throw new Error(
						`Json has the wrong type. Expected ${ChatContentTypes.DELETE_USER_FROM_CHAT} instead of ${iMessageResult?.chatContent}`,
					);
				}
				iMessage = iMessageResult;
				return User.load(iMessageResult.sender as any);
			})
			.then((sender: User) => {
				if (!sender) {
					throw 'sender is undefined!';
				}
				return new DeleteMessage(
					sender,
					iMessage.messageId,
					iMessage.chatId,
					iMessage.messageDate,
					(iMessage as any).deleteUserEmail,
				);
			})
			.catch((err) => {
				console.error('DeleteMessage - load', err);
				throw err;
			});
	}

	static fromEmail(email: EMail): Promise<DeleteMessage> {
		return Promise.resolve()
			.then(() => {
				if (email.chatContent !== ChatContentTypes.DELETE_USER_FROM_CHAT) {
					throw new WrongEmailType(
						email,
						ChatContentTypes.DELETE_USER_FROM_CHAT,
					);
				}
				return User.loadOrCreateNew(email.from);
			})
			.then((sender: User) => {
				return new DeleteMessage(
					sender,
					email.messageId,
					email.chatId,
					email.date,
					email.additionalHeaderFields['deleteUserEmail'],
				);
			});
	}

	toEmail(chat: Chat): IEMail {
		const senderDeleteHimself = this.sender.email === this.deleteUserEmail;
		const subject: string = senderDeleteHimself
			? `${this.sender.email} left the chat ${chat.name}`
			: `${this.sender.email} removes ${this.deleteUserEmail} from the chat ${chat.name}`;
		return new EMail(
			{
				from: this.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== this.sender.email)
					.concat(this.deleteUserEmail),
				subject: subject,
				date: this.messageDate,
				messageId: this.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: this.chatId,
				chatContent: ChatContentTypes.DELETE_USER_FROM_CHAT,
				additionalHeaderFields: {
					deleteUserEmail: this.deleteUserEmail,
				},
			},
			subject,
		);
	}

	equals(other: DeleteMessage): boolean {
		return (
			super.equals(other as AMessage) &&
			this.deleteUserEmail === other.deleteUserEmail
		);
	}

	senderRemovesHimself(): boolean {
		return this.sender.email === this.deleteUserEmail;
	}

	toString(): string {
		return this.senderRemovesHimself()
			? `${this.sender.email} left the chat`
			: `${this.sender.email} removes ${this.deleteUserEmail} from the chat`;
	}
}
