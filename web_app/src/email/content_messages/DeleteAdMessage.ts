import { AMessage as AMessage } from '../dexie_interfaces/AMessage';
import { ChatContentTypes } from '../ChatContentTypes';
import { EMail } from '../EMail';
import { WrongEmailType } from '../WrongEmailType';
import { User } from '../User';
import { DB } from '../db';
import { JsonContext } from '../interfaces/json-rebuildable';
import { NotUseableInJsonContextError } from '../exceptions/NotUseableInJsonContextError';
import { Chat } from '../Chat';
import { EMailDefaults } from '../../Config';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { Utils } from '../utils';

export class DeleteAdMessage extends AMessage {
	constructor(
		sender: User,
		messageId: string,
		chatId: string,
		messageDate: string,
		readonly adId: string,
		readonly title: string,
	) {
		super(sender, messageId, chatId, messageDate, ChatContentTypes.DELETE_AD);
	}

	equals(other: DeleteAdMessage): boolean {
		return (
			(this.sender as User).equals(other.sender as User) &&
			this.messageId === other.messageId &&
			this.chatId === other.chatId &&
			this.messageDate === other.messageDate &&
			this.adId === other.adId &&
			this.title === other.title
		);
	}

	toEmail(chat: Chat): EMail {
		return new EMail(
			{
				from: this.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== this.sender.email),
				subject: this.toString(),
				date: this.messageDate,
				messageId: this.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: this.chatId,
				chatContent: ChatContentTypes.DELETE_AD,
				additionalHeaderFields: {
					adId: this.adId,
					title: this.title,
				},
			},
			// TODO create custom email message for every price type.
			`${this.sender.email} deletes the ad "${this.title}".`,
		);
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | any {
		let json;
		switch (context) {
			case JsonContext.INTERNAL:
				json = {
					sender: (this.sender as User).toJson(false, context),
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					adId: this.adId,
					title: this.title,
					chatContent: ChatContentTypes.DELETE_AD,
				};
				break;
			case JsonContext.DATABASE:
				json = {
					sender: this.sender.email,
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					adId: this.adId,
					title: this.title,
					chatContent: ChatContentTypes.DELETE_AD,
				};
				break;
			case JsonContext.PROXY:
				throw new NotUseableInJsonContextError(
					'DeleteAdMessage.toJson',
					JsonContext.PROXY,
				);
		}
		return asString ? JSON.stringify(json) : json;
	}

	toString(): string {
		return `${this.sender.email} deletes "${this.title}".`;
	}

	static fromJson(json: string | any) {
		json = Utils.getAsObject(json);
		const sender = User.fromJson(Utils.getDelAndThrow('sender', json));
		return new DeleteAdMessage(
			sender,
			json.messageId,
			json.chatId,
			json.messageDate,
			Utils.getDelAndThrow('adId', json),
			Utils.getAndDel('title', json) || 'NO_VALUE',
		);
	}

	static load(messageId: string): Promise<DeleteAdMessage> {
		let iMessage: any;
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.messages.where('messageId')
					.equals(messageId)
					.first();
			})
			.then((iMessageResult: AMessage | undefined) => {
				if (!iMessageResult) {
					throw new NotFoundInDatabase(`DeleteAdMessage ${messageId}`);
				}
				if (iMessageResult?.chatContent != ChatContentTypes.DELETE_AD) {
					throw new Error(
						`Json has the wrong type. Expected ${ChatContentTypes.DELETE_AD} instead of ${iMessageResult?.chatContent}`,
					);
				}
				iMessage = iMessageResult as DeleteAdMessage;
				return User.load(iMessageResult.sender as any);
			})
			.then((sender: User) => {
				if (!sender) {
					throw 'sender is undefined!';
				}
				return new DeleteAdMessage(
					sender,
					iMessage.messageId,
					iMessage.chatId,
					iMessage.messageDate,
					iMessage.adId,
					iMessage.title,
				);
			})
			.catch((err) => {
				console.error('DeleteAdMessage - load', err);
				throw err;
			});
	}

	static fromEmail(email: EMail): Promise<DeleteAdMessage> {
		return Promise.resolve()
			.then(() => {
				if (email.chatContent !== ChatContentTypes.DELETE_AD) {
					throw new WrongEmailType(email, ChatContentTypes.DELETE_AD);
				}
				return User.loadOrCreateNew(email.from);
			})
			.then((sender: User) => {
				return new DeleteAdMessage(
					sender,
					email.messageId,
					email.chatId,
					email.date,
					email.additionalHeaderFields.adId,
					email.additionalHeaderFields.title,
				);
			});
	}
}
