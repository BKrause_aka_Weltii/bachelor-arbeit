import { AMessage } from '../dexie_interfaces/AMessage';
import { User } from '../User';
import { ChatContentTypes } from '../ChatContentTypes';
import { Chat } from '../Chat';
import { EMailDefaults } from '../../Config';
import { EMail } from '../EMail';
import { JsonContext } from '../interfaces/json-rebuildable';
import { NotUseableInJsonContextError } from '../exceptions/NotUseableInJsonContextError';
import { Utils } from '../utils';
import { DB } from '../db';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { WrongEmailType } from '../WrongEmailType';

export class JoinRequestMessage extends AMessage {
	constructor(
		sender: User,
		messageId: string,
		chatId: string,
		messageDate: string,
	) {
		super(
			sender,
			messageId,
			chatId,
			messageDate,
			ChatContentTypes.JOIN_REQUEST,
		);
	}

	toEmail(chat: Chat): EMail {
		return new EMail(
			{
				from: this.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== this.sender.email),
				subject: `${this.sender.email} whats to join the chat ${chat.name}`,
				date: this.messageDate,
				messageId: this.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: this.chatId,
				chatContent: ChatContentTypes.JOIN_REQUEST,
			},
			`${this.sender.email} whats to join the chat ${chat.name}`,
		);
	}

	toString(): string {
		return `${this.sender.email} whats to join this chat}`;
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | any {
		let json;
		switch (context) {
			case JsonContext.INTERNAL:
				json = {
					sender: (this.sender as User).toJson(false, context),
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					chatContent: ChatContentTypes.JOIN_REQUEST,
				};
				break;
			case JsonContext.DATABASE:
				json = {
					sender: this.sender.email,
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					chatContent: ChatContentTypes.JOIN_REQUEST,
				};
				break;
			case JsonContext.PROXY:
				throw new NotUseableInJsonContextError(
					'JoinRequestMessage.toJson',
					JsonContext.PROXY,
				);
		}
		return asString ? JSON.stringify(json) : json;
	}

	static fromJson(json: string | any) {
		json = Utils.getAsObject(json);
		return new JoinRequestMessage(
			User.fromJson(Utils.getDelAndThrow('sender', json)),
			Utils.getDelAndThrow('messageId', json),
			Utils.getDelAndThrow('chatId', json),
			Utils.getDelAndThrow('messageDate', json),
		);
	}

	static load(messageId: string) {
		let iMessage: AMessage;
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.messages.where('messageId')
					.equals(messageId)
					.first();
			})
			.then((iMessageResult: AMessage | undefined) => {
				if (!iMessageResult) {
					throw new NotFoundInDatabase(`JoinRequestMessage ${messageId}`);
				}
				if (iMessageResult?.chatContent != ChatContentTypes.JOIN_REQUEST) {
					throw new Error(
						`Json has the wrong type. Expected ${ChatContentTypes.JOIN_REQUEST} instead of ${iMessageResult?.chatContent}`,
					);
				}
				iMessage = iMessageResult;
				return User.load(iMessageResult.sender as any);
			})
			.then((sender: User) => {
				if (!sender) {
					throw 'sender is undefined!';
				}
				return new JoinRequestMessage(
					sender,
					iMessage.messageId,
					iMessage.chatId,
					iMessage.messageDate,
				);
			})
			.catch((err) => {
				console.error('JoinRequestMessage - load', err);
				throw err;
			});
	}

	static fromEmail(email: EMail): Promise<JoinRequestMessage> {
		return Promise.resolve()
			.then(() => {
				if (email.chatContent !== ChatContentTypes.JOIN_REQUEST) {
					throw new WrongEmailType(email, ChatContentTypes.JOIN_REQUEST);
				}
				return User.loadOrCreateNew(email.from);
			})
			.then((sender: User) => {
				return new JoinRequestMessage(
					sender,
					email.messageId,
					email.chatId,
					email.date,
				);
			});
	}
}
