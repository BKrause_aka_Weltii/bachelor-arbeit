import { DeleteAdMessage } from '../DeleteAdMessage';
import { TestUtils } from '../../tests/TestUtils';
import { JsonContext } from '../../interfaces/json-rebuildable';
import sinon from 'sinon';
import generateRandomUser = TestUtils.generateRandomUser;
import generateNumber = TestUtils.generateNumber;
import { User } from '../../User';
import { NotFoundInDatabase } from '../../NotFoundInDatabase';
import { EMail } from '../../EMail';
import { EMailDefaults } from '../../../Config';
import { ChatContentTypes } from '../../ChatContentTypes';
import { Chat } from '../../Chat';
import { WrongEmailType } from '../../WrongEmailType';
import generateChat = TestUtils.generateChat;
import { Utils } from '../../utils';
import { NotUseableInJsonContextError } from '../../exceptions/NotUseableInJsonContextError';
import { Ad } from '../../../email/Ad';

function generateDeleteAdMessage(custom?: {
	sender?: User;
	chatId?: string;
	date?: string;
}) {
	const adId = Ad.genId();
	return new DeleteAdMessage(
		custom?.sender || generateRandomUser(),
		`message-id-${generateNumber()}`,
		custom?.chatId || `chat-id-${generateNumber()}`,
		custom?.date || Utils.dateToTimeString(Utils.getCurrentDateTime()),
		adId,
		`title-${adId}`,
	);
}

describe('Test DeleteAdMessage - toJson', () => {
	it('should return correct json for internal context', function () {
		const message = generateDeleteAdMessage();
		const json = message.toJson(false, JsonContext.INTERNAL);
		const expectedJson = {
			sender: {
				email: message.sender.email,
				nickname: message.sender.nickname,
			},
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			adId: message.adId,
			title: message.title,
			chatContent: ChatContentTypes.DELETE_AD,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should return correct json for database context', function () {
		const message = generateDeleteAdMessage();
		const json = message.toJson(false, JsonContext.DATABASE);
		const expectedJson = {
			sender: message.sender.email,
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			adId: message.adId,
			title: message.title,
			chatContent: ChatContentTypes.DELETE_AD,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should throw error on trying to export for proxy context', function () {
		const message = generateDeleteAdMessage();
		try {
			message.toJson(false, JsonContext.PROXY);
		} catch (e) {
			expect(e).toEqual(
				new NotUseableInJsonContextError(
					'DeleteAdMessage.toJson',
					JsonContext.PROXY,
				),
			);
		}
	});
});

describe('Test DeleteAdMessage - fromJson', function () {
	it('should load from json', function () {
		const message = generateDeleteAdMessage();
		const fromJson: DeleteAdMessage = DeleteAdMessage.fromJson(
			message.toJson(),
		);
		expect(fromJson).toEqual(message);
		expect(fromJson.equals(message)).toBeTrue();
	});
});

describe('Test DeleteAdMessage - save', () => {
	it('should save successfully', function (done) {
		const message = generateDeleteAdMessage();
		message.save().then((saveResult) => {
			expect(saveResult).toBe(message.messageId);
			done();
		});
	});
});

describe('Test DeleteAdMessage - load', () => {
	it('should load successfully', function (done) {
		const message = generateDeleteAdMessage();
		(message.sender as User)
			.save()
			.then((saveResult) => {
				expect(saveResult).toBe(message.sender.email);
				return message.save();
			})
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return DeleteAdMessage.load(message.messageId);
			})
			.then((resultMessage: DeleteAdMessage) => {
				expect(message.equals(resultMessage)).toBeTrue();
				done();
			});
	});

	it('should fail because there is no message', function (done) {
		const message = generateDeleteAdMessage();
		DeleteAdMessage.load(message.messageId).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`DeleteAdMessage ${message.messageId}`),
			);
			done();
		});
	});
});

describe('Test DeleteAdMessage - delete', () => {
	it('should delete successfully', function (done) {
		const message = generateDeleteAdMessage();
		message
			.save()
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return message.delete();
			})
			.then((deleteResult) => {
				expect(deleteResult).toBe(1);
				done();
			});
	});

	it('should delete no data because the message is not saved', function (done) {
		const message = generateDeleteAdMessage();
		return message.delete().then((deleteResult) => {
			expect(deleteResult).toBe(0);
			done();
		});
	});
});

describe('Test DeleteAdMessage - fromEmail', () => {
	it('should create a DeleteAdMessage from an email successfully', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const adId = Ad.genId();
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.DELETE_AD,
				additionalHeaderFields: {
					adId: adId,
					title: `title-${adId}`,
				},
			},
			'A body for testing',
		);
		const expectedDeleteAdMessage = new DeleteAdMessage(
			sender,
			email.messageId,
			email.chatId,
			email.date,
			email.additionalHeaderFields?.adId,
			email.additionalHeaderFields?.title,
		);
		DeleteAdMessage.fromEmail(email).then((resultMessage: DeleteAdMessage) => {
			expect(resultMessage.equals(expectedDeleteAdMessage)).toBeTrue();
			done();
		});
	});

	it('should failed because the email has the wrong chatContent', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.INVITE,
			},
			'A body for testing',
		);
		DeleteAdMessage.fromEmail(email).catch((err) => {
			expect(err).toEqual(
				new WrongEmailType(email, ChatContentTypes.DELETE_AD),
			);
			done();
		});
	});
});

describe('Test DeleteAdMessage - toEMail', () => {
	it('should generates a EMail from DeleteAdMessage', function () {
		const chat = generateChat(1, 2);
		const message = generateDeleteAdMessage({
			sender: chat.participants[0],
			chatId: chat.id,
		});
		const expectedEmail = new EMail(
			{
				from: message.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== message.sender.email),
				subject: message.toString(),
				date: message.messageDate,
				messageId: message.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: chat.id,
				additionalHeaderFields: {
					adId: message.adId,
					title: message.title,
				},
				chatContent: ChatContentTypes.DELETE_AD,
			},
			`${message.sender.email} deletes the ad "${message.title}".`,
		);
		expect(message.toEmail(chat)).toEqual(expectedEmail);
	});
});
