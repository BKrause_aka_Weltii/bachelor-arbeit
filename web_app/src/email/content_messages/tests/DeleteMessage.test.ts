import { TestUtils } from '../../tests/TestUtils';
import { JsonContext } from '../../interfaces/json-rebuildable';
import generateRandomUser = TestUtils.generateRandomUser;
import generateNumber = TestUtils.generateNumber;
import { User } from '../../User';
import { NotFoundInDatabase } from '../../NotFoundInDatabase';
import { EMail } from '../../EMail';
import { EMailDefaults } from '../../../Config';
import { ChatContentTypes } from '../../ChatContentTypes';
import { WrongEmailType } from '../../WrongEmailType';
import generateChat = TestUtils.generateChat;
import { Utils } from '../../utils';
import { DeleteMessage } from '../DeleteMessage';
import { NotUseableInJsonContextError } from '../../exceptions/NotUseableInJsonContextError';
import { Chat } from '../../../email/Chat';

function generateDeleteMessage(custom?: {
	sender?: User;
	chatId?: string;
	date?: string;
	deleteUserMessage?: string;
}) {
	const sender = custom?.sender || generateRandomUser();
	return new DeleteMessage(
		sender,
		`message-id-${generateNumber()}`,
		custom?.chatId || `chat-id-${generateNumber()}`,
		custom?.date || Utils.dateToTimeString(Utils.getCurrentDateTime()),
		custom?.deleteUserMessage || sender.email,
	);
}

describe('Test DeleteMessage - toJson', () => {
	it('should return correct json for internal context', function () {
		const message = generateDeleteMessage();
		const json = message.toJson(false, JsonContext.INTERNAL);
		const expectedJson = {
			sender: {
				email: message.sender.email,
				nickname: message.sender.nickname,
			},
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			deleteUserEmail: message.deleteUserEmail,
			chatContent: ChatContentTypes.DELETE_USER_FROM_CHAT,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should return correct json for database context', function () {
		const message = generateDeleteMessage();
		const json = message.toJson(false, JsonContext.DATABASE);
		const expectedJson = {
			sender: message.sender.email,
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			deleteUserEmail: message.deleteUserEmail,
			chatContent: ChatContentTypes.DELETE_USER_FROM_CHAT,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should throw error on trying to export for proxy context', function () {
		const message = generateDeleteMessage();
		try {
			message.toJson(false, JsonContext.PROXY);
		} catch (e) {
			expect(e).toEqual(
				new NotUseableInJsonContextError(
					'DeleteMessage.toJson',
					JsonContext.PROXY,
				),
			);
		}
	});
});

describe('Test DeleteMessage - fromJson', function () {
	it('should load from json', function () {
		const message = generateDeleteMessage();
		expect(DeleteMessage.fromJson(message.toJson())).toEqual(message);
		expect(DeleteMessage.fromJson(message.toJson()).equals(message)).toBeTrue();
	});
});

describe('Test DeleteMessage - save', () => {
	it('should save successfully', function (done) {
		const message = generateDeleteMessage();
		message.save().then((saveResult) => {
			expect(saveResult).toBe(message.messageId);
			done();
		});
	});
});

describe('Test DeleteMessage - load', () => {
	it('should load successfully', function (done) {
		const message = generateDeleteMessage();
		(message.sender as User)
			.save()
			.then((saveResult) => {
				expect(saveResult).toBe(message.sender.email);
				return message.save();
			})
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return DeleteMessage.load(message.messageId);
			})
			.then((resultMessage: DeleteMessage) => {
				expect(message.equals(resultMessage)).toBeTrue();
				done();
			});
	});

	it('should fail because there is no message', function (done) {
		const message = generateDeleteMessage();
		DeleteMessage.load(message.messageId).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`DeleteMessage ${message.messageId}`),
			);
			done();
		});
	});
});

describe('Test DeleteMessage - delete', () => {
	it('should delete successfully', function (done) {
		const message = generateDeleteMessage();
		message
			.save()
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return message.delete();
			})
			.then((deleteResult) => {
				expect(deleteResult).toBe(1);
				done();
			});
	});

	it('should delete no data because the message is not saved', function (done) {
		const message = generateDeleteMessage();
		return message.delete().then((deleteResult) => {
			expect(deleteResult).toBe(0);
			done();
		});
	});
});

describe('Test DeleteMessage - fromEmail', () => {
	it('should create a DeleteMessage from an email successfully', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.DELETE_USER_FROM_CHAT,
				additionalHeaderFields: {
					deleteUserEmail: to[0].email,
				},
			},
			'The body can be ignored.',
		);
		const expectedDeleteMessage = new DeleteMessage(
			sender,
			email.messageId,
			email.chatId,
			email.date,
			email.to[0],
		);
		DeleteMessage.fromEmail(email).then((resultMessage: DeleteMessage) => {
			expect(resultMessage.equals(expectedDeleteMessage)).toBeTrue();
			done();
		});
	});

	it('should faile because the email has the wrong chatContent', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.TEXT_MESSAGE,
				additionalHeaderFields: {
					deleteUserEmail: to[0].email,
				},
			},
			'A body for testing',
		);
		DeleteMessage.fromEmail(email).catch((err) => {
			expect(err).toEqual(
				new WrongEmailType(email, ChatContentTypes.DELETE_USER_FROM_CHAT),
			);
			done();
		});
	});
});

describe('Test DeleteMessage - toEMail', () => {
	it('should generates a EMail from DeleteMessage', function (done) {
		const chat = generateChat(1, 3);
		const message = generateDeleteMessage({
			sender: chat.participants[2],
			chatId: chat.id,
			deleteUserMessage: chat.participants[2].email,
		});
		const subject = `${message.toString()} ${chat.id}`;
		const expectedEmail = new EMail(
			{
				from: message.sender.email,
				to: chat.participants.map((member: User) => member.email),
				subject: subject,
				date: message.messageDate,
				messageId: message.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: chat.id,
				chatContent: ChatContentTypes.DELETE_USER_FROM_CHAT,
				additionalHeaderFields: {
					deleteUserEmail: chat.participants[2].email,
				},
			},
			subject,
		);
		chat.addMessage(message).then(() => {
			expect(message.toEmail(chat)).toEqual(expectedEmail);
			done();
		});
	});
});
