import { TestUtils } from '../../tests/TestUtils';
import { JsonContext } from '../../interfaces/json-rebuildable';
import generateRandomUser = TestUtils.generateRandomUser;
import generateNumber = TestUtils.generateNumber;
import { User } from '../../User';
import { NotFoundInDatabase } from '../../NotFoundInDatabase';
import { EMail } from '../../EMail';
import { EMailDefaults } from '../../../Config';
import { ChatContentTypes } from '../../ChatContentTypes';
import { Chat } from '../../Chat';
import { WrongEmailType } from '../../WrongEmailType';
import generateChat = TestUtils.generateChat;
import { Utils } from '../../utils';
import { NotUseableInJsonContextError } from '../../../email/exceptions/NotUseableInJsonContextError';
import { JoinRequestMessage } from '../JoinRequestMessage';

function generateJoinRequestMessage(custom?: {
	sender?: User;
	chatId?: string;
	date?: string;
}) {
	return new JoinRequestMessage(
		custom?.sender || generateRandomUser(),
		`message-id-${generateNumber()}`,
		custom?.chatId || `chat-id-${generateNumber()}`,
		custom?.date || Utils.dateToTimeString(Utils.getCurrentDateTime()),
	);
}

describe('Test JoinRequestMessage - toJson', () => {
	it('should return correct json for internal context', function () {
		const message = generateJoinRequestMessage();
		const json = message.toJson(false, JsonContext.INTERNAL);
		const expectedJson = {
			sender: {
				email: message.sender.email,
				nickname: message.sender.nickname,
			},
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			chatContent: ChatContentTypes.JOIN_REQUEST,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should return correct json for database context', function () {
		const message = generateJoinRequestMessage();
		const json = message.toJson(false, JsonContext.DATABASE);
		const expectedJson = {
			sender: message.sender.email,
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			chatContent: ChatContentTypes.JOIN_REQUEST,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should throw error on trying to export for proxy context', function () {
		const message = generateJoinRequestMessage();
		try {
			message.toJson(false, JsonContext.PROXY);
		} catch (e) {
			expect(e).toEqual(
				new NotUseableInJsonContextError(
					'JoinRequestMessage.toJson',
					JsonContext.PROXY,
				),
			);
		}
	});
});

describe('Test JoinRequestMessage - fromJson', function () {
	it('should load from json', function () {
		const message = generateJoinRequestMessage();
		expect(JoinRequestMessage.fromJson(message.toJson())).toEqual(message);
		expect(
			JoinRequestMessage.fromJson(message.toJson()).equals(message),
		).toBeTrue();
	});
});

describe('Test JoinRequestMessage - save', () => {
	it('should save successfully', function (done) {
		const message = generateJoinRequestMessage();
		message.save().then((saveResult) => {
			expect(saveResult).toBe(message.messageId);
			done();
		});
	});
});

describe('Test JoinRequestMessage - load', () => {
	it('should load successfully', function (done) {
		const message = generateJoinRequestMessage();
		(message.sender as User)
			.save()
			.then((saveResult) => {
				expect(saveResult).toBe(message.sender.email);
				return message.save();
			})
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return JoinRequestMessage.load(message.messageId);
			})
			.then((resultMessage: JoinRequestMessage) => {
				expect(message.equals(resultMessage)).toBeTrue();
				done();
			});
	});

	it('should fail because there is no message', function (done) {
		const message = generateJoinRequestMessage();
		JoinRequestMessage.load(message.messageId).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`JoinRequestMessage ${message.messageId}`),
			);
			done();
		});
	});
});

describe('Test JoinRequestMessage - delete', () => {
	it('should delete successfully', function (done) {
		const message = generateJoinRequestMessage();
		message
			.save()
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return message.delete();
			})
			.then((deleteResult) => {
				expect(deleteResult).toBe(1);
				done();
			});
	});

	it('should delete no data because the message is not saved', function (done) {
		const message = generateJoinRequestMessage();
		return message.delete().then((deleteResult) => {
			expect(deleteResult).toBe(0);
			done();
		});
	});
});

describe('Test JoinRequestMessage - fromEmail', () => {
	it('should create a JoinRequestMessage from an email successfully', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: 'I am a title',
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.JOIN_REQUEST,
			},
			'I am a message!',
		);
		const expectedJoinRequestMessage = new JoinRequestMessage(
			sender,
			email.messageId,
			email.chatId,
			email.date,
		);
		JoinRequestMessage.fromEmail(email).then(
			(resultMessage: JoinRequestMessage) => {
				expect(resultMessage.equals(expectedJoinRequestMessage)).toBeTrue();
				done();
			},
		);
	});

	it('should failed because the email has the wrong chatContent', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.PROMOTE,
			},
			'A body for testing',
		);
		JoinRequestMessage.fromEmail(email).catch((err) => {
			expect(err).toEqual(
				new WrongEmailType(email, ChatContentTypes.JOIN_REQUEST),
			);
			done();
		});
	});
});

describe('Test JoinRequestMessage - toEMail', () => {
	it('should generates a EMail from JoinRequestMessage', function () {
		const chat = generateChat(1, 2);
		const message = generateJoinRequestMessage({
			sender: chat.participants[0],
			chatId: chat.id,
		});
		const expectedEmail = new EMail(
			{
				from: message.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== message.sender.email),
				subject: `${message.sender.email} whats to join the chat ${chat.name}`,
				date: message.messageDate,
				messageId: message.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: chat.id,
				chatContent: ChatContentTypes.JOIN_REQUEST,
			},
			`${message.sender.email} whats to join the chat ${chat.name}`,
		);
		expect(message.toEmail(chat)).toEqual(expectedEmail);
	});
});
