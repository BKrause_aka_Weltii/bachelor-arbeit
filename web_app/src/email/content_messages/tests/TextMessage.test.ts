import { TextMessage } from '../TextMessage';
import { TestUtils } from '../../tests/TestUtils';
import { JsonContext } from '../../interfaces/json-rebuildable';
import sinon from 'sinon';
import generateRandomUser = TestUtils.generateRandomUser;
import generateNumber = TestUtils.generateNumber;
import { User } from '../../User';
import { NotFoundInDatabase } from '../../NotFoundInDatabase';
import { EMail } from '../../EMail';
import { EMailDefaults } from '../../../Config';
import { ChatContentTypes } from '../../ChatContentTypes';
import { Chat } from '../../Chat';
import { WrongEmailType } from '../../WrongEmailType';
import generateChat = TestUtils.generateChat;
import { Utils } from '../../utils';
import { NotUseableInJsonContextError } from '../../../email/exceptions/NotUseableInJsonContextError';

function generateTextMessage(custom?: {
	sender?: User;
	chatId?: string;
	date?: string;
}) {
	return new TextMessage(
		custom?.sender || generateRandomUser(),
		`message-id-${generateNumber()}`,
		custom?.chatId || `chat-id-${generateNumber()}`,
		custom?.date || Utils.dateToTimeString(Utils.getCurrentDateTime()),
		'I am a message! :)',
	);
}

describe('Test TextMessage - toJson', () => {
	it('should return correct json for internal context', function () {
		const message = generateTextMessage();
		const json = message.toJson(false, JsonContext.INTERNAL);
		const expectedJson = {
			sender: {
				email: message.sender.email,
				nickname: message.sender.nickname,
			},
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			message: message.message,
			chatContent: ChatContentTypes.TEXT_MESSAGE,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should return correct json for database context', function () {
		const message = generateTextMessage();
		const json = message.toJson(false, JsonContext.DATABASE);
		const expectedJson = {
			sender: message.sender.email,
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			message: message.message,
			chatContent: ChatContentTypes.TEXT_MESSAGE,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should throw error on trying to export for proxy context', function () {
		const message = generateTextMessage();
		try {
			message.toJson(false, JsonContext.PROXY);
		} catch (e) {
			expect(e).toEqual(
				new NotUseableInJsonContextError(
					'TextMessage.toJson',
					JsonContext.PROXY,
				),
			);
		}
	});
});

describe('Test TextMessage - fromJson', function () {
	it('should load from json', function () {
		const message = generateTextMessage();
		expect(TextMessage.fromJson(message.toJson())).toEqual(message);
		expect(TextMessage.fromJson(message.toJson()).equals(message)).toBeTrue();
	});
});

describe('Test TextMessage - save', () => {
	it('should save successfully', function (done) {
		const message = generateTextMessage();
		message.save().then((saveResult) => {
			expect(saveResult).toBe(message.messageId);
			done();
		});
	});
});

describe('Test TextMessage - load', () => {
	it('should load successfully', function (done) {
		const message = generateTextMessage();
		(message.sender as User)
			.save()
			.then((saveResult) => {
				expect(saveResult).toBe(message.sender.email);
				return message.save();
			})
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return TextMessage.load(message.messageId);
			})
			.then((resultMessage: TextMessage) => {
				expect(message.equals(resultMessage)).toBeTrue();
				done();
			});
	});

	it('should fail because there is no message', function (done) {
		const message = generateTextMessage();
		TextMessage.load(message.messageId).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`TextMessage ${message.messageId}`),
			);
			done();
		});
	});
});

describe('Test TextMessage - delete', () => {
	it('should delete successfully', function (done) {
		const message = generateTextMessage();
		message
			.save()
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return message.delete();
			})
			.then((deleteResult) => {
				expect(deleteResult).toBe(1);
				done();
			});
	});

	it('should delete no data because the message is not saved', function (done) {
		const message = generateTextMessage();
		return message.delete().then((deleteResult) => {
			expect(deleteResult).toBe(0);
			done();
		});
	});
});

describe('Test TextMessage - fromEmail', () => {
	it('should create a TextMessage from an email successfully', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.TEXT_MESSAGE,
			},
			'A body for testing',
		);
		const expectedTextMessage = new TextMessage(
			sender,
			email.messageId,
			email.chatId,
			email.date,
			email.body,
		);
		TextMessage.fromEmail(email).then((resultMessage: TextMessage) => {
			expect(resultMessage.equals(expectedTextMessage)).toBeTrue();
			done();
		});
	});

	it('should failed because the email has the wrong chatContent', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.INVITE,
			},
			'A body for testing',
		);
		TextMessage.fromEmail(email).catch((err) => {
			expect(err).toEqual(
				new WrongEmailType(email, ChatContentTypes.TEXT_MESSAGE),
			);
			done();
		});
	});
});

describe('Test TextMessage - toEMail', () => {
	it('should generates a EMail from TextMessage', function () {
		const chat = generateChat(1, 2);
		const message = generateTextMessage({
			sender: chat.participants[0],
			chatId: chat.id,
		});
		const expectedEmail = new EMail(
			{
				from: message.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== message.sender.email),
				subject: `${message.sender.email} sends a message`,
				date: message.messageDate,
				messageId: message.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: chat.id,
				chatContent: ChatContentTypes.TEXT_MESSAGE,
			},
			message.message,
		);
		expect(message.toEmail(chat)).toEqual(expectedEmail);
	});
});
