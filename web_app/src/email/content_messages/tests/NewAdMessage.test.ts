import { NewAdMessage } from '../NewAdMessage';
import { TestUtils } from '../../tests/TestUtils';
import { JsonContext } from '../../interfaces/json-rebuildable';
import sinon from 'sinon';
import generateRandomUser = TestUtils.generateRandomUser;
import generateNumber = TestUtils.generateNumber;
import { User } from '../../User';
import { NotFoundInDatabase } from '../../NotFoundInDatabase';
import { EMail } from '../../EMail';
import { EMailDefaults } from '../../../Config';
import { ChatContentTypes } from '../../ChatContentTypes';
import { Chat } from '../../Chat';
import { WrongEmailType } from '../../WrongEmailType';
import generateChat = TestUtils.generateChat;
import { Utils } from '../../utils';
import { NotUseableInJsonContextError } from '../../exceptions/NotUseableInJsonContextError';
import { PriceType } from '../../../email/dexie_interfaces/IAd';
import { Ad } from '../../../email/Ad';

function generateNewAdMessage(custom?: {
	sender?: User;
	chatId?: string;
	date?: string;
	priceType?: PriceType;
}) {
	const adId = Ad.genId();
	return new NewAdMessage(
		custom?.sender || generateRandomUser(),
		`message-id-${generateNumber()}`,
		custom?.chatId || `chat-id-${generateNumber()}`,
		custom?.date || Utils.dateToTimeString(Utils.getCurrentDateTime()),
		adId,
		`title-${adId}`,
		'A ad description',
		custom?.priceType || PriceType.SELL,
		'100',
	);
}

describe('Test NewAdMessage - toJson', () => {
	it('should return correct json for internal context', function () {
		const message = generateNewAdMessage();
		const json = message.toJson(false, JsonContext.INTERNAL);
		const expectedJson = {
			sender: {
				email: message.sender.email,
				nickname: message.sender.nickname,
			},
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			adId: message.adId,
			title: message.title,
			description: message.description,
			priceType: message.priceType,
			priceAmount: message.priceAmount,
			chatContent: ChatContentTypes.NEW_AD,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should return correct json for database context', function () {
		const message = generateNewAdMessage();
		const json = message.toJson(false, JsonContext.DATABASE);
		const expectedJson = {
			sender: message.sender.email,
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			adId: message.adId,
			title: message.title,
			description: message.description,
			priceType: message.priceType,
			priceAmount: message.priceAmount,
			chatContent: ChatContentTypes.NEW_AD,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should throw error on trying to export for proxy context', function () {
		const message = generateNewAdMessage();
		try {
			message.toJson(false, JsonContext.PROXY);
		} catch (e) {
			expect(e).toEqual(
				new NotUseableInJsonContextError(
					'NewAdMessage.toJson',
					JsonContext.PROXY,
				),
			);
		}
	});
});

describe('Test NewAdMessage - fromJson', function () {
	it('should load from json', function () {
		const message = generateNewAdMessage();
		const fromJson: NewAdMessage = NewAdMessage.fromJson(message.toJson());
		expect(fromJson).toEqual(message);
		expect(fromJson.equals(message)).toBeTrue();
	});
});

describe('Test NewAdMessage - save', () => {
	it('should save successfully', function (done) {
		const message = generateNewAdMessage();
		message.save().then((saveResult) => {
			expect(saveResult).toBe(message.messageId);
			done();
		});
	});
});

describe('Test NewAdMessage - load', () => {
	it('should load successfully', function (done) {
		const message = generateNewAdMessage();
		(message.sender as User)
			.save()
			.then((saveResult) => {
				expect(saveResult).toBe(message.sender.email);
				return message.save();
			})
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return NewAdMessage.load(message.messageId);
			})
			.then((resultMessage: NewAdMessage) => {
				expect(message.equals(resultMessage)).toBeTrue();
				done();
			});
	});

	it('should fail because there is no message', function (done) {
		const message = generateNewAdMessage();
		NewAdMessage.load(message.messageId).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`NewAdMessage ${message.messageId}`),
			);
			done();
		});
	});
});

describe('Test NewAdMessage - delete', () => {
	it('should delete successfully', function (done) {
		const message = generateNewAdMessage();
		message
			.save()
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return message.delete();
			})
			.then((deleteResult) => {
				expect(deleteResult).toBe(1);
				done();
			});
	});

	it('should delete no data because the message is not saved', function (done) {
		const message = generateNewAdMessage();
		return message.delete().then((deleteResult) => {
			expect(deleteResult).toBe(0);
			done();
		});
	});
});

describe('Test NewAdMessage - fromEmail', () => {
	it('should create a NewAdMessage from an email successfully', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const adId = Ad.genId();
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.NEW_AD,
				additionalHeaderFields: {
					adId: adId,
					title: `title-${adId}`,
					description: 'A custom description',
					priceType: PriceType.SELL,
					priceAmount: '200',
				},
			},
			'A body for testing',
		);
		const expectedNewAdMessage = new NewAdMessage(
			sender,
			email.messageId,
			email.chatId,
			email.date,
			email.additionalHeaderFields?.adId,
			email.additionalHeaderFields?.title,
			email.additionalHeaderFields?.description,
			email.additionalHeaderFields?.priceType,
			email.additionalHeaderFields?.priceAmount,
		);
		NewAdMessage.fromEmail(email).then((resultMessage: NewAdMessage) => {
			expect(resultMessage.equals(expectedNewAdMessage)).toBeTrue();
			done();
		});
	});

	it('should failed because the email has the wrong chatContent', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.INVITE,
			},
			'A body for testing',
		);
		NewAdMessage.fromEmail(email).catch((err) => {
			expect(err).toEqual(new WrongEmailType(email, ChatContentTypes.NEW_AD));
			done();
		});
	});
});

describe('Test NewAdMessage - toEMail', () => {
	it('should generates a EMail from NewAdMessage', function () {
		const chat = generateChat(1, 2);
		const message = generateNewAdMessage({
			sender: chat.participants[0],
			chatId: chat.id,
		});
		const expectedEmail = new EMail(
			{
				from: message.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== message.sender.email),
				subject: `${message.sender.email} creates (or change) a ad - ${message.title}`,
				date: message.messageDate,
				messageId: message.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: chat.id,
				additionalHeaderFields: {
					adId: message.adId,
					title: message.title,
					description: message.description,
					priceType: message.priceType,
					priceAmount: message.priceAmount,
				},
				chatContent: ChatContentTypes.NEW_AD,
			},
			message.toString(),
		);
		expect(message.toEmail(chat)).toEqual(expectedEmail);
	});
});
