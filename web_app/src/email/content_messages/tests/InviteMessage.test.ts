import { InviteMessage, JsonInviteMessage } from '../InviteMessage';
import { ChatContentTypes } from '../../ChatContentTypes';
import { IUser } from '../../dexie_interfaces/IUser';
import { User } from '../../User';
import { MissingData } from '../../MissingData';
import { NotFoundInDatabase } from '../../NotFoundInDatabase';
import { EMail } from '../../EMail';
import { EMailDefaults } from '../../../Config';
import { WrongEmailType } from '../../WrongEmailType';
import { TestUtils } from '../../tests/TestUtils';
import { JsonContext } from '../../interfaces/json-rebuildable';
import { Utils } from '../../utils';
import { ChatType } from '../../../email/dexie_interfaces/IChat';

function generateInviteMessage(custom?: {
	isPublic?: boolean;
	onlyAdminsCanSend?: boolean;
}) {
	const creator = TestUtils.generateRandomUser();
	const users = [
		TestUtils.generateRandomUser(),
		TestUtils.generateRandomUser(),
	];
	const admins = [creator];
	const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
	return new InviteMessage(
		creator,
		`random-message-id-${TestUtils.generateNumber()}`,
		`random-chat-id-${TestUtils.generateNumber()}`,
		`random-chat-name-${TestUtils.generateNumber()}`,
		date,
		users,
		[TestUtils.generateRandomUser()],
		admins,
		date,
		custom?.isPublic || false,
		custom?.onlyAdminsCanSend || false,
		ChatType.NORMAL,
	);
}

describe('Test InviteMessage - toJson', () => {
	it('should create correct InviteMessage for internal context', function () {
		const inviteMessage = generateInviteMessage({
			isPublic: false,
			onlyAdminsCanSend: false,
		});
		const json = {
			sender: (inviteMessage.sender as User).toJson() as IUser,
			messageId: inviteMessage.messageId,
			chatId: inviteMessage.chatId,
			chatName: inviteMessage.chatName,
			messageDate: inviteMessage.messageDate,
			chatContent: ChatContentTypes.INVITE,
			newMembers: inviteMessage.newMembers.map((member: User) => {
				return member.toJson() as IUser;
			}),
			existingMembers: inviteMessage.existingMembers.map((member: User) => {
				return member.toJson() as IUser;
			}),
			admins: inviteMessage.admins.map((admin: User) => {
				return admin.toJson() as IUser;
			}),
			joinDate: inviteMessage.joinDate,
			isPublic: false,
			onlyAdminsCanSend: false,
			chatType: ChatType.NORMAL,
		};
		expect(inviteMessage.toJson(false, JsonContext.INTERNAL)).toEqual(
			json as any,
		);
	});

	it('should create correct InviteMessage for database context', function () {
		const inviteMessage = generateInviteMessage({
			isPublic: true,
			onlyAdminsCanSend: false,
		});
		const json: any = {
			sender: inviteMessage.sender.email,
			messageId: inviteMessage.messageId,
			chatId: inviteMessage.chatId,
			chatName: inviteMessage.chatName,
			messageDate: inviteMessage.messageDate,
			chatContent: ChatContentTypes.INVITE,
			newMembers: inviteMessage.newMembers.map((member: User) => {
				return member.email;
			}),
			existingMembers: inviteMessage.existingMembers.map((member: User) => {
				return member.email;
			}),
			admins: inviteMessage.admins.map((admin: User) => {
				return admin.email;
			}),
			joinDate: inviteMessage.joinDate,
			isPublic: true,
			onlyAdminsCanSend: false,
			chatType: ChatType.NORMAL,
		};
		expect(inviteMessage.toJson(false, JsonContext.DATABASE)).toEqual(json);
	});
});

describe('Test InviteMessage - fromJson', () => {
	it('should load from json object', function () {
		const inviteMessage = generateInviteMessage({
			isPublic: true,
			onlyAdminsCanSend: false,
		});
		const json = {
			sender: (inviteMessage.sender as User).toJson() as IUser,
			messageId: inviteMessage.messageId,
			chatId: inviteMessage.chatId,
			chatName: inviteMessage.chatName,
			messageDate: inviteMessage.messageDate,
			chatContent: ChatContentTypes.INVITE,
			newMembers: inviteMessage.newMembers.map((member: User) => {
				return member.toJson() as IUser;
			}),
			existingMembers: inviteMessage.existingMembers.map((member: User) => {
				return member.toJson() as IUser;
			}),
			admins: inviteMessage.admins.map((admin: User) => {
				return admin.toJson() as IUser;
			}),
			joinDate: inviteMessage.joinDate,
			isPublic: true,
			onlyAdminsCanSend: false,
			chatType: ChatType.NORMAL,
		};
		expect(inviteMessage).toEqual(InviteMessage.fromJson(json as any));
	});

	it('should fail because messageId is undefined', function (done) {
		const creator = TestUtils.generateRandomUser();
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		new Promise(() =>
			InviteMessage.fromJson({
				sender: creator,
				//@ts-ignore
				messageId: undefined as any,
				chatId: `random-chat-id-${TestUtils.generateNumber()}`,
				messageDate: date,
				chatContent: ChatContentTypes.INVITE,
				chatName: '',
				newMembers: [
					creator,
					TestUtils.generateRandomUser(),
					TestUtils.generateRandomUser(),
				].map((member: User) => {
					return member.toJson() as IUser;
				}),
				existingMembers: [],
				admins: [creator],
				joinDate: date,
				isPublic: false,
				onlyAdminsCanSend: false,
				chatType: ChatType.NORMAL,
			} as JsonInviteMessage),
		).catch((error) => {
			expect(error).toEqual(new MissingData('messageId'));
			done();
		});
	});
});

describe('Test InviteMessage - save', () => {
	it('should save successfully', function (done) {
		const inviteMessage = generateInviteMessage();
		inviteMessage.save().then((result) => {
			expect(result).toBe(inviteMessage.messageId);
			done();
		});
	});
});

describe('Test InviteMessage - load', () => {
	it('should load successfully', function (done) {
		const inviteMessage = generateInviteMessage();
		return Promise.resolve()
			.then(() => {
				return (inviteMessage.sender as User).save();
			})
			.then((senderSaveResult: any) => {
				expect(senderSaveResult).toBe(inviteMessage.sender.email);
				return Promise.all(
					inviteMessage.newMembers.map((member: User) => {
						return member.save();
					}),
				);
			})
			.then((savedMembers) => {
				expect(savedMembers.length).toBe(2);
				return Promise.all(
					inviteMessage.existingMembers.map((member: User) => {
						return member.save();
					}),
				);
			})
			.then((savedExistingMembers) => {
				expect(savedExistingMembers.length).toBe(1);
				return inviteMessage.save();
			})
			.then((result) => {
				expect(result).toBe(inviteMessage.messageId);
				return InviteMessage.load(inviteMessage.messageId);
			})
			.then((loadedInviteMessage: InviteMessage) => {
				expect(inviteMessage).toEqual(loadedInviteMessage);
				done();
			});
	});

	it('should failed to load because there is no data', (done) => {
		const messageId = 'this-id-is-not-in-the-database';
		return InviteMessage.load(messageId).catch((err) => {
			expect(err).toEqual(new NotFoundInDatabase(messageId));
			done();
		});
	});
});

describe('Test InviteMessage - delete', () => {
	it('should delete successfully', (done) => {
		const inviteMessage = generateInviteMessage();
		return Promise.resolve()
			.then(() => {
				return Promise.all(
					inviteMessage.newMembers.map((member: User) => {
						return member.save();
					}),
				);
			})
			.then((savedMembers) => {
				expect(savedMembers.length).toBe(2);
				return inviteMessage.save();
			})
			.then((result) => {
				expect(result).toBe(inviteMessage.messageId);
				return inviteMessage.delete();
			})
			.then((result) => {
				expect(result).toBe(1);
				done();
			});
	});

	it('should fail because there is no data', (done) => {
		const messages = generateInviteMessage();
		messages.delete().then((result) => {
			expect(result).toBe(0);
			done();
		});
	});
});

describe('Test InviteMessage - fromEmail', () => {
	it('should create InviteMessage successfully from email', (done) => {
		const from: User = TestUtils.generateRandomUser();
		const to: User[] = [
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
		];
		const newMembers: User[] = [to[2]];
		let newMemberString = '';
		newMembers.forEach((member) => {
			newMemberString += `${member.email}, `;
		});
		newMemberString = newMemberString.substr(0, newMemberString.length - 2);
		const email = new EMail(
			{
				from: from.email,
				to: to.map((user) => {
					return user.email;
				}),
				subject: '',
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: `random-message-id-${TestUtils.generateNumber()}`,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				additionalHeaderFields: {
					chatName: 'a-chat-name',
					admins: from.email,
					newMembers: newMemberString,
					userAgent: EMailDefaults.userAgent,
					isPublic: 'true',
					onlyAdminsCanSend: 'false',
					chatType: ChatType.NORMAL,
				},
				chatId: `random-chat-id-${TestUtils.generateNumber()}`,
				chatVersion: EMailDefaults.chatVersion,
				chatContent: ChatContentTypes.INVITE,
			},
			'nothing special',
		);
		const inviteMessage = new InviteMessage(
			from,
			email.messageId,
			email.chatId,
			email.additionalHeaderFields.chatName,
			email.date,
			newMembers,
			[to[0], to[1], from],
			[from],
			email.date,
			true,
			false,
			email.additionalHeaderFields.chatType,
		);

		Promise.all(
			newMembers.map((user) => {
				return user.save();
			}),
		)
			.then(() => {
				return from.save();
			})
			.then(() => {
				return InviteMessage.fromEmail(email);
			})
			.then((resultInviteMessage: InviteMessage) => {
				expect(resultInviteMessage.equals(inviteMessage)).toBeTrue();
				done();
			});
	});

	it('should create InviteMessage successfully from email without chatType', (done) => {
		const from: User = TestUtils.generateRandomUser();
		const to: User[] = [
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
			TestUtils.generateRandomUser(),
		];
		const newMembers: User[] = [to[2]];
		let newMemberString = '';
		newMembers.forEach((member) => {
			newMemberString += `${member.email}, `;
		});
		newMemberString = newMemberString.substr(0, newMemberString.length - 2);
		const email = new EMail(
			{
				from: from.email,
				to: to.map((user) => {
					return user.email;
				}),
				subject: '',
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: `random-message-id-${TestUtils.generateNumber()}`,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				additionalHeaderFields: {
					chatName: 'a-chat-name',
					admins: from.email,
					newMembers: newMemberString,
					userAgent: EMailDefaults.userAgent,
					isPublic: 'true',
					onlyAdminsCanSend: 'false',
				},
				chatId: `random-chat-id-${TestUtils.generateNumber()}`,
				chatVersion: EMailDefaults.chatVersion,
				chatContent: ChatContentTypes.INVITE,
			},
			'nothing special',
		);
		const inviteMessage = new InviteMessage(
			from,
			email.messageId,
			email.chatId,
			email.additionalHeaderFields.chatName,
			email.date,
			newMembers,
			[to[0], to[1], from],
			[from],
			email.date,
			true,
			false,
			email.additionalHeaderFields.NORMAL,
		);

		Promise.all(
			newMembers.map((user) => {
				return user.save();
			}),
		)
			.then(() => {
				return from.save();
			})
			.then(() => {
				return InviteMessage.fromEmail(email);
			})
			.then((resultInviteMessage: InviteMessage) => {
				expect(resultInviteMessage.equals(inviteMessage)).toBeTrue();
				done();
			});
	});

	it('should create InviteMessage fail with WrongEmailType', (done) => {
		const email = TestUtils.generateEMail();
		InviteMessage.fromEmail(email).catch((err) => {
			expect(err).toEqual(new WrongEmailType(email, ChatContentTypes.INVITE));
			done();
		});
	});
});

describe('Test InviteMessage - toEmail', () => {
	it('should create an email successfully', () => {
		const chat = TestUtils.generateChat(0, 2, false, false, ChatType.AD);
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		const message = new InviteMessage(
			chat.admins[0],
			EMail.genId(),
			chat.id,
			chat.name,
			date,
			[TestUtils.generateRandomUser()],
			chat.participants,
			chat.admins,
			date,
			chat.isPublic,
			chat.onlyAdminsCanSend,
			ChatType.AD,
		);
		const email = new EMail(
			{
				from: message.sender.email,
				to: chat.participants
					.concat(message.newMembers)
					.map((member: User) => member.email)
					.filter((email: string) => email !== message.sender.email),
				subject: `${message.sender.email} invites you to chat`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: message.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				additionalHeaderFields: {
					chatName: chat.name,
					admins: chat.admins.map((admin) => admin.email),
					userAgent: EMailDefaults.userAgent,
					newMembers: message.newMembers.map((member) => member.email),
					isPublic: `${chat.isPublic}`,
					onlyAdminsCanSend: `${chat.onlyAdminsCanSend}`,
					chatType: ChatType.AD,
				},
				chatVersion: EMailDefaults.chatVersion,
				chatId: chat.id,
				chatContent: ChatContentTypes.INVITE,
			},
			`${message.sender.email} invites you to chat with him!`,
		);
		expect(message.toEmail(chat)).toEqual(email);
	});
});

describe('Test InviteMessage - toString', () => {
	it('should return expected string', () => {
		const message = generateInviteMessage();
		const expectedString = `${message.sender.nickname} invites ${message.newMembers[0].nickname}, ${message.newMembers[1].nickname} to this chat!`;
		expect(expectedString).toBe(message.toString());
	});
});

describe('Test InviteMessage - fromChat', () => {
	it('should create correctly', () => {
		const chat = TestUtils.generateChat(2, 3);
		const newMember = [TestUtils.generateRandomUser()];
		const invite = InviteMessage.fromChat(chat, chat.admins[0], newMember);
		const expectedInvite = new InviteMessage(
			chat.admins[0],
			invite.messageId,
			chat.id,
			chat.name,
			invite.messageDate,
			newMember,
			chat.participants,
			chat.admins,
			invite.joinDate,
			chat.isPublic,
			chat.onlyAdminsCanSend,
			chat.type,
		);
		expect(invite).toEqual(expectedInvite);
		expect(invite.equals(expectedInvite)).toBeTrue();
	});
});
