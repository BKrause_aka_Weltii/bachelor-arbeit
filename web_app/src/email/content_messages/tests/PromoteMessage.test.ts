import { TestUtils } from '../../tests/TestUtils';
import { JsonContext } from '../../interfaces/json-rebuildable';
import generateRandomUser = TestUtils.generateRandomUser;
import generateNumber = TestUtils.generateNumber;
import { User } from '../../User';
import { NotFoundInDatabase } from '../../NotFoundInDatabase';
import { EMail } from '../../EMail';
import { EMailDefaults } from '../../../Config';
import { ChatContentTypes } from '../../ChatContentTypes';
import { Chat } from '../../Chat';
import { WrongEmailType } from '../../WrongEmailType';
import generateChat = TestUtils.generateChat;
import { Utils } from '../../utils';
import { NotUseableInJsonContextError } from '../../../email/exceptions/NotUseableInJsonContextError';
import { PromoteMessage } from '../PromoteMessage';

function generatePromoteMessage(custom?: {
	sender?: User;
	chatId?: string;
	toAdmin?: boolean;
	userEmail?: string;
}) {
	return new PromoteMessage(
		custom?.sender || generateRandomUser(),
		`message-id-${generateNumber()}`,
		custom?.chatId || `chat-id-${generateNumber()}`,
		Utils.dateToTimeString(Utils.getCurrentDateTime()),
		custom?.userEmail || generateRandomUser().email,
		custom?.toAdmin || false,
	);
}

describe('Test PromoteMessage - toJson', () => {
	it('should return correct json for internal context', function () {
		const message = generatePromoteMessage();
		const json = message.toJson(false, JsonContext.INTERNAL);
		const expectedJson = {
			sender: {
				email: message.sender.email,
				nickname: message.sender.nickname,
			},
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			promoteUserEmail: message.promoteUserEmail,
			toAdmin: message.toAdmin,
			chatContent: ChatContentTypes.PROMOTE,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should return correct json for database context', function () {
		const message = generatePromoteMessage();
		const json = message.toJson(false, JsonContext.DATABASE);
		const expectedJson = {
			sender: message.sender.email,
			messageId: message.messageId,
			chatId: message.chatId,
			messageDate: message.messageDate,
			promoteUserEmail: message.promoteUserEmail,
			toAdmin: message.toAdmin,
			chatContent: ChatContentTypes.PROMOTE,
		};
		expect(json).toEqual(expectedJson);
	});

	it('should throw error on trying to export for proxy context', function () {
		const message = generatePromoteMessage();
		try {
			message.toJson(false, JsonContext.PROXY);
		} catch (e) {
			expect(e).toEqual(
				new NotUseableInJsonContextError(
					'PromoteMessage.toJson',
					JsonContext.PROXY,
				),
			);
		}
	});
});

describe('Test PromoteMessage - fromJson', function () {
	it('should load from json', function () {
		const message = generatePromoteMessage();
		expect(PromoteMessage.fromJson(message.toJson())).toEqual(message);
		expect(
			PromoteMessage.fromJson(message.toJson()).equals(message),
		).toBeTrue();
	});
});

describe('Test PromoteMessage - save', () => {
	it('should save successfully', function (done) {
		const message = generatePromoteMessage();
		message.save().then((saveResult) => {
			expect(saveResult).toBe(message.messageId);
			done();
		});
	});
});

describe('Test PromoteMessage - load', () => {
	it('should load successfully', function (done) {
		const message = generatePromoteMessage();
		(message.sender as User)
			.save()
			.then((saveResult) => {
				expect(saveResult).toBe(message.sender.email);
				return message.save();
			})
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return PromoteMessage.load(message.messageId);
			})
			.then((resultMessage: PromoteMessage) => {
				expect(message.equals(resultMessage)).toBeTrue();
				done();
			});
	});

	it('should fail because there is no message', function (done) {
		const message = generatePromoteMessage();
		PromoteMessage.load(message.messageId).catch((err) => {
			expect(err).toEqual(
				new NotFoundInDatabase(`PromoteMessage ${message.messageId}`),
			);
			done();
		});
	});
});

describe('Test PromoteMessage - delete', () => {
	it('should delete successfully', function (done) {
		const message = generatePromoteMessage();
		message
			.save()
			.then((messageSaveResult) => {
				expect(messageSaveResult).toBe(message.messageId);
				return message.delete();
			})
			.then((deleteResult) => {
				expect(deleteResult).toBe(1);
				done();
			});
	});

	it('should delete no data because the message is not saved', function (done) {
		const message = generatePromoteMessage();
		return message.delete().then((deleteResult) => {
			expect(deleteResult).toBe(0);
			done();
		});
	});
});

describe('Test PromoteMessage - fromEmail', () => {
	it('should create a PromoteMessage from an email successfully', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.PROMOTE,
				additionalHeaderFields: {
					promoteUserEmail: to[0].email,
					toAdmin: 'true',
				},
			},
			`${sender.email} promotes ${to[0].email} to admin`,
		);
		const expectedPromoteMessage = new PromoteMessage(
			sender,
			email.messageId,
			email.chatId,
			email.date,
			to[0].email,
			true,
		);
		PromoteMessage.fromEmail(email).then((resultMessage: PromoteMessage) => {
			expect(resultMessage.equals(expectedPromoteMessage)).toBeTrue();
			done();
		});
	});

	it('should failed because the email has the wrong chatContent', function (done) {
		const sender = generateRandomUser();
		sender.nickname = sender.email;
		const to = [generateRandomUser(), generateRandomUser()];
		const email = new EMail(
			{
				from: sender.email,
				to: to.map((member) => member.email),
				subject: `${sender.email} sends a message`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: EMail.genId(),
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: Chat.genId(),
				chatContent: ChatContentTypes.INVITE,
			},
			'A body for testing',
		);
		PromoteMessage.fromEmail(email).catch((err) => {
			expect(err).toEqual(new WrongEmailType(email, ChatContentTypes.PROMOTE));
			done();
		});
	});
});

describe('Test PromoteMessage - toEMail', () => {
	it('should generates a EMail from PromoteMessage', function () {
		const chat = generateChat(1, 2);
		const message = generatePromoteMessage({
			sender: chat.participants[0],
			chatId: chat.id,
			userEmail: chat.participants[0],
			toAdmin: true,
		});
		const expectedEmail = new EMail(
			{
				from: message.sender.email,
				to: chat.participants
					.map((member: any) => member.email)
					.filter((email: any) => email !== message.sender.email),
				subject: `${message.sender.email} sends a message`,
				date: message.messageDate,
				messageId: message.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: chat.id,
				chatContent: ChatContentTypes.PROMOTE,
				additionalHeaderFields: {
					promoteUserEmail: message.promoteUserEmail,
					toAdmin: 'true',
				},
			},
			`${message.sender.email} promotes ${message.promoteUserEmail} to admin`,
		);
		expect(message.toEmail(chat)).toEqual(expectedEmail);
	});
});
