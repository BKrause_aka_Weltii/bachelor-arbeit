import { AMessage } from '../dexie_interfaces/AMessage';
import { User } from '../User';
import { ChatContentTypes } from '../ChatContentTypes';
import { Chat } from '../Chat';
import { EMailDefaults } from '../../Config';
import { EMail } from '../EMail';
import { JsonContext } from '../interfaces/json-rebuildable';
import { NotUseableInJsonContextError } from '../exceptions/NotUseableInJsonContextError';
import { Utils } from '../utils';
import { DB } from '../db';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { WrongEmailType } from '../WrongEmailType';

export class NewsMessage extends AMessage {
	constructor(
		sender: User,
		messageId: string,
		chatId: string,
		messageDate: string,
		readonly title: string,
		readonly message: string,
	) {
		super(sender, messageId, chatId, messageDate, ChatContentTypes.NEWS);
	}

	toEmail(chat: Chat): EMail {
		return new EMail(
			{
				from: this.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== this.sender.email),
				subject: this.title,
				date: this.messageDate,
				messageId: this.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: this.chatId,
				chatContent: ChatContentTypes.NEWS,
			},
			this.message,
		);
	}

	toString(): string {
		return `${this.title}\n${this.message}`;
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | any {
		let json;
		switch (context) {
			case JsonContext.INTERNAL:
				json = {
					sender: (this.sender as User).toJson(false, context),
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					title: this.title,
					message: this.message,
					chatContent: ChatContentTypes.NEWS,
				};
				break;
			case JsonContext.DATABASE:
				json = {
					sender: this.sender.email,
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					title: this.title,
					message: this.message,
					chatContent: ChatContentTypes.NEWS,
				};
				break;
			case JsonContext.PROXY:
				throw new NotUseableInJsonContextError(
					'NewsMessage.toJson',
					JsonContext.PROXY,
				);
		}
		return asString ? JSON.stringify(json) : json;
	}

	static fromJson(json: string | any) {
		json = Utils.getAsObject(json);
		return new NewsMessage(
			User.fromJson(Utils.getDelAndThrow('sender', json)),
			Utils.getDelAndThrow('messageId', json),
			Utils.getDelAndThrow('chatId', json),
			Utils.getDelAndThrow('messageDate', json),
			Utils.getDelAndThrow('title', json),
			Utils.getDelAndThrow('message', json),
		);
	}

	static load(messageId: string) {
		let iMessage: AMessage;
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.messages.where('messageId')
					.equals(messageId)
					.first();
			})
			.then((iMessageResult: AMessage | undefined) => {
				if (!iMessageResult) {
					throw new NotFoundInDatabase(`NewsMessage ${messageId}`);
				}
				if (iMessageResult?.chatContent != ChatContentTypes.NEWS) {
					throw new Error(
						`Json has the wrong type. Expected ${ChatContentTypes.NEWS} instead of ${iMessageResult?.chatContent}`,
					);
				}
				iMessage = iMessageResult;
				return User.load(iMessageResult.sender as any);
			})
			.then((sender: User) => {
				if (!sender) {
					throw 'sender is undefined!';
				}
				return new NewsMessage(
					sender,
					iMessage.messageId,
					iMessage.chatId,
					iMessage.messageDate,
					(iMessage as any).title,
					(iMessage as any).message,
				);
			})
			.catch((err) => {
				console.error('NewsMessage - load', err);
				throw err;
			});
	}

	static fromEmail(email: EMail): Promise<NewsMessage> {
		return Promise.resolve()
			.then(() => {
				if (email.chatContent !== ChatContentTypes.NEWS) {
					throw new WrongEmailType(email, ChatContentTypes.NEWS);
				}
				return User.loadOrCreateNew(email.from);
			})
			.then((sender: User) => {
				return new NewsMessage(
					sender,
					email.messageId,
					email.chatId,
					email.date,
					email.subject,
					email.body,
				);
			});
	}
}
