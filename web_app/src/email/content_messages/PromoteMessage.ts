import { AMessage } from '../dexie_interfaces/AMessage';
import { JsonRebuildable, JsonContext } from '../interfaces/json-rebuildable';
import { IFromEmail } from '../interfaces/IFromEmail';
import { User } from '../User';
import { ChatContentTypes } from '../ChatContentTypes';
import { Chat } from '../Chat';
import { EMail } from '../EMail';
import { EMailDefaults } from '../../Config';
import { NotUseableInJsonContextError } from '../exceptions/NotUseableInJsonContextError';
import { DB } from '../db';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { WrongEmailType } from '../WrongEmailType';
import { Utils } from '../utils';

export class PromoteMessage
	extends AMessage
	implements JsonRebuildable, IFromEmail {
	constructor(
		sender: User,
		messageId: string,
		chatId: string,
		messageDate: string,
		readonly promoteUserEmail: string,
		readonly toAdmin: boolean, // is true, the promoteEmail user will be an admin
	) {
		super(sender, messageId, chatId, messageDate, ChatContentTypes.PROMOTE);
	}

	equals(other: PromoteMessage): boolean {
		return (
			(this.sender as User).equals(other.sender as User) &&
			this.messageId === other.messageId &&
			this.chatId === other.chatId &&
			this.messageDate === other.messageDate &&
			this.promoteUserEmail === other.promoteUserEmail &&
			this.toAdmin === other.toAdmin
		);
	}

	toEmail(chat: Chat): EMail {
		return new EMail(
			{
				from: this.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== this.sender.email),
				subject: `${this.sender.email} sends a message`,
				date: this.messageDate,
				messageId: this.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: this.chatId,
				chatContent: ChatContentTypes.PROMOTE,
				additionalHeaderFields: {
					promoteUserEmail: this.promoteUserEmail,
					toAdmin: `${this.toAdmin}`,
				},
			},
			this.toString(),
		);
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | any {
		let json;
		switch (context) {
			case JsonContext.INTERNAL:
				json = {
					sender: (this.sender as User).toJson(false, context),
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					promoteUserEmail: this.promoteUserEmail,
					toAdmin: this.toAdmin,
					chatContent: ChatContentTypes.PROMOTE,
				};
				break;
			case JsonContext.DATABASE:
				json = {
					sender: this.sender.email,
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					promoteUserEmail: this.promoteUserEmail,
					toAdmin: this.toAdmin,
					chatContent: ChatContentTypes.PROMOTE,
				};
				break;
			case JsonContext.PROXY:
				throw new NotUseableInJsonContextError(
					'PromoteMessage.toJson',
					JsonContext.PROXY,
				);
		}
		return asString ? JSON.stringify(json) : json;
	}

	toString(): string {
		return `${this.sender.email} promotes ${this.promoteUserEmail} to ${
			this.toAdmin ? 'admin' : 'member'
		}`;
	}

	static fromJson(json: string | any) {
		json = Utils.getAsObject(json);
		return new PromoteMessage(
			User.fromJson(json.sender),
			json.messageId,
			json.chatId,
			json.messageDate,
			json.promoteUserEmail,
			json.toAdmin,
		);
	}

	static load(messageId: string): Promise<PromoteMessage> {
		let iMessage: AMessage;
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.messages.where('messageId')
					.equals(messageId)
					.first();
			})
			.then((iMessageResult: AMessage | undefined) => {
				if (!iMessageResult) {
					throw new NotFoundInDatabase(`PromoteMessage ${messageId}`);
				}
				if (iMessageResult?.chatContent != ChatContentTypes.PROMOTE) {
					throw new Error(
						`Json has the wrong type. Expected ${ChatContentTypes.PROMOTE} instead of ${iMessageResult?.chatContent}`,
					);
				}
				iMessage = iMessageResult;
				return User.load(iMessageResult.sender as any);
			})
			.then((sender: User) => {
				if (!sender) {
					throw 'sender is undefined!';
				}
				return new PromoteMessage(
					sender,
					iMessage.messageId,
					iMessage.chatId,
					iMessage.messageDate,
					(iMessage as any).promoteUserEmail,
					(iMessage as any).toAdmin,
				);
			})
			.catch((err) => {
				console.error('PromoteMessage - load', err);
				throw err;
			});
	}

	static fromEmail(email: EMail): Promise<PromoteMessage> {
		return Promise.resolve()
			.then(() => {
				if (email.chatContent !== ChatContentTypes.PROMOTE) {
					throw new WrongEmailType(email, ChatContentTypes.PROMOTE);
				}
				return User.loadOrCreateNew(email.from);
			})
			.then((sender: User) => {
				return new PromoteMessage(
					sender,
					email.messageId,
					email.chatId,
					email.date,
					email.additionalHeaderFields.promoteUserEmail,
					email.additionalHeaderFields.toAdmin === 'true',
				);
			});
	}
}
