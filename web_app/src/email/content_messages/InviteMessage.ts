import { User } from '../User';
import { AMessage } from '../dexie_interfaces/AMessage';
import { JsonContext, JsonRebuildable } from '../interfaces/json-rebuildable';
import { IFromEmail } from '../interfaces/IFromEmail';
import { EMail } from '../EMail';
import { ChatContentTypes } from '../ChatContentTypes';
import { IUser } from '../dexie_interfaces/IUser';
import { DB } from '../db';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { Utils } from '../utils';
import { WrongEmailType } from '../WrongEmailType';
import { EMailDefaults } from '../../Config';
import { Chat } from '../Chat';
import { EMailProxy } from '../EMailProxy';
import { ChatType } from '../dexie_interfaces/IChat';

export type JsonInviteMessage = {
	sender: IUser;
	chatContent: string; // will be always invite
	messageId: string;
	messageDate: string;
	chatId: string;
	chatName: string;
	newMembers: IUser[];
	existingMembers: IUser[];
	admins: IUser[];
	joinDate: string;
	isPublic: boolean;
	onlyAdminsCanSend: boolean;
	chatType: ChatType;
};

export class InviteMessage
	extends AMessage
	implements JsonRebuildable, IFromEmail {
	constructor(
		sender: User,
		messageId: string,
		chatId: string,
		readonly chatName: string,
		messageDate: string,
		readonly newMembers: User[],
		readonly existingMembers: User[],
		readonly admins: User[],
		readonly joinDate: string,
		readonly isPublic: boolean,
		readonly onlyAdminsCanSend: boolean,
		readonly chatType: ChatType,
	) {
		super(sender, messageId, chatId, messageDate, ChatContentTypes.INVITE);
	}

	equals(other: InviteMessage): boolean {
		return (
			(this.sender as User).equals(other.sender as User) &&
			this.messageId === other.messageId &&
			this.chatId === other.chatId &&
			this.chatName === other.chatName &&
			this.messageDate === other.messageDate &&
			this.newMembers.length === other.newMembers.length &&
			this.existingMembers.length === other.existingMembers.length &&
			this.admins.length === other.admins.length &&
			this.joinDate === other.joinDate &&
			this.isPublic === other.isPublic &&
			this.onlyAdminsCanSend === other.onlyAdminsCanSend &&
			this.chatType === other.chatType
		);
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | JsonInviteMessage {
		let json;
		switch (context) {
			case JsonContext.INTERNAL:
				json = {
					sender: (this.sender as User).toJson(false, context) as IUser,
					messageId: this.messageId,
					chatId: this.chatId,
					chatName: this.chatName,
					messageDate: this.messageDate,
					chatContent: ChatContentTypes.INVITE,
					newMembers: this.newMembers.map((member: User) => {
						return member.toJson(false, context) as IUser;
					}),
					existingMembers: this.existingMembers.map((member: User) => {
						return member.toJson() as IUser;
					}),
					admins: this.admins.map((admin: User) => {
						return admin.toJson(false, context) as IUser;
					}),
					joinDate: this.joinDate,
					isPublic: this.isPublic,
					onlyAdminsCanSend: this.onlyAdminsCanSend,
					chatType: ChatType.NORMAL,
				};
				break;
			case JsonContext.DATABASE:
				json = {
					sender: this.sender.email,
					messageId: this.messageId,
					chatId: this.chatId,
					chatName: this.chatName,
					messageDate: this.messageDate,
					chatContent: ChatContentTypes.INVITE,
					newMembers: this.newMembers.map((member: User) => {
						return member.email;
					}),
					existingMembers: this.existingMembers.map((member: User) => {
						return member.email;
					}),
					admins: this.admins.map((admin: User) => {
						return admin.email;
					}),
					joinDate: this.joinDate,
					isPublic: this.isPublic,
					onlyAdminsCanSend: this.onlyAdminsCanSend,
					chatType: ChatType.NORMAL,
				};
				break;
			case JsonContext.PROXY:
				throw new Error('InviteMessage cannot use for context proxy!');
		}
		return asString ? JSON.stringify(json) : (json as any);
	}

	toString() {
		let members = '';
		this.newMembers.forEach(
			(member) => (members += `${member.nickname || member.email}, `),
		);
		return `${
			this.sender.nickname || this.sender.email
		} invites ${members.substring(0, members.length - 2)} to this chat!`;
	}

	toEmail(chat?: Chat): EMail {
		return new EMail(
			{
				from: this.sender.email,
				to: chat?.participants
					.concat(this.newMembers)
					.map((member: User) => {
						return member.email;
					})
					.filter((email: string) => {
						return email !== this.sender.email;
					}) as string[],
				subject: `${this.sender.email} invites you to chat`,
				date: Utils.dateToTimeString(Utils.getCurrentDateTime()),
				messageId: this.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				additionalHeaderFields: {
					chatName: this.chatName,
					admins: this.admins.map((admin) => admin.email),
					userAgent: EMailDefaults.userAgent,
					newMembers: this.newMembers.map((member) => member.email),
					isPublic: `${this.isPublic}`,
					onlyAdminsCanSend: `${this.onlyAdminsCanSend}`,
					chatType: this.chatType,
				},
				chatVersion: EMailDefaults.chatVersion,
				chatId: this.chatId,
				chatContent: ChatContentTypes.INVITE,
			},
			`${this.sender.email} invites you to chat with him!`,
		);
	}

	static fromChat(
		chat: Chat,
		sender: User,
		inviteNewMember: User[],
	): InviteMessage {
		const date = Utils.dateToTimeString(Utils.getCurrentDateTime());
		return new InviteMessage(
			sender,
			EMail.genId(
				EMailProxy.getInstance().provider?.baseUrl || 'no-provider-is-chosen',
			),
			chat.id,
			chat.name,
			date,
			inviteNewMember,
			chat.participants,
			chat.admins,
			date,
			chat.isPublic,
			chat.onlyAdminsCanSend,
			chat.type,
		);
	}

	static fromEmail(email: EMail): Promise<InviteMessage> {
		let from: User;
		let newMembers: User[];
		let admins: User[];
		return Promise.resolve()
			.then(async () => {
				if (email.chatContent != ChatContentTypes.INVITE) {
					throw new WrongEmailType(email, ChatContentTypes.INVITE);
				}
				return User.loadOrCreateNew(email.from);
			})
			.then((fromResult: User) => {
				from = fromResult;
				return Promise.all(
					(email.additionalHeaderFields.newMembers.split(',') as string[]).map(
						(email: string) => {
							return User.loadOrCreateNew(email.trim());
						},
					),
				);
			})
			.then((toResult: User[]) => {
				newMembers = toResult;
				return Promise.all(
					email.additionalHeaderFields.admins
						.split(',')
						.map((email: string) => {
							return User.loadOrCreateNew(email.trim());
						}),
				);
			})
			.then((adminResult: any[]) => {
				admins = adminResult;
				const newMemberEmails = newMembers.map((member) => member.email);
				return Promise.all(
					email.to
						.filter((email) => {
							return !newMemberEmails.includes(email);
						})
						.map((user) => User.loadOrCreateNew(user)),
				);
			})
			.then((existingMembers: User[]) => {
				return new InviteMessage(
					from,
					email.messageId,
					email.chatId,
					email.additionalHeaderFields['chatName'],
					email.date,
					newMembers,
					existingMembers.concat([from]),
					admins,
					email.date,
					email.additionalHeaderFields['isPublic'] === 'true',
					email.additionalHeaderFields['onlyAdminsCanSend'] === 'true',
					email.additionalHeaderFields['chatType'],
				);
			});
	}

	static load(pk: string): Promise<InviteMessage> {
		let message: any = {};
		return Promise.resolve()
			.then(() => {
				return DB.getInstance().messages.where('messageId').equals(pk).first();
			})
			.then((rawIMessageResult: AMessage | undefined) => {
				if (!rawIMessageResult) {
					throw new NotFoundInDatabase(pk);
				}
				message = rawIMessageResult as AMessage;
				return Promise.all(
					message.newMembers.map((member: any) => {
						return User.load(member);
					}),
				);
			})
			.then((resultMembers) => {
				message.newMembers = resultMembers;
				return Promise.all(
					message.admins.map((member: any) => {
						return User.load(member);
					}),
				);
			})
			.then((adminResult) => {
				message.admins = adminResult;
				return User.load(message.sender);
			})
			.then((senderResult) => {
				message.sender = senderResult;
				return Promise.all(
					(message.existingMembers || []).map((member: any) => {
						return User.load(member).catch((err) => {
							console.error('Found Error on load user', err);
						});
					}),
				);
			})
			.then((existingMembersResult: any) => {
				return new InviteMessage(
					message.sender,
					message.messageId,
					message.chatId,
					message.chatName,
					message.messageDate,
					message.newMembers,
					existingMembersResult || [],
					message.admins,
					message.joinDate,
					message.isPublic,
					message.onlyAdminsCanSend,
					message.chatType,
				);
			})
			.catch((err) => {
				return Promise.reject(err);
			});
	}

	static fromJson(json: string | JsonInviteMessage): InviteMessage {
		if (typeof json === 'string') {
			json = JSON.parse(json) as JsonInviteMessage;
		}
		const sender = User.fromJson(Utils.getDelAndThrow('sender', json));
		const newMembers = Utils.getDelAndThrow('newMembers', json).map(
			(member: any) => {
				return User.fromJson(member);
			},
		);
		const admins = Utils.getDelAndThrow('admins', json).map((admin: any) => {
			return User.fromJson(admin);
		});
		const existingMembers = (
			Utils.getAndDel('existingMembers', json) || []
		).map((member: any) => {
			return User.fromJson(member);
		});
		return new InviteMessage(
			sender,
			Utils.getDelAndThrow('messageId', json),
			Utils.getDelAndThrow('chatId', json),
			Utils.getDelAndThrow('chatName', json),
			Utils.getDelAndThrow('messageDate', json),
			newMembers,
			existingMembers,
			admins,
			Utils.getDelAndThrow('joinDate', json),
			Utils.getDelAndThrow('isPublic', json),
			Utils.getDelAndThrow('onlyAdminsCanSend', json),
			Utils.getDelAndThrow('chatType', json),
		);
	}
}
