import { ChatContentTypes } from '../ChatContentTypes';
import { InviteMessage } from './InviteMessage';
import { AMessage } from '../dexie_interfaces/AMessage';
import { JsonRebuildable } from '../interfaces/json-rebuildable';
import { EMail } from '../EMail';
import { TextMessage } from './TextMessage';
import { DeleteMessage } from './DeleteMessage';
import { PromoteMessage } from './PromoteMessage';
import { JoinRequestMessage } from './JoinRequestMessage';
import { NewsMessage } from './NewsMessage';
import { NewAdMessage } from './NewAdMessage';
import { DeleteAdMessage } from './DeleteAdMessage';

export class UnknownChatContent extends Error {
	constructor(chatContent: ChatContentTypes | null | undefined) {
		super(`The ${chatContent} type won't be handled!`);
	}
}

export namespace MessageRebuilder {
	export function messageRebuilder(rawMessage: any): JsonRebuildable {
		switch (rawMessage.chatContent) {
			case ChatContentTypes.INVITE:
				return InviteMessage.fromJson(rawMessage);
			case ChatContentTypes.TEXT_MESSAGE:
				return TextMessage.fromJson(rawMessage);
			case ChatContentTypes.DELETE_USER_FROM_CHAT:
				return DeleteMessage.fromJson(rawMessage);
			case ChatContentTypes.PROMOTE:
				return PromoteMessage.fromJson(rawMessage);
			case ChatContentTypes.JOIN_REQUEST:
				return JoinRequestMessage.fromJson(rawMessage);
			case ChatContentTypes.NEWS:
				return NewsMessage.fromJson(rawMessage);
			case ChatContentTypes.NEW_AD:
				return NewAdMessage.fromJson(rawMessage);
			case ChatContentTypes.DELETE_AD:
				return DeleteAdMessage.fromJson(rawMessage);
			default:
				const err = new UnknownChatContent(rawMessage.chatContent);
				throw err;
		}
	}

	export function messageLoader(type: ChatContentTypes, id: string) {
		switch (type) {
			case ChatContentTypes.INVITE:
				return InviteMessage.load(id);
			case ChatContentTypes.TEXT_MESSAGE:
				return TextMessage.load(id);
			case ChatContentTypes.DELETE_USER_FROM_CHAT:
				return DeleteMessage.load(id);
			case ChatContentTypes.PROMOTE:
				return PromoteMessage.load(id);
			case ChatContentTypes.JOIN_REQUEST:
				return JoinRequestMessage.load(id);
			case ChatContentTypes.NEWS:
				return NewsMessage.load(id);
			case ChatContentTypes.NEW_AD:
				return NewAdMessage.load(id);
			case ChatContentTypes.DELETE_AD:
				return DeleteAdMessage.load(id);
			default:
				const message = `ChatContent ${type} won't be handled in MesseageRebuilder.messageLoader!`;
				console.error(message);
				return Promise.reject(new Error(message));
		}
	}

	export function messageSaver(message: AMessage) {
		switch (message.chatContent) {
			case ChatContentTypes.INVITE:
				return (message as InviteMessage).save();
			case ChatContentTypes.TEXT_MESSAGE:
				return (message as TextMessage).save();
			case ChatContentTypes.DELETE_USER_FROM_CHAT:
				return (message as DeleteMessage).save();
			case ChatContentTypes.PROMOTE:
				return (message as PromoteMessage).save();
			case ChatContentTypes.JOIN_REQUEST:
				return (message as JoinRequestMessage).save();
			case ChatContentTypes.NEWS:
				return (message as NewsMessage).save();
			case ChatContentTypes.NEW_AD:
				return (message as NewAdMessage).save();
			case ChatContentTypes.DELETE_AD:
				return (message as DeleteAdMessage).save();
			default:
				const err = `ChatContent ${message.chatContent} is not handled in MesseageRebuilder.messageSaver!`;
				console.error(err);
				return Promise.reject(new Error(err));
		}
	}

	export function messageFromEmail(email: EMail): Promise<AMessage | null> {
		switch (email.chatContent) {
			case ChatContentTypes.INVITE:
				return InviteMessage.fromEmail(email) as Promise<AMessage>;
			case ChatContentTypes.TEXT_MESSAGE:
				return TextMessage.fromEmail(email) as Promise<AMessage>;
			case ChatContentTypes.DELETE_USER_FROM_CHAT:
				return DeleteMessage.fromEmail(email) as Promise<AMessage>;
			case ChatContentTypes.PROMOTE:
				return PromoteMessage.fromEmail(email) as Promise<AMessage>;
			case ChatContentTypes.JOIN_REQUEST:
				return JoinRequestMessage.fromEmail(email) as Promise<AMessage>;
			case ChatContentTypes.NEWS:
				return NewsMessage.fromEmail(email) as Promise<AMessage>;
			case ChatContentTypes.NEW_AD:
				return NewAdMessage.fromEmail(email) as Promise<AMessage>;
			case ChatContentTypes.DELETE_AD:
				return DeleteAdMessage.fromEmail(email) as Promise<AMessage>;
			default:
				const message = `ChatContent ${email.chatContent} of email ${email.messageId} won't be handled!`;
				console.error(message);
				return Promise.reject(new Error(message));
		}
	}
}
