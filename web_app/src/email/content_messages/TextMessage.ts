import { AMessage } from '../dexie_interfaces/AMessage';
import { JsonContext, JsonRebuildable } from '../interfaces/json-rebuildable';
import { IFromEmail } from '../interfaces/IFromEmail';
import { EMail } from '../EMail';
import { User } from '../User';
import { EMailDefaults } from '../../Config';
import { ChatContentTypes } from '../ChatContentTypes';
import { Chat } from '../Chat';
import { DB } from '../db';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { WrongEmailType } from '../WrongEmailType';
import { NotUseableInJsonContextError } from '../exceptions/NotUseableInJsonContextError';

export class TextMessage
	extends AMessage
	implements JsonRebuildable, IFromEmail {
	constructor(
		sender: User,
		messageId: string,
		chatId: string,
		messageDate: string,
		readonly message: string,
	) {
		super(
			sender,
			messageId,
			chatId,
			messageDate,
			ChatContentTypes.TEXT_MESSAGE,
		);
	}

	equals(other: TextMessage): boolean {
		return (
			(this.sender as User).equals(other.sender as User) &&
			this.messageId === other.messageId &&
			this.chatId === other.chatId &&
			this.messageDate === other.messageDate &&
			this.message === other.message
		);
	}

	toEmail(chat: Chat): EMail {
		const to = chat.participants
			.map((member) => member.email)
			.filter((email) => email !== this.sender.email);
		console.log('Textmessage. toEmail', to);
		return new EMail(
			{
				from: this.sender.email,
				to: to,
				subject: `${this.sender.email} sends a message`,
				date: this.messageDate,
				messageId: this.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: this.chatId,
				chatContent: ChatContentTypes.TEXT_MESSAGE,
			},
			this.message,
		);
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | any {
		let json;
		switch (context) {
			case JsonContext.INTERNAL:
				json = {
					sender: (this.sender as User).toJson(false, context),
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					message: this.message,
					chatContent: ChatContentTypes.TEXT_MESSAGE,
				};
				break;
			case JsonContext.DATABASE:
				json = {
					sender: this.sender.email,
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					message: this.message,
					chatContent: ChatContentTypes.TEXT_MESSAGE,
				};
				break;
			case JsonContext.PROXY:
				throw new NotUseableInJsonContextError(
					'TextMessage.toJson',
					JsonContext.PROXY,
				);
		}
		return asString ? JSON.stringify(json) : json;
	}

	toString(): string {
		return this.message;
	}

	static fromJson(json: string | any) {
		if (typeof json === 'string') {
			json = JSON.parse(json) as any;
		}
		return new TextMessage(
			User.fromJson(json.sender),
			json.messageId,
			json.chatId,
			json.messageDate,
			json.message,
		);
	}

	static load(messageId: string): Promise<TextMessage> {
		let iMessage: AMessage;
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.messages.where('messageId')
					.equals(messageId)
					.first();
			})
			.then((iMessageResult: AMessage | undefined) => {
				if (!iMessageResult) {
					throw new NotFoundInDatabase(`TextMessage ${messageId}`);
				}
				if (iMessageResult?.chatContent != ChatContentTypes.TEXT_MESSAGE) {
					throw new Error(
						`Json has the wrong type. Expected ${ChatContentTypes.TEXT_MESSAGE} instead of ${iMessageResult?.chatContent}`,
					);
				}
				iMessage = iMessageResult;
				return User.load(iMessageResult.sender as any);
			})
			.then((sender: User) => {
				if (!sender) {
					throw 'sender is undefined!';
				}
				return new TextMessage(
					sender,
					iMessage.messageId,
					iMessage.chatId,
					iMessage.messageDate,
					(iMessage as any).message,
				);
			})
			.catch((err) => {
				console.error('TextMessage - load', err);
				throw err;
			});
	}

	static fromEmail(email: EMail): Promise<TextMessage> {
		return Promise.resolve()
			.then(() => {
				if (email.chatContent !== ChatContentTypes.TEXT_MESSAGE) {
					throw new WrongEmailType(email, ChatContentTypes.TEXT_MESSAGE);
				}
				return User.loadOrCreateNew(email.from);
			})
			.then((sender: User) => {
				return new TextMessage(
					sender,
					email.messageId,
					email.chatId,
					email.date,
					email.body,
				);
			});
	}
}
