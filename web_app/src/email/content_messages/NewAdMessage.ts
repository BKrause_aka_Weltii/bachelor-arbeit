import { AMessage as AMessage } from '../dexie_interfaces/AMessage';
import { ChatContentTypes } from '../ChatContentTypes';
import { EMail } from '../EMail';
import { WrongEmailType } from '../WrongEmailType';
import { User } from '../User';
import { DB } from '../db';
import { JsonContext } from '../interfaces/json-rebuildable';
import { NotUseableInJsonContextError } from '../exceptions/NotUseableInJsonContextError';
import { Chat } from '../Chat';
import { EMailDefaults } from '../../Config';
import { NotFoundInDatabase } from '../NotFoundInDatabase';
import { Utils } from '../utils';
import { PriceType } from '../dexie_interfaces/IAd';

export class NewAdMessage extends AMessage {
	constructor(
		sender: User,
		messageId: string,
		chatId: string,
		messageDate: string,
		readonly adId: string,
		readonly title: string,
		readonly description: string,
		readonly priceType: PriceType,
		readonly priceAmount: string,
	) {
		super(sender, messageId, chatId, messageDate, ChatContentTypes.NEW_AD);
	}

	equals(other: NewAdMessage): boolean {
		return (
			(this.sender as User).equals(other.sender as User) &&
			this.messageId === other.messageId &&
			this.chatId === other.chatId &&
			this.messageDate === other.messageDate &&
			this.adId === other.adId &&
			this.title === other.title &&
			this.description === other.description &&
			this.priceType === other.priceType &&
			this.priceAmount === other.priceAmount
		);
	}

	toEmail(chat: Chat): EMail {
		return new EMail(
			{
				from: this.sender.email,
				to: chat.participants
					.map((member) => member.email)
					.filter((email) => email !== this.sender.email),
				subject: `${this.sender.email} creates (or change) a ad - ${this.title}`,
				date: this.messageDate,
				messageId: this.messageId,
				mimeVersion: EMailDefaults.mimeVersion,
				contentType: EMailDefaults.contentType,
				chatVersion: EMailDefaults.chatVersion,
				chatId: this.chatId,
				chatContent: ChatContentTypes.NEW_AD,
				additionalHeaderFields: {
					adId: this.adId,
					title: this.title,
					description: this.description,
					priceType: this.priceType,
					priceAmount: this.priceAmount,
				},
			},
			// TODO create custom email message for every price type.
			`${this.sender.email} creates (or change) the ad ${this.title}.`,
		);
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | any {
		let json;
		switch (context) {
			case JsonContext.INTERNAL:
				json = {
					sender: (this.sender as User).toJson(false, context),
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					adId: this.adId,
					title: this.title,
					description: this.description,
					priceType: this.priceType,
					priceAmount: this.priceAmount,
					chatContent: ChatContentTypes.NEW_AD,
				};
				break;
			case JsonContext.DATABASE:
				json = {
					sender: this.sender.email,
					messageId: this.messageId,
					chatId: this.chatId,
					messageDate: this.messageDate,
					adId: this.adId,
					title: this.title,
					description: this.description,
					priceType: this.priceType,
					priceAmount: this.priceAmount,
					chatContent: ChatContentTypes.NEW_AD,
				};
				break;
			case JsonContext.PROXY:
				throw new NotUseableInJsonContextError(
					'NewAdMessage.toJson',
					JsonContext.PROXY,
				);
		}
		return asString ? JSON.stringify(json) : json;
	}

	toString(): string {
		return `${this.sender.email} creates (or change) the ad ${this.title}.`;
	}

	static fromJson(json: string | any) {
		json = Utils.getAsObject(json);
		const sender = User.fromJson(Utils.getDelAndThrow('sender', json));
		return new NewAdMessage(
			sender,
			json.messageId,
			json.chatId,
			json.messageDate,
			Utils.getDelAndThrow('adId', json),
			Utils.getAndDel('title', json) || 'NO_VALUE',
			Utils.getAndDel('description', json) || 'NO_VALUE',
			Utils.getAndDel('priceType', json) || 'NO_VALUE',
			Utils.getAndDel('priceAmount', json) || 'NO_VALUE',
		);
	}

	static load(messageId: string): Promise<NewAdMessage> {
		let iMessage: any;
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.messages.where('messageId')
					.equals(messageId)
					.first();
			})
			.then((iMessageResult: AMessage | undefined) => {
				if (!iMessageResult) {
					throw new NotFoundInDatabase(`NewAdMessage ${messageId}`);
				}
				if (iMessageResult?.chatContent != ChatContentTypes.NEW_AD) {
					throw new Error(
						`Json has the wrong type. Expected ${ChatContentTypes.NEW_AD} instead of ${iMessageResult?.chatContent}`,
					);
				}
				iMessage = iMessageResult as NewAdMessage;
				return User.load(iMessageResult.sender as any);
			})
			.then((sender: User) => {
				if (!sender) {
					throw 'sender is undefined!';
				}
				return new NewAdMessage(
					sender,
					iMessage.messageId,
					iMessage.chatId,
					iMessage.messageDate,
					iMessage.adId,
					iMessage.title,
					iMessage.description,
					iMessage.priceType,
					iMessage.priceAmount,
				);
			})
			.catch((err) => {
				console.error('NewAdMessage - load', err);
				throw err;
			});
	}

	static fromEmail(email: EMail): Promise<NewAdMessage> {
		return Promise.resolve()
			.then(() => {
				if (email.chatContent !== ChatContentTypes.NEW_AD) {
					throw new WrongEmailType(email, ChatContentTypes.NEW_AD);
				}
				return User.loadOrCreateNew(email.from);
			})
			.then((sender: User) => {
				return new NewAdMessage(
					sender,
					email.messageId,
					email.chatId,
					email.date,
					email.additionalHeaderFields.adId,
					email.additionalHeaderFields.title,
					email.additionalHeaderFields.description,
					email.additionalHeaderFields.priceType,
					email.additionalHeaderFields.priceAmount,
				);
			});
	}
}
