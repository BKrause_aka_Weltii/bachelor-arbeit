export class NotFoundInDatabase extends Error {
	constructor(object: string) {
		super(`${object} not found in Database`);
	}
}
