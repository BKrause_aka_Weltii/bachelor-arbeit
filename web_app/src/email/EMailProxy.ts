import { IProxy } from './IProxy';
import { IEMail } from './dexie_interfaces/IEMail';
import { LoginUser } from './LoginUser';
import { Config, EMailDefaults } from '../Config';
import { UserSettings, SettingKeys } from './UserSettings';
import { DateTime } from 'luxon';
import { Utils } from './utils';
import { UseMailbox } from './UseMailbox';
import { JsonContext } from './interfaces/json-rebuildable';
import { EMail, JsonEMail } from './EMail';
import { Provider } from './Provider';
import { JsonProvider } from './interfaces/provider';

export class EMailProxy implements IProxy {
	private static instance: EMailProxy;
	private _loggedInUser: LoginUser | null;
	private proxyUrl: string = Config.proxyUrl;
	private _provider: Provider | null;
	private _cachedProviderId: string;

	constructor(
		provider: Provider | null = null,
		loggedInUser: LoginUser | null = null,
	) {
		this._provider = provider;
		this._cachedProviderId = '';
		this._loggedInUser = loggedInUser;
	}

	get loggedInUser(): LoginUser | null {
		return this._loggedInUser;
	}

	set provider(provider: Provider | null) {
		this._provider = provider;
	}

	get provider(): Provider | null {
		return this._provider;
	}

	set cachedProviderId(id: string) {
		this._cachedProviderId = id;
	}

	cacheProvider(provider: Provider) {
		const url = `${this.proxyUrl}providers`;
		let cachedResponse: Response | null = null;
		return fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: provider.toJson(true, JsonContext.PROXY) as string,
		})
			.then((response) => {
				cachedResponse = response;
				return response.json();
			})
			.then((json) => {
				if (cachedResponse?.status != 200) {
					throw {
						status: cachedResponse?.status,
						statusText: cachedResponse?.statusText,
						json: json,
					};
				}
				this._cachedProviderId = provider.id;
				this._provider = provider;
				return json;
			});
	}

	fetchProviders() {
		const url = `${Config.proxyUrl}providers`;
		return fetch(url, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then(async (response) => {
				if (response.status != 200) {
					throw {
						status: response.status,
						statusText: response.statusText,
						json: await response.json(),
					};
				}
				return response.json();
			})
			.then((json) => {
				return json.map((provider: JsonProvider) => {
					return Provider.fromJson(provider);
				});
			})
			.catch((err) => {
				console.error('WebWrokerLogic - getProviders', err);
				throw err;
			});
	}

	pullMessages(): Promise<Array<EMail>> {
		return Promise.resolve().then(() => {
			this.isReadyToTalkToProxy();
			const url = `${this.proxyUrl}provider/user/get_mails`;
			let searchSinceDate: DateTime;
			return UserSettings.load()
				.then((userSettings: UserSettings) => {
					searchSinceDate = Utils.timeStringToDate(
						userSettings.getSetting(SettingKeys.LAST_PULL).value,
					);
					return UseMailbox.getOnlyUsed();
				})
				.then((mailboxes) => {
					return fetch(url, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
						},
						body: JSON.stringify(
							Utils.mergeObjects(this.getProviderDataForBody(), {
								user: this._loggedInUser?.toJson(false, JsonContext.PROXY),
								search_string: `HEADER user-agent ${EMailDefaults.userAgent}`, // SINCE ${searchSinceDate.toFormat('dd-LLL-kkkk')}`,
								mailboxes: mailboxes
									.map((mailboxe) => mailboxe.mailbox)
									.filter((mailbox) => mailbox != EMailDefaults.userAgent), // FIXME I think this run into an eror and copy wrong emails
								move_to_mailbox: EMailDefaults.userAgent,
							}),
						),
					});
				})
				.then((response) => {
					return response.json().then((json: any) => {
						if (response.status != 200) {
							return Promise.reject({
								status: response.status,
								statusText: response.statusText,
								json: json,
							});
						}
						// FIXME Move the advanced date time check to the email-proxy! it will be faster
						const emails: EMail[] =
							json.mails
								.filter(
									(rawMail: JsonEMail) =>
										Utils.timeStringToDate(rawMail.header.date) >
										searchSinceDate,
								)
								.map((email: any) => {
									return EMail.fromJson(email);
								}) || [];
						return UserSettings.load().then((settings: UserSettings) => {
							settings.setSetting(
								SettingKeys.LAST_PULL,
								Utils.getLastDateFromEmails(emails) ||
									Utils.dateToTimeString(searchSinceDate),
							);
							return Promise.resolve(emails);
						});
					});
				});
		});
	}

	sendMail(mail: EMail): Promise<boolean> {
		return Promise.resolve().then(() => {
			this.isReadyToTalkToProxy();
			const url = `${this.proxyUrl}provider/user/send_mail`;
			let cachedResponse: Response;
			// overwrite fields
			mail.additionalHeaderFields['userAgent'] = EMailDefaults.userAgent;
			return fetch(url, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(
					Utils.mergeObjects(this.getProviderDataForBody(), {
						user: this.loggedInUser?.toJson(false, JsonContext.PROXY),
						email: mail.toJson(false, JsonContext.PROXY),
					}),
				),
			})
				.then((response: Response) => {
					cachedResponse = response;
					return response.json();
				})
				.then((json: any) => {
					if (cachedResponse.status != 200) {
						throw {
							status: cachedResponse.status,
							statusText: cachedResponse.statusText,
							json: json,
						};
					}
					return json;
				})
				.catch((err) => {
					console.log('EMailProxy.sendMail', mail);
					throw err;
				});
		});
	}

	getMailboxes(): Promise<Array<String>> {
		return Promise.resolve().then(() => {
			this.isReadyToTalkToProxy();
			const url = `${this.proxyUrl}provider/user/get_mailboxes`;
			let cachedResponse: Response;
			return fetch(url, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(
					Utils.mergeObjects(this.getProviderDataForBody(), {
						user: this.loggedInUser?.toJson(false, JsonContext.PROXY),
					}),
				),
			})
				.then((response: Response) => {
					cachedResponse = response;
					return response.json();
				})
				.then((json: any) => {
					if (cachedResponse.status != 200) {
						throw {
							status: cachedResponse.status,
							statusText: cachedResponse.statusText,
							json: json,
						};
					}
					return json.mailboxes;
				});
		});
	}

	loginUser(user: LoginUser): Promise<any> {
		return Promise.resolve().then(() => {
			if (!this._provider) {
				throw 'No provder is chosen, please choose a provider!';
			}
			const url = `${this.proxyUrl}provider/check_user`;
			let cachedResponse: Response;
			return fetch(url, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(
					Utils.mergeObjects(this.getProviderDataForBody(), {
						user: user.toJson(false, JsonContext.PROXY),
					}),
				),
			})
				.then((response: Response) => {
					cachedResponse = response;
					return response.json();
				})
				.then((json: any) => {
					if (cachedResponse.status != 200) {
						throw {
							status: cachedResponse.status,
							statusText: cachedResponse.statusText,
							json: json,
						};
					}
					this._loggedInUser = user;
					return json;
				});
		});
	}

	logoutUser(): Promise<boolean> {
		this._loggedInUser = null;
		return Promise.resolve(true);
	}

	ping(): Promise<boolean> {
		const url = `${this.proxyUrl}ding`;
		return fetch(url, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then(() => {
				return true;
			})
			.catch(() => {
				return false;
			});
	}

	getProvider(providerId: string): Promise<Provider> {
		// TODO write test for the function
		const url = `${this.proxyUrl}providers/${providerId}`;
		return fetch(url, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then(async (response) => {
				if (response.status != 200) {
					throw {
						status: response.status,
						statusText: response.statusText,
						json: await response.json(),
					};
				}
				return response.json();
			})
			.then((json) => {
				return Provider.fromJson(json);
			});
	}

	private getProviderDataForBody() {
		if (this._cachedProviderId) {
			return {
				provider_id: this._cachedProviderId,
			};
		} else if (this._provider) {
			return {
				provider: this._provider.toJson(false, JsonContext.PROXY),
			};
		} else {
			throw new Error('No provider is chosen!');
		}
	}

	private isReadyToTalkToProxy() {
		if (!this._loggedInUser) {
			throw 'No user is logged in!';
		} else if (!this._provider) {
			throw 'No provder is chosen, please choose a provider!';
		}
		return true;
	}

	static getInstance() {
		if (!EMailProxy.instance) {
			EMailProxy.instance = new EMailProxy();
		}
		return EMailProxy.instance;
	}
}
