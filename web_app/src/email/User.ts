import { JsonContext, JsonRebuildable } from './interfaces/json-rebuildable';
import { IUser } from './dexie_interfaces/IUser';
import { DbClass } from './interfaces/db-class';
import { DB } from './db';
import { NotFoundInDatabase } from './NotFoundInDatabase';

export class User extends DbClass implements JsonRebuildable, IUser {
	email: string;
	nickname: string;

	constructor(email: string, nickname: string) {
		super();
		this.email = (email || '').toLowerCase();
		this.nickname = nickname || '';
	}

	get displayName() {
		return this.nickname ? this.nickname : this.email;
	}

	public static fromJson(json: string | IUser): User {
		if (typeof json === 'string') {
			json = JSON.parse(json);
		}
		json = json as IUser;
		return new User(json.email || '', json.nickname || '');
	}

	public toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | IUser {
		switch (context) {
			case JsonContext.INTERNAL:
			case JsonContext.PROXY:
			case JsonContext.DATABASE:
				const json = {
					email: this.email,
					nickname: this.nickname,
				};
				return asString ? JSON.stringify(json) : json;
		}
	}

	static async load(pk: string): Promise<User> {
		return Promise.resolve()
			.then(() => {
				return DB.getInstance().users.where('email').equals(pk).first();
			})
			.then((data) => {
				if (!data) {
					throw new NotFoundInDatabase(`User with email: ${pk}`);
				}
				return User.fromJson(data as IUser);
			});
	}

	static loadOrCreateNew(email: string): Promise<User> {
		return User.load(email)
			.then((responseUser) => {
				return responseUser;
			})
			.catch((err) => {
				console.error(
					'Load User failed with',
					err,
					'A default will create from the specified email!',
				);
				const user = new User(email, email);
				return user.save().then(() => user);
			})
			.then((responseUser) => {
				return responseUser;
			});
	}

	save(): Promise<any> {
		return DB.getInstance()
			.users.put(this.toJson(false, JsonContext.DATABASE) as IUser)
			.catch((err) => {
				console.error('Save user failed', this, err);
			});
	}

	delete(): Promise<any> {
		return DB.getInstance().users.where('email').equals(this.email).delete();
	}

	equals(other: User): boolean {
		return this.email === other.email && this.nickname === other.nickname;
	}

	static getAllUsers() {
		return DB.getInstance()
			.users.toArray()
			.then((users: IUser[]) => {
				return users.map((user) => User.fromJson(user));
			});
	}
}
