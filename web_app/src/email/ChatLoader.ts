import { DB } from './db';
import { ChatType, IChat } from './dexie_interfaces/IChat';
import { AdChat } from './AdChat';
import { Chat } from './Chat';
import { AChat } from './AChat';
import { Utils } from './utils';
import { InviteMessage } from './content_messages/InviteMessage';

export namespace ChatLoader {
	export function load(id: string): Promise<AChat> {
		return DB.getInstance()
			.chats.where('id')
			.equals(id)
			.first()
			.then((result: IChat | undefined) => {
				switch (result?.type) {
					case ChatType.AD:
						return AdChat.load(id) as Promise<AChat>;
					case ChatType.NORMAL:
						return Chat.load(id) as Promise<AChat>;
					default:
						console.error(
							`ChatLoader.load - ChatType ${result?.type} is not supported!`,
							result,
						);
						return Promise.reject();
				}
			});
	}

	export function fromJson(json: any | string) {
		json = Utils.getAsObject(json);
		switch (json.type) {
			case ChatType.AD:
				return AdChat.fromJson(json);
			case ChatType.NORMAL:
				return Chat.fromJson(json);
			default:
				console.error(
					`ChatLoader.fromJson - ChatType ${json.type} is not supported!`,
				);
				return null;
		}
	}

	export function fromInviteMessage(message: InviteMessage) {
		console.log('fromInviteMessage', message);
		switch (message.chatType) {
			case ChatType.AD:
				return AdChat.fromInviteMessage(message);
			case ChatType.NORMAL:
				console.log('Chat.fromInviteMessage', message);
				return Chat.fromInviteMessage(message);
			default:
				console.error(
					`ChatLoader.fromInviteMessage - ChatType ${message.chatType} is not supported!`,
				);
				return null;
		}
	}
}
