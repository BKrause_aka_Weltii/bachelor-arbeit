import { Chat } from './Chat';
import { User } from './User';
import { AMessage } from './dexie_interfaces/AMessage';
import { Ad } from './Ad';
import { JsonContext, JsonRebuildable } from './interfaces/json-rebuildable';
import { Utils } from './utils';
import { IChatParticipants } from './dexie_interfaces/IChatParticipants';
import { DB } from './db';
import { NotFoundInDatabase } from './NotFoundInDatabase';
import { IChat, ChatType } from './dexie_interfaces/IChat';
import { ChatContentTypes } from './ChatContentTypes';
import { DeleteAdMessage } from './content_messages/DeleteAdMessage';
import { UserHasNoRightsToPerformeEmailError } from './exceptions/UserHasNoRightsToPerformeEmailError';
import { NewAdMessage } from './content_messages/NewAdMessage';
import { DbClass } from './interfaces/db-class';
import { AChat } from './AChat';
import { DeleteMessage } from './content_messages/DeleteMessage';
import { InviteMessage } from './content_messages/InviteMessage';
import { MessageRebuilder } from './content_messages/MessageRebuilder';

export class AdChat extends AChat {
	constructor(
		id: string,
		name: string,
		creationDate: string,
		messages: AMessage[],
		participants: User[],
		admins: User[],
		public _ads: Ad[],
		isPublic: boolean,
		onlyAdminsCanSend: boolean,
	) {
		super(
			id,
			name,
			creationDate,
			messages,
			participants,
			admins,
			isPublic,
			onlyAdminsCanSend,
			ChatType.AD,
		);
	}

	get ads() {
		return this._ads;
	}

	toJson(
		asString: boolean = false,
		context: JsonContext = JsonContext.INTERNAL,
	): string | any {
		const json: any = super.toJson(false, context) as any;
		if (context == JsonContext.INTERNAL) {
			json['ads'] = this._ads.map((ad) => ad.toJson(false, context));
		}
		return asString ? JSON.stringify(json) : json;
	}

	static fromInviteMessage(message: InviteMessage): AdChat {
		return new AdChat(
			message.chatId,
			message.chatName,
			message.messageDate,
			[message],
			message.newMembers.concat(message.existingMembers),
			message.admins,
			[],
			message.isPublic,
			message.onlyAdminsCanSend,
		);
	}

	static fromJson(json: string | any): AdChat {
		json = Utils.getAsObject(json);
		const messages = (json.messages || []).map((message: any) =>
			MessageRebuilder.messageRebuilder(message),
		);
		const participants = (json.participants || []).map((participant: any) =>
			User.fromJson(participant),
		);
		const admins = (json.admins || []).map((admin: any) =>
			User.fromJson(admin),
		);
		const ads = (json.ads || []).map((ad: any) => Ad.fromJson(ad));
		return new AdChat(
			json.id,
			json.name,
			json.creationDate,
			messages,
			participants,
			admins,
			ads,
			json.isPublic,
			json.onlyAdminsCanSend,
		);
	}

	equals(other: AdChat) {
		return (
			this.id === other.id &&
			this.creationDate === other.creationDate &&
			this.name === other.name &&
			this.messages.length === other.messages.length &&
			this.participants.length === other.participants.length &&
			this.isPublic === other.isPublic &&
			this.type === other.type &&
			this._ads.length == other._ads.length
		);
	}

	save(): Promise<string> {
		return new Promise<string>((resolve) => {
			const stats: any = {};
			const chat: IChat = this.toJson(false, JsonContext.DATABASE) as IChat;
			const participants = this.participants.map((participant) => {
				return {
					email: participant.email,
					chatId: this.id,
					isAdmin:
						this.admins.filter((admin) => {
							return admin.email === participant.email;
						}).length > 0,
				};
			});

			return Promise.resolve()
				.then(() => {
					return Promise.all(
						this.participants.concat().map((user: User) => {
							return user.save();
						}),
					);
				})
				.then((userResult) => {
					stats['users'] = userResult;
					return DB.getInstance().chatParticipants.bulkPut(participants, {
						allKeys: true,
					});
				})
				.then((participantsResult) => {
					stats['participants'] = participantsResult;
					const messages = this.messages;
					return Promise.all(
						messages.map((message: any) => {
							return MessageRebuilder.messageSaver(message);
						}),
					);
				})
				.then((messagesResult) => {
					stats['messages'] = messagesResult;
					console.log(this._ads);
					return Promise.all(this._ads.map((ad) => ad.save()));
				})
				.then((adSaveResult) => {
					stats['ads'] = adSaveResult;
					console.log(adSaveResult);
					return DB.getInstance().chats.put(chat);
				})
				.then((chatResult) => {
					stats['chat'] = chatResult;
					resolve(stats);
				})
				.catch((err) => {
					throw err;
				});
		});
	}

	static load(id: string): Promise<AdChat> {
		let rawChatParticipants: IChatParticipants[] = [];
		let participants: User[] = [];
		let admins: User[] = [];
		let messages: any[] = [];
		let ads: Ad[] = [];
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.chatParticipants.where('chatId')
					.equals(id)
					.toArray();
			})
			.then(async (iChatParticipantResult: IChatParticipants[]) => {
				rawChatParticipants = iChatParticipantResult;
				return Promise.all(
					iChatParticipantResult.map((participant) => {
						return User.load(participant.email);
					}),
				);
			})
			.then((loadUserResult: User[]) => {
				participants = loadUserResult;
				rawChatParticipants.forEach((rawParticipant: IChatParticipants) => {
					participants.forEach((participant: User) => {
						if (
							participant.email === rawParticipant.email &&
							rawParticipant.isAdmin
						) {
							admins.push(participant);
						}
					});
				});
				return DB.getInstance().messages.where('chatId').equals(id).toArray();
			})
			.then((messageResult) => {
				messages = messageResult;
				return Promise.all(
					messages.map((message: AMessage) => {
						// @ts-ignore
						return MessageRebuilder.messageLoader(
							message.chatContent,
							message.messageId,
						).then((result: any) => result);
					}),
				);
			})
			.then((res: AMessage[]) => {
				messages = res;
				return DB.getInstance().ad.where('chatId').equals(id).toArray();
			})
			.then((adResult: any[]) => {
				return Promise.all(adResult.map((ad: any) => Ad.load(ad.id)));
			})
			.then((adLoadedResult) => {
				ads = adLoadedResult;
				return DB.getInstance().chats.where('id').equals(id).first();
			})
			.then((result) => {
				if (!result) {
					throw new NotFoundInDatabase(`AdChat with id: ${id}`);
				}
				return new AdChat(
					result.id,
					result.name,
					result.creationDate,
					messages,
					participants,
					admins,
					ads,
					result.isPublic,
					result.onlyAdminsCanSend,
				);
			})
			.catch((err) => {
				throw err;
			});
	}

	getAd(adId: string): Ad | null {
		return this.ads.find((ad) => ad.id == adId) || null;
	}

	isAllowedToPerformeAction(message: AMessage) {
		if (!super.isAllowedToPerformeAction(message)) {
			return false;
		}

		if (this.onlyAdminsCanSend) {
			return this.isAdmin(message.sender.email);
		}
		switch (message.chatContent) {
			case ChatContentTypes.DELETE_USER_FROM_CHAT:
				return (
					this.isAdmin(message.sender.email) ||
					message.sender.email === (message as DeleteMessage).deleteUserEmail
				);
			case ChatContentTypes.INVITE:
				return this.isPublic ? true : this.isAdmin(message.sender.email);
			case ChatContentTypes.PROMOTE:
				return this.isAdmin(message.sender.email);
			case ChatContentTypes.TEXT_MESSAGE:
			case ChatContentTypes.NEWS:
			case ChatContentTypes.DELETE_AD:
			case ChatContentTypes.NEW_AD:
				console.log('sender', message, message.sender);
				return this.isParticipant(message.sender.email);
			case ChatContentTypes.JOIN_REQUEST:
				return this.isPublic;
			default:
				console.error(
					`${message.chatContent} won't be processed in Chat.isAllowedToPerformeAction`,
				);
				return false;
		}
	}

	delete(): Promise<any> {
		const stats: any = {};
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.messages.where('chatId')
					.equals(this.id)
					.delete();
			})
			.then((deleteMessagesResult) => {
				stats['messages'] = deleteMessagesResult;
				return DB.getInstance()
					.chatParticipants.where('chatId')
					.equals(this.id)
					.delete();
			})
			.then((deleteChatParticipantsResult) => {
				stats['participants'] = deleteChatParticipantsResult;
				return DB.getInstance().ad.where('chatId').equals(this.id).delete();
			})
			.then((adDeleteResult) => {
				stats['ads'] = adDeleteResult;
				return DB.getInstance().chats.where('id').equals(this.id).delete();
			})
			.then((deleteChatResult) => {
				stats['chat'] = deleteChatResult;
				return stats;
			});
	}

	addMessage(message: AMessage): Promise<any> {
		if (!this.isAllowedToPerformeAction(message)) {
			return Promise.reject(new UserHasNoRightsToPerformeEmailError(message));
		}
		let ad: Ad | null;
		switch (message.chatContent) {
			case ChatContentTypes.DELETE_AD:
				const adId = (message as DeleteAdMessage).adId;
				ad = this.getAd(adId);
				if (ad) {
					if (
						ad.creator.email == message.sender.email ||
						this.isAdmin(message.sender.email)
					) {
						this._ads = this.ads.filter((ad) => ad.id != adId);
						return ad.delete();
					} else {
						Promise.reject(new UserHasNoRightsToPerformeEmailError(message)); // FIXME this is dirty, add a isAllowdToPerformaeAction function!
					}
				} else {
					return Promise.reject(new Error(`Ad ${adId} not found!`));
				}
				this._ads = this.ads.filter(
					(ad) => (message as DeleteAdMessage).adId == ad.id,
				);
				this.messages.push(message);
				return Promise.resolve();
			case ChatContentTypes.NEW_AD:
				ad = this.getAd((message as NewAdMessage).adId);
				if (!ad) {
					this._ads.push(Ad.fromNewAdMessage(message as NewAdMessage));
				} else {
					const replacedAd = new Ad(
						ad.id,
						ad.creator,
						ad.chatId,
						(message as NewAdMessage).title || ad.title,
						(message as NewAdMessage).description || ad.description,
						(message as NewAdMessage).priceType || ad.priceType,
						(message as NewAdMessage).priceAmount || ad.priceAmount,
					);
					this._ads = this._ads.map((ad) =>
						ad.id == replacedAd.id ? replacedAd : ad,
					);
				}
				this.messages.push(message);
				return Promise.resolve();
				break;
			default:
				return super.addMessage(message);
		}
	}
}
