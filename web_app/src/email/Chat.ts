import { JsonContext } from './interfaces/json-rebuildable';
import { IChat, ChatType } from './dexie_interfaces/IChat';
import { EMail } from './EMail';
import { User } from './User';
import { Utils } from './utils';
import { DB } from './db';
import { NotFoundInDatabase } from './NotFoundInDatabase';
import { ChatContentTypes } from './ChatContentTypes';
import { WrongEmailType } from './WrongEmailType';
import { AMessage } from './dexie_interfaces/AMessage';
import { InviteMessage } from './content_messages/InviteMessage';
import { IChatParticipants } from './dexie_interfaces/IChatParticipants';
import { DeleteMessage } from './content_messages/DeleteMessage';
import { AChat } from './AChat';
import { MessageRebuilder } from './content_messages/MessageRebuilder';

export class Chat extends AChat {
	constructor(
		id: string,
		name: string,
		creationDate: string,
		messages: AMessage[],
		participants: User[],
		admins: User[],
		isPublic: boolean,
		onlyAdminsCanSend: boolean,
	) {
		super(
			id,
			name,
			creationDate,
			messages,
			participants,
			admins,
			isPublic,
			onlyAdminsCanSend,
			ChatType.NORMAL,
		);
	}

	isAllowedToPerformeAction(message: AMessage) {
		if (!super.isAllowedToPerformeAction(message)) {
			return false;
		}

		if (this.onlyAdminsCanSend) {
			return this.isAdmin(message.sender.email);
		}
		switch (message.chatContent) {
			case ChatContentTypes.DELETE_USER_FROM_CHAT:
				return (
					this.isAdmin(message.sender.email) ||
					message.sender.email === (message as DeleteMessage).deleteUserEmail
				);
			case ChatContentTypes.INVITE:
				return this.isPublic ? true : this.isAdmin(message.sender.email);
			case ChatContentTypes.PROMOTE:
				return this.isAdmin(message.sender.email);
			case ChatContentTypes.TEXT_MESSAGE:
			case ChatContentTypes.NEWS:
				console.log('sender', message, message.sender);
				return this.isParticipant(message.sender.email);
			case ChatContentTypes.JOIN_REQUEST:
				return this.isPublic;
			default:
				console.error(
					`${message.chatContent} won't be processed in Chat.isAllowedToPerformeAction`,
				);
				return false;
		}
	}

	static fromInviteMessage(message: InviteMessage): AChat {
		return new Chat(
			message.chatId,
			message.chatName,
			message.messageDate,
			[],
			message.newMembers.concat(message.existingMembers as User[]),
			message.admins,
			message.isPublic,
			message.onlyAdminsCanSend,
		);
	}

	static fromEMail(email: EMail): Promise<Chat> {
		return new Promise((resolve, reject) => {
			if (email.chatContent === ChatContentTypes.INVITE) {
				resolve(
					InviteMessage.fromEmail(email).then(
						(inviteMessage: InviteMessage) => {
							return new Chat(
								inviteMessage.chatId,
								inviteMessage.chatName,
								inviteMessage.joinDate,
								[inviteMessage],
								inviteMessage.newMembers.concat(
									inviteMessage.existingMembers as User[],
								),
								inviteMessage.admins,
								inviteMessage.isPublic,
								inviteMessage.onlyAdminsCanSend,
							);
						},
					),
				);
			} else {
				reject(new WrongEmailType(email, ChatContentTypes.INVITE));
			}
		});
	}

	static load(pk: string): Promise<Chat> {
		let rawChatParticipants: IChatParticipants[] = [];
		let participants: User[] = [];
		let admins: User[] = [];
		let messages: any[] = [];
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.chatParticipants.where('chatId')
					.equals(pk)
					.toArray();
			})
			.then(async (iChatParticipantResult: IChatParticipants[]) => {
				rawChatParticipants = iChatParticipantResult;
				return Promise.all(
					iChatParticipantResult.map((participant) => {
						return User.load(participant.email);
					}),
				);
			})
			.then((loadUserResult: User[]) => {
				participants = loadUserResult;
				rawChatParticipants.forEach((rawParticipant: IChatParticipants) => {
					participants.forEach((participant: User) => {
						if (
							participant.email === rawParticipant.email &&
							rawParticipant.isAdmin
						) {
							admins.push(participant);
						}
					});
				});
				return DB.getInstance().messages.where('chatId').equals(pk).toArray();
			})
			.then((messageResult) => {
				messages = messageResult;
				return Promise.all(
					messages.map((message: AMessage) => {
						// @ts-ignore
						return MessageRebuilder.messageLoader(
							message.chatContent,
							message.messageId,
						).then((result: any) => result);
					}),
				);
			})
			.then((res: AMessage[]) => {
				messages = res;
				return DB.getInstance().chats.where('id').equals(pk).first();
			})
			.then((result) => {
				if (!result) {
					throw new NotFoundInDatabase(`Chat with id: ${pk}`);
				}
				return new Chat(
					result.id,
					result.name,
					result.creationDate,
					messages,
					participants,
					admins,
					result.isPublic,
					result.onlyAdminsCanSend,
				);
			})
			.catch((err) => {
				throw err;
			});
	}

	delete(): Promise<any> {
		const stats: any = {};
		return Promise.resolve()
			.then(() => {
				return DB.getInstance()
					.messages.where('chatId')
					.equals(this.id)
					.delete();
			})
			.then((deleteMessagesResult) => {
				stats['messages'] = deleteMessagesResult;
				return DB.getInstance()
					.chatParticipants.where('chatId')
					.equals(this.id)
					.delete();
			})
			.then((deleteChatParticipantsResult) => {
				stats['participants'] = deleteChatParticipantsResult;
				return DB.getInstance().chats.where('id').equals(this.id).delete();
			})
			.then((deleteChatResult) => {
				stats['chat'] = deleteChatResult;
				return stats;
			});
	}

	save(): Promise<string> {
		return new Promise<string>((resolve) => {
			const stats: any = {};
			const chat: IChat = this.toJson(false, JsonContext.DATABASE) as IChat;
			const participants = this.participants.map((participant) => {
				return {
					email: participant.email,
					chatId: this.id,
					isAdmin:
						this.admins.filter((admin) => {
							return admin.email === participant.email;
						}).length > 0,
				};
			});

			return Promise.resolve()
				.then(() => {
					return Promise.all(
						this.participants.concat().map((user: User) => {
							return user.save();
						}),
					);
				})
				.then((userResult) => {
					stats['users'] = userResult;
					return DB.getInstance().chatParticipants.bulkPut(participants, {
						allKeys: true,
					});
				})
				.then((participantsResult) => {
					stats['participants'] = participantsResult;
					const messages = this.messages;
					return Promise.all(
						messages.map((message: AMessage) => {
							return message.save();
						}),
					);
				})
				.then((messagesResult) => {
					stats['messages'] = messagesResult;
					return DB.getInstance().chats.put(chat);
				})
				.then((chatResult) => {
					stats['chat'] = chatResult;
					resolve(stats);
				})
				.catch((err) => {
					throw err;
				});
		});
	}

	static fromJson(json: string | IChat | any) {
		json = Utils.getAsObject(json);
		const messages = (json.messages || []).map((message: any) => {
			return MessageRebuilder.messageRebuilder(message);
		});
		const participants = (json.participants || []).map((participant: any) => {
			return User.fromJson(participant);
		});
		const admins = (json.admins || []).map((admin: any) => {
			return User.fromJson(admin);
		});
		return new Chat(
			json.id,
			json.name,
			json.creationDate,
			messages,
			participants,
			admins,
			json.isPublic,
			json.onlyAdminsCanSend,
		);
	}

	equals(other: Chat) {
		// FIXME add more specific test for participants and messages!
		return (
			this.id === other.id &&
			this.creationDate === other.creationDate &&
			this.name === other.name &&
			this.messages.length === other.messages.length &&
			this.participants.length === other.participants.length &&
			this.isPublic === other.isPublic &&
			this.type === other.type
		);
	}
}
