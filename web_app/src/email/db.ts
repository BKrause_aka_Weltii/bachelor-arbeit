import Dexie from 'dexie';
import { IUser } from './dexie_interfaces/IUser';
import { ILoginUser } from './dexie_interfaces/ILoginUser';
import { IEMail } from './dexie_interfaces/IEMail';
import { IProvider } from './dexie_interfaces/IProvider';
import { IAdditionalHeaderFields } from './dexie_interfaces/IAdditionalHeaderFields';
import { IEmailTo } from './dexie_interfaces/IEmailTo';
import { IChat } from './dexie_interfaces/IChat';
import { IChatParticipants } from './dexie_interfaces/IChatParticipants';
import { Config } from '../Config';
import { AMessage } from './dexie_interfaces/AMessage';
import { ISetting } from './dexie_interfaces/ISetting';
import { IUseMailbox } from './dexie_interfaces/IUseMailbox';
import { IAd } from './dexie_interfaces/IAd';

export class DB extends Dexie {
	private static instance: DB;

	lastUser: Dexie.Table<ILoginUser, number>;
	providers: Dexie.Table<IProvider, number>;
	messages: Dexie.Table<AMessage, number>;
	chats: Dexie.Table<IChat, number>;
	chatParticipants: Dexie.Table<IChatParticipants, number>;
	users: Dexie.Table<IUser, number>;
	settings: Dexie.Table<ISetting, number>;
	mailboxes: Dexie.Table<IUseMailbox, number>;
	ad: Dexie.Table<IAd, number>;

	private constructor() {
		super(Config.appDatabaseName);
		// NOTE from https://dexie.org/docs/API-Reference#quick-reference:
		// Don’t declare all columns like in SQL.
		// You only declare properties you want to index,
		// that is properties you want to use in a where(…) query.
		this.version(1).stores({
			lastUser: 'email, nickname, loginName, password',
			providers: 'id, name, baseUrl',
			messages: 'messageId, chatId, messageDate',
			chats: 'id, name',
			chatParticipants: '[chatId+email], chatId, email',
			users: 'email, nickname',
			settings: 'key, value',
		});
		this.version(2).stores({
			// old
			lastUser: 'email, nickname, loginName, password',
			providers: 'id, name, baseUrl',
			messages: 'messageId, chatId, messageDate',
			chats: 'id, name',
			chatParticipants: '[chatId+email], chatId, email',
			users: 'email, nickname',
			settings: 'key, value',
			// chagnes
			mailboxes: 'mailbox, use',
		});
		this.version(3).stores({
			// old
			lastUser: 'email, nickname, loginName, password',
			providers: 'id, name, baseUrl',
			messages: 'messageId, chatId, messageDate',
			chats: 'id, name',
			chatParticipants: '[chatId+email], chatId, email',
			users: 'email, nickname',
			settings: 'key, value',
			mailboxes: 'mailbox, use',
			// changes
			ad: 'id, creator, chatId',
		});
		this.lastUser = this.table('lastUser');
		this.providers = this.table('providers');
		this.messages = this.table('messages');
		this.chats = this.table('chats');
		this.chatParticipants = this.table('chatParticipants');
		this.users = this.table('users');
		this.settings = this.table('settings');
		this.mailboxes = this.table('mailboxes');
		this.ad = this.table('ad');
	}

	/**
	 * Get the DB instance.
	 */
	public static getInstance() {
		if (!DB.instance) {
			DB.instance = new DB();
		}
		return DB.instance;
	}

	public clearAll() {
		this.lastUser.clear();
		this.providers.clear();
		this.messages.clear();
		this.chats.clear();
		this.chatParticipants.clear();
		this.users.clear();
		this.settings.clear();
		this.mailboxes.clear();
		this.ad.clear();
	}
}
