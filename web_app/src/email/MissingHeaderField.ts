export class MissingHeaderField extends Error {
	constructor(headerField: string) {
		super(`The header field ${headerField} is missing!`);
	}
}
