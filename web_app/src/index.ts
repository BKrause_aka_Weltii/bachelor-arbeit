import { WebWorkerBridge } from './WebWorker/WebWorkerBridge';
import { WebWorkerEventType } from './WebWorkerEvents/WebWorkerEventType';
import { WebWorkerEvent } from './WebWorkerEvents/WebWorkerEvent';

const webWorkerBridge = new WebWorkerBridge();
require('./app/index');
webWorkerBridge.consumeEvent(
	new WebWorkerEvent(WebWorkerEventType.READY_TO_WORK, {}, null, false),
);
